![logo](/static/cavecanem.svg)

Cave Canem est un site qui concentre les outils pour contrer la surveillance de masse et améliorer la protection numérique de sa vie privée.

## Adresse actuelle

[https://cavecanem.frama.io](https://cavecanem.frama.io)

## Contributions

Créer un compte Gitlab sur [Framagit](https://framagit.org/) et soumettre des modifications de contenu au dossier /content.

## Contact

Par mail via PGP > [Contact](https://cavecanem.frama.io/contact.html).

---

[https://fr.wikipedia.org/wiki/Cave_canem](https://fr.wikipedia.org/wiki/Cave_canem)
