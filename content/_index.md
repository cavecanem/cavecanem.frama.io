---
title: "Accueil"
bookToc: false
---
{{<align center>}}

![logo](./cavecanem.svg "Chien chien")

# Cave Canem

### Améliorer la protection numérique de sa vie privée et contrer la surveillance de masse.

###### [Mise à jour : 14 décembre 2020](./maj.html)

{{</align>}}

{{<block info>}}

### Déclaration universelle des droits de l'homme
***

<u>Article 12.</u>

Nul ne sera l'objet d'immixtions arbitraires dans sa vie privée, sa
famille, son domicile ou sa correspondance, ni d'atteintes à son honneur
et à sa réputation. Toute personne a droit à la protection de la loi
contre de telles immixtions ou de telles atteintes.

<u>Article 19.</u>

Tout individu a droit à la liberté d'opinion et d'expression, ce qui
implique le droit de ne pas être inquiété pour ses opinions et celui de
chercher, de recevoir et de répandre, sans considérations de frontières,
les informations et les idées par quelque moyen d'expression que ce
soit.

{{</block>}}

{{< next relref="/docs/start/introduction" >}} Introduction  > {{< /next >}}
