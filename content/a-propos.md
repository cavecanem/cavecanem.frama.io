---
title: "À propos"
bookToc: false
---

# À propos
***

Ce site est issu d'une initiative indépendante, anonyme et menée sans financements.

Il est à vocation éducative et sociale dans le but de fournir à chacun les informations nécessaires pour sécuriser et protéger la confidentialité de ses données numériques.

Ce site n'utilise aucun outil d’analyses, aucun cookie, pas de publicité, aucune affiliation, aucune requête inter-sites.

Il n'utilise pas non plus de requêtes externes. Les images, polices et scripts sont servis depuis la racine du site.

Pour éviter les traqueurs, et que Google puisse suivre l'utilisateur sur le site, aucune vidéo Youtube n'est intégrée sur le site. Elles sont servies avec [Invidious](https://invidious.snopyta.org/) en liens externes.

Conçu avec [Hugo](https://gohugo.io/) et sur la base [Hugo Book](https://github.com/alex-shpak/hugo-book).

Hébergé sur le [gitlab](https://framagit.org) de [Framasoft](https://framasoft.org/fr/), le code source est disponible à [cette adresse](https://framagit.org/cavecanem/cavecanem.frama.io).

Les polices utilisées, [Fira](https://github.com/mozilla/Fira) et [Hepta Slab](https://github.com/mjlagattuta/Hepta-Slab), sont sous [SIL Open Font License](https://scripts.sil.org/cms/scripts/page.php?site_id=nrsi&id=ofl).

Site sous [Creative Commons Attribution-NonCommercial-ShareAlike 4.0 International License](http://creativecommons.org/licenses/by-nc-sa/4.0/).

{{<license>}}
