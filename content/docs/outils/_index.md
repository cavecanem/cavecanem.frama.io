---
title: Outils et services
bookFlatSection: true
bookCollapseSection: true
bookTOC: false
weight: 2
---

# Outils et services
***

Cette partie concerne les outils ou les services utilisés tous les jours. Pour chacun, une analyse de ce qui est en jeu et une sélection recommandée des meilleurs et ceux qui ne devraient pas ou plus être utilisés.

{{<section>}}
