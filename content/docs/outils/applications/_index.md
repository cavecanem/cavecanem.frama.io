---
title: "Applications mobiles"
weight: 5
bookToc: false
description: Que font les applications mobiles ? Faut-il les utiliser ? Quels sont les alternatives ?
---

# Applications mobiles
***

Une application mobile, ou app, est un programme.

Il y a plusieurs types d'app:
-  Celles qui sont purement un programme d'application (traitement d'image, calendrier, messagerie instantanée : Signal, WhatsApp par ex., etc.).
-  Celles qui sont un programme qui remplace le site Internet d'un service (Amazon, Gmail, Facebook, Messenger, etc.).

Dans le second cas, l'app propose principalement un accès à Internet avec une interface différente de celle du site classique qui gère mieux les gestes tactiles et les petits écrans de téléphones et de tablettes.

Il n'en reste pas moins que ce second type d'app est exactement comme un site Internet à la différence qu'elle accède au service par Internet <u>sans passer par un navigateur</u>.

## Les dangers
***

Dans les deux cas, une app est donc un petit programme qui a la possibilité d'intégrer une quantité de fonctionnalités et d'obligations auxquelles un navigateur, lui, n'est pas soumis et peut neutraliser.

Une app peut entre autre:

-  Intégrer des traqueurs
-  Intégrer de la publicité
-  Contourner un VPN
-  Obliger l'autorisation d'accès à toutes les photos, le micro, la caméra, les contacts, les calendriers ou la connexion Bluetooth de l'appareil, pour fonctionner.
-  Récupérer des données personnelles sur les activités de l'utilisateur (heure d'ouverture, recherches, temps passé sur l'app, niveau de connexion, télémétrie (enregistrement du clavier, des doigts (nombre de clics, types de gestes), etc.).
-  Communiquer avec le système et avec d'autres applications.
-  Et même, ironie du sort, intégrer un navigateur interne !

{{<block info>}}

### Exodus Privacy | [exodus-privacy.eu.org](https://exodus-privacy.eu.org/fr/)
***

Organisation européenne qui analyse les problèmes de vie privée des applications Android. L'app proposée par le site peut également analyser celles présentes sur le téléphone.

Pour les iPhone, l'organisation ne peut pas faire d'analyses légalement, ce qui veut dire que les apps peuvent avoir des analyses similaires, mais ce n'est pas sûr. Il est quand même de bonne augure d'aller voir ce que nécessite l'app sur Android comme permissions d'accès ou quels traceurs elle contient.

{{</block>}}

## Navigateurs internes aux apps
***

Ce cas est très pernicieux. Des applications, comme Facebook et Instagram, utilisent depuis plusieurs années un navigateur interne.

Ainsi tous les liens touchés dans l'app ne s'ouvrent désormais plus avec un navigateur externe comme Firefox, Chrome ou Safari, mais directement dans un navigateur intégré à l'application mobile.

Ce navigateur propriétaire ne peut absolument pas être configuré et n'est pas imposé par hasard.

Ses principales fonctions :

-  Faire rester l'utilisateur dans l'app le plus possible.
-  Récupérer encore plus de liens touchés par les utilisateurs en dehors du service original.
-  Récupérer, lire et analyser tous les cookies de connexion.
-  Récupérer tout l'historique de navigation.
-  Récupérer toutes les données de navigation.
-  Inciter à faire des marque-pages qui seront stockés dans l'app afin d'analyser encore plus nos intérêts.

## Parades
***

Pour contrer les services qui contournent nos possibilités de protection personnelle:

1.  Supprimer l'application.
2.  Utiliser son navigateur pour accéder au site internet directement.
3.  Boycotter et changer de service s'il ne l'autorise pas.
4.  Faire des _Progressive Web App_.
5.  Pour Gmail et Outlook, utiliser un [client email](/docs/outils/emails/clients.html).

## Progressive Web App (PWA)
***

C'est un système assez récent qui permet d'enregistrer un site Web sur l'écran d'accueil, comme une application mobile classique.

De plus en plus, les sites proposent une version PWA spécialement codée. Donc certains auront une utilisation très proche d'une application mobile, d'autres ne seront pas bien différent de la version sur le navigateur.

Dans les deux cas, l'avantage réside dans le fait qu'il est aussi rapide qu'avec une app d'accéder au service. Ce dernier sera utilisé exclusivement en passant par le navigateur, qui est, contrairement aux apps, [configuré](/docs/outils/navigateurs/configurations.html) pour protéger l'utilisateur. Le service ne pourra pas l'obliger d'accepter l'accès à toutes ses photos, son micro, sa caméra, ses contacts, ses calendriers etc.

Dans certains cas, faire des PWA peut aussi permettre aux téléphones "obsolètes" (qui ne peuvent plus recevoir certaines apps), d'utiliser un service comme une app, sans changer de téléphone.

{{<tabs "PWA">}}
{{<tab "Android">}}
{{<block info>}}

1.  Si ce n'est pas déjà fait: [installer et configurer Firefox + installer les extensions.](/docs/outils/navigateurs.html)
2.  Puis > [Tutoriel](https://invidious.snopyta.org/watch?v=heSvwQgEMLM) <span class="gris">25s.</span>

![Images-PWA-Tutoriel](/img/applications/PWA-Android.png "PWA-Android-Tutoriel")

{{</block>}}
{{</tab>}}

{{<tab "iOS">}}
{{<block info>}}

Apple monopolise Safari en interdisant aux autres navigateurs d'utiliser les PWA.

1.  Donc [configurer Safari](/docs/outils/navigateurs/configurations.html) si ce n'est pas déjà fait.
2.  Puis > [Tutoriel](https://invidious.snopyta.org/watch?v=KY0ixCLegvs) <span class="gris">30s.</span>

![Images-PWA-Tutoriel](/img/applications/PWA-iOS.png "PWA-iOS-Tutoriel")

{{</block>}}
{{</tab>}}
{{</tabs>}}

## App store alternatifs
***

Les alternatives aux _stores_ propriétaires (Google et Apple) existent et permettent d'utiliser une variété d'applications mobiles [_open source_ (FOSS)](/docs/start/code-source.html) qui seront bien plus respectueuses de notre confidentialité :

-  Ne récoltent pas les données de ce que l'on télécharge à titre personnel.
-  Proposent uniquement des applications mobiles _open source_.
-  Ne font pas de censure en fonction des pays, ou en fonction du type d'application et/ou de leurs descriptions.
-  Ne volent pas les créateurs en leurs imposant des frais d'exploitation ultra élevé empêchant les alternatives d'exister. (Source : [Blog protonmail.com](https://protonmail.com/blog/apple-app-store-antitrust/) 22/06/20)

{{<tabs "store">}}
{{<tab "Android">}}
{{<block info>}}

### F-Droid | [f-droid.org](https://f-droid.org/)
***

Un magasin d'applications [FOSS](/docs/start/code-source.html) qui remplace le Google App Store d'Android en recherchant des applications libres et _open source_.

{{</block>}}

{{<block info>}}

### Aurora Store | [auroraoss.com](https://auroraoss.com)
***

Aurora Store permet le téléchargement d'applications [FOSS](/docs/start/code-source.html) et également d'une grande partie des applications contenues dans le Google Play Store, anonymement, et donc, sans avoir besoin de créer un compte Google.

Aurora Store est disponible sur F-Droid [ici](https://f-droid.org/en/packages/com.aurora.store/).

{{</block>}}

{{</tab>}}

{{<tab "iOS">}}
{{<block info>}}

### AltStore | [altstore.io](https://altstore.io/)
***

Ce n'est pas réellement un _App Store_ mais plutôt une technique permettant d'installer n'importe quelle application sur iPhone sans _Jailbreak_. Il peut donc servir à remplacer l'_App Store_ d'Apple en recherchant des applications libres et _open source_ qui ne sont pas présentes sur celui-ci.

Contenu du fait qu'Apple refuse d'offrir la possibilité d'installer ce genre d'alternative, le processus est plus complexe qu'avec F-Droid sur Android. Il nécessite également un ordinateur sous Windows 10 ou macOS 10.14.4 (Mojave) ou supérieure, pour être utilisé.

Vidéo : [The alternative App Store for iPhones](https://invidious.snopyta.org/watch?v=ftyWe6DVvO4)  <span class="gris">7min 55s</span>

{{</block>}}

{{</tab>}}
{{</tabs>}}
