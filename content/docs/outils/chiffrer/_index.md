---
title: "Chiffrer"
weight: 7
bookToc: false
bookCollapseSection: true
description: Le chiffrement permet de rendre illisible ses fichiers personnels numériques et de se protéger ainsi du piratage et de l'espionnage. Quels sont les différentes techniques de chiffrement ?
---
# Chiffrer
***

Lorsque le service n'est pas chiffré de bout en bout, ou si notre accord de confiance envers lui est questionné, il est judicieux, voire nécessaire, de chiffrer ses fichiers. Cela accroît sa sécurité et la confidentialité de ses documents.  
(Rappel ? Voir chapitre sur [le chiffrement](/docs/start/chiffrement.html).)

{{< section >}}
