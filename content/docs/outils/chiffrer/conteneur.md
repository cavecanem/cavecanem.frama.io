---
title: "Conteneur"
weight: 3
bookToc: false
summary: Créer un dossier chiffré pouvant accueillir de multiple fichiers.
---

# Créer un conteneur chiffré
***

Un conteneur est comme un dossier où tous les fichiers qu'il contient sont chiffrés. C'est utile pour ranger un ensemble de documents sans les chiffrer un à un, que ce soit sur un ordinateur, une clé USB, un Cloud ou même un CD.


{{<block info>}}

### Cryptomator | [cryptomator.org](https://cryptomator.org/)
***

– Tutoriel: [Cryptomator - Chiffrez vos fichiers dans le cloud](https://invidious.snopyta.org/watch?v=uEftE5gLn1U) <span class="gris">16min 05s</span>

{{</block>}}

{{<block info>}}

### Veracrypt | [veracrypt.fr](https://www.veracrypt.fr/)
***

– Tutoriel: [Veracrypt - Disques et conteneurs chiffrés](https://invidious.snopyta.org/watch?v=wtC_qz-RCiE) <span class="gris">31min 06s</span>

{{</block>}}

{{<block info>}}

### Filevault
***

Filevault est intégré dans macOS:  
Préférences Système > Sécurité et confidentialité > Filevault

– Tutoriel: [Comment crypter seulement quelques données](https://invidious.snopyta.org/watch?v=ug-sXvINiyc) <span class="t">7min 59s</span>

{{</block>}}

{{< prev relref="/docs/outils/chiffrer" >}} < Chiffrer {{< /prev >}}
