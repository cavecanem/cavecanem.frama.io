---
title: "Disque dur"
weight: 1
bookToc: false
summary: Pour chiffrer toutes les données d'un appareil (ordinateur, téléphone, tablette).
---

# Chiffrer un disque dur
***

Chiffrer toutes les données d'un appareil (ordinateur, téléphone, tablette) permet de rendre tous les fichiers illisibles en cas de perte ou de vol de ses appareils mais aussi de protéger ses contenus des attaques extérieures.

{{<block sain>}}
**Conseil:** si le chiffrement n'est pas mis en place à la réception de l'appareil et que le disque dur contient une grande quantité de fichiers, cela peut être très long et ne doit surtout pas être interrompu. Dans ce cas, le faire avant de se coucher et non si l'on compte utiliser son appareil dans l'heure.
{{</block>}}

{{<tabs "OS">}}

{{<tab "Windows, Linux">}}

{{<block info>}}

### Veracrypt | [veracrypt.fr](https://www.veracrypt.fr/)
***

– Tutoriel: [Veracrypt - Disques et conteneurs chiffrés](https://invidious.snopyta.org/watch?v=wtC_qz-RCiE) <span class="gris">31min 06s</span>

{{</block>}}
{{</tab>}}

{{<tab "macOS">}}

{{<block info>}}

### Filevault
***

Filevault est intégré dans macOS:  
Préférences Système > Sécurité et confidentialité > Filevault

– Tutoriel: [Comment crypter le disque interne d’un Mac](https://invidious.snopyta.org/watch?v=pOF-K53Vlmg) <span class="gris">10min 26s</span>

– Tutoriel: [Comment crypter un disque dur externe](https://invidious.snopyta.org/watch?v=tjxEuzP8Fmo) <span class="gris">6min 45s</span>

{{</block>}}

{{</tab>}}

{{<tab "Android">}}

{{<block info>}}

### Intégré
***

Pour l'activer:

1. Mettre en charge le téléphone jusqu'à la fin du processus.
2. Activer un mot de passe dans Sécurité > Verrouillage d'écran.
3. Puis Paramètres > Sécurité > Crypter le téléphone.
4. Lire et appuyer sur Crypter le téléphone pour lancer le processus.
5. Débrancher lorsque tout est fini.

N.B.: ce code n'a rien à voir avec celui de la carte sim.

{{</block>}}

{{</tab>}}

{{<tab "iOS">}}

{{<block info>}}

### Intégré
***

Pour l'activer:  
Réglages > Touch ID et code > Activer le code

Dès l'activation du code de verrouillage, pour le téléphone ou la tablette, tout le contenu de l'appareil sera chiffré.

N.B.: ce code n'a rien à voir avec celui de la carte sim.

{{</block>}}

{{</tab>}}

{{</tabs>}}


{{<hint danger>}}

**Attention:** Les fichiers ne sont plus chiffrés lorsqu'ils sont sortis du stockage (mis sur une clé USB, envoyé à un tiers,etc.).  
Il ne s'agit ici que d'un [chiffrement au repos](/docs/start/chiffrement/au-repos.html).

{{</hint>}}

{{< prev relref="/docs/outils/chiffrer" >}} < Chiffrer {{< /prev >}}
