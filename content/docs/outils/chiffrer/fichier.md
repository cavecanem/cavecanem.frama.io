---
title: "Fichier"
weight: 2
bookToc: false
summary: Pour chiffrer un fichier en particulier.
---

# Chiffrer un fichier

* * *

Chiffrer un fichier permet de le transporter ou de l'envoyer en toute sécurité.

{{<block info>}}

### Cryptomator | [cryptomator.org](https://cryptomator.org/)

* * *

Logiciel _open source_ multi-plateformes.

– Tutoriel: [Cryptomator - Chiffrez vos fichiers dans le cloud](https://invidious.snopyta.org/watch?v=uEftE5gLn1U) <span class="gris">16min 05s</span>

{{</block>}}

{{< prev relref="/docs/outils/chiffrer" >}} < Chiffrer {{< /prev >}}
