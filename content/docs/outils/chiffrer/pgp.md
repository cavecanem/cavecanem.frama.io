---
title: "PGP"
weight: 4
bookToc: false
summary: Chiffrer ses emails avec le protocole PGP.
---

# Chiffrement PGP
***

En anglais: _Pretty Good Privacy_.

C'est un système de [chiffrement de bout en bout](/docs/start/chiffrement/de-bout-en-bout.html) pour les emails délivrant les clés aux utilisateurs.

Il existe différents protocoles: OpenPGP est le plus courant et libre.

N'importe quel utilisateur peut communiquer avec PGP tant qu'il a soit:
-   Un fournisseur d'email sécurisé.
-   Une extension permettant de (dé)chiffrer le même protocole que son contact.

## Fonctionnement
***

Comme pour le chiffrement de bout en bout:

Une clé privée et une clé publique sont données à chaque utilisateur.
-   La clé privée qui déchiffre ne doit jamais être partagée.
-   La clé publique qui chiffre se partage.

1.  La clé privée est générée aléatoirement.
1.  La clé publique est générée à partir de la clé privée.
1.  Pour recevoir un message, j'envoie ma clé publique à Hugo.
1.  Hugo chiffre son message avec ma clé publique.
1.  Je déchiffre le message reçu grâce à ma clé privée.
1.  Moi seul peut prendre connaissance des messages qu'Hugo m'envoie.
1.  Il suffit qu'Hugo applique le même procédé pour que l'échange de nos clés publiques nous permette une communication bi-directionnelle sécurisée.

{{<hint danger>}}

**Attention:** Lorsque les clés sont générées et même si elles sont enregistrées par le logiciel, il faut absolument les stocker dans un endroit sûr > voir [mots de passe](/docs/outils/m
ots-de-passe.html).

Si la clé privée est perdue, tous les messages resteront chiffrés; <u>personne</u> ne pourra jamais récupérer la clé car personne ne la connaît !

{{</hint>}}

{{<block info>}}

### Fournisseurs d'emails sécurisés
***
Les [fournisseurs d'emails sécurisés](/docs/outils/emails/fournisseurs.html) permettent l'utilisation de PGP sans installation supplémentaire. De base, les courriels sont chiffrés entre les utilisateurs.

Pour les utilisateurs externes, ProtonMail et Tutanota créent un lien spécial, mais pour plus de praticité, on peut également récupérer ses clés PGP.

Leurs sites respectifs expliquent comment récupérer et utiliser ses clés avec leur service.

{{</block>}}

{{<block info>}}

### Mailvelope | [mailvelope.com](https://www.mailvelope.com/en/)
***
Extension pour navigateur permettant de chiffrer déchiffrer ses emails lorsqu'un utilisateur n'utilise pas de fournisseurs d'email sécurisé.

– Tutoriel: [mailvelope.com/fr/help](https://www.mailvelope.com/fr/help)  
– Tutoriel pas à pas très complet: [emailselfdefense.fsf.org](https://emailselfdefense.fsf.org/fr/)  
– Tutoriel: [Vidéo avec un son pourri](https://invidious.snopyta.org/watch?v=-a6LvEgROZY) <span class="gris">9min 07s</span>

{{</block>}}

{{<block info>}}

### Enigmail | [enigmail.net](https://www.enigmail.net/index.php/en/)
***

Extension pour Thunderbird > voir [emails > clients](/docs/outils/emails/clients.html) permettant de chiffrer déchiffrer ses emails lorsqu'un utilisateur n'utilise pas de fournisseurs d'email sécurisé.

– Tutoriel: [Chiffrez vos e-mails avec Thunderbird et Enigmail](https://invidious.snopyta.org/watch?v=gurLRymW-7c&list=PLHj4WuEG4h_qBNHCOdLCygf8iIwwPeQD7&index=4&t=0s) <span class="gris">18min 32s</span>

{{</block>}}

{{< prev relref="/docs/outils/chiffrer" >}} < Chiffrer {{< /prev >}}
