---
title: "DNS"
bookCollapseSection: true
bookToc: false
weight: 3
description: DNS est le système permettant d'échanger entre les serveurs et les appareils pour afficher un site web. Chaque requête interroge un annuaire répertoriant les adresses des serveurs. Cet entremetteur peut s'avérer être un outil de collecte de chaque site visité par un utilisateur.
---

# DNS
***

En anglais : _Domain Name System_ (système de nom de domaine)

C'est un vieux système à la base de la navigation en ligne.

Il est aujourd'hui devenu une énorme boîte noire de nos navigations sur Internet.

## Principe
***

Chaque site Internet est hébergé sur un serveur. Un serveur est un ordinateur dans un _data-center_. Les adresses des ordinateurs (serveurs ou ordinateurs personnels) sont attachés à une adresse IP, c'est comme son adresse postale mais sur Internet, elle est unique.

Le système DNS permet de transformer l'adresse IP d'un site Internet en un nom plus compréhensible pour simplifier la navigation.

Un serveur avec l'IP `194.187.168.99` devient `www.qwant.com` par ex.

Quand on souhaite accéder à n'importe quel site Internet, le navigateur envoie une requête à un annuaire (serveur DNS) pour connaître l'adresse IP du serveur qui correspond au nom du site. Ensuite il renvoie l'adresse IP sur le navigateur pour qu'il puisse se connecter au serveur du site.

{{< mermaid >}}
sequenceDiagram
  autonumber
    Hugo-->>Serveur DNS n°1: www.qwant.com
    Serveur DNS n°1-->>Hugo: 194.187.168.99
    Hugo->>194.187.168.99: Connexion au serveur de Qwant
{{< /mermaid >}}

Le résolveur DNS gère les requêtes avec ses serveurs. Il traite la demande et renvoie le résultat.

Donc chaque requête sur Internet passent par des serveurs DNS appartenant à des résolveurs DNS.

Il existe énormément de résolveurs DNS, avec de bonnes, pour certains, et de très mauvaises politiques ou intentions, pour d'autres.

– Explication vidéo : [DNS - La surveillance de masse facile](https://invidious.snopyta.org/watch?v=S1f4NB72lMQ) <span class="gris">16min 01s</span>

## Analogie
***

Dans une bibliothèque, je cherche un livre dont je connais le titre (adresse Web) mais pas sa référence qui permet de le trouver dans les étagères (adresse IP du site).

Pour trouver où il est rangé, j'utilise l'annuaire (serveur DNS) de la bibliothèque (résolveur DNS) pour connaître la référence du livre (adresse IP du site) et pouvoir le lire.

Si mes demandes sont adressées à haute voix plutôt que sur un bout de papier chiffré, n'importe qui peut entendre et tenir un registre complet de mes différentes consultations.

Si la bibliothèque décide de me censurer, l'annuaire me dira qu'il n'existe pas, ou bien, me re-dirigera vers un livre copié qui n'a pas le même contenu.

## Pourquoi changer de résolveur DNS ?
***

Les requêtes DNS sont actuellement effectuées en clair (pas de [chiffrement en transit](doc/chiffrement/en-transit.html)) ce qui pose de gros problèmes de confidentialité et de sécurité :

{{<hint danger>}}

### Avec le résolveur de son Fournisseurs d'Accès à Internet
***

-   Les gouvernements utilisent des [DNS menteurs](https://fr.wikipedia.org/wiki/Manipulation_de_l%27espace_des_noms_de_domaine) pour effectuer une censure.
-   Les pirates informatiques utilisent les routeurs infectés pour rediriger le trafic où ils veulent (sites copiés, hijacking etc.).
-   C'est l'annuaire de son FAI (Free, Proximus, Orange, etc.) qui est interrogé, donc tout ce qui est visité est enregistré et connu par son FAI.
-   Les FAI utilisent leurs serveurs DNS pour limiter l'accès en fonction de l'abonnement.
-   Les FAI tiennent (en France depuis 2018) un registre constant de toute la navigation Internet de chaque abonnement (Box ou téléphone).

{{</hint>}}

{{<block sain>}}

### Avec un résolveur sécurisé
***

-   On peut éviter les registres, la traque, la publicité, les malwares, le [hijacking](https://fr.wikipedia.org/wiki/Hijacking), la censure.
-   Le chiffrement en transit (DoH, DoT ou DNSCrypt) de ses requêtes entre soi et le serveur implique que personne d'autre que soi ou le serveur ne peut connaître le contenu des requêtes.
-   Utiliser un résolveur DNS sécurisé (DNSSEC) offre la garantie que le site demandé est bien celui affiché.
-   Utiliser un résolveur "sans filtres" permet d'avoir l'Internet le plus libre (pas de censure d'état et/ou de FAI).
-   Utiliser des filtres peut permettre de bloquer la publicité, les sites dangereux, les _malware_, les traqueurs, la télémétrie, etc.
-   Possibilité de bloquer les traqueurs des apps dans un téléphone ou une tablette.
-   Protéger ses enfants en bloquant l'accès aux sites explicites et pour adultes en choisissant un résolveur "family" sur les appareils qu'ils utilisent.
-   Également la vitesse d'affichage des pages peut augmenter considérablement.

{{</block>}}

## À noter
***

Les requêtes sont chiffrées <u>uniquement en transit</u>. Le résolveur doit pouvoir les lire pour les résoudre. En changeant de résolveur DNS, les requêtes sont reçues, lues et traitées par lui.

Il ne s'agit donc que d'un report de confiance, du  résolveur de son FAI, à un autre.

La sécurité réside dans la confiance accordée au résolveur choisi pour ne pas tenir de registre ou utiliser les informations personnelles.

Changer de résolveur se fait sur chaque appareil.

**Avec un VPN :**

Les serveurs du fournisseurs VPN sont utilisés. Le DNS du VPN passera toujours au-dessus de la configuration de l'appareil.

Son VPN doit donc être un prestataire digne de confiance > voir [VPN](/docs/outils/vpn.html).

## Contrôles
***

[dnsleaktest.com](https://dnsleaktest.com/) et [ipleak.net](https://ipleak.net/) vérifient qu'on utilise bien son serveur DNS et qu'il n'y a pas de fuite(s). Si l'on voit son FAI dans la liste c'est pas bon du tout.

{{< next relref="/docs/outils/dns/configuration" >}} Chiffrement DNS > {{< /next >}}
