---
title: "Chiffrement"
bookToc: false
weight: 1
---

# Chiffrement des requêtes DNS
***

Le chiffrement de ses requêtes DNS doit être réalisé sur chaque appareil (ordinateurs, téléphones, tablettes).

Si un [VPN](/docs/outils/vpn.html) est utilisé, la configuration sera prise en compte lorsque le VPN est coupé.

Le but est ici de changer le serveur par lequel transite toutes vos requêtes sur Internet et les chiffrer en utilisant DNSCrypt ou Dns-over-Https. Tous vos clics et tous les sites Web visités sont pour l'instant demandés en clair, utilisés et archivés par votre Fournisseur d'Accès à Internet.

{{<tabs "OS">}}

{{<tab "Windows">}}

## La manière recommandée
***

{{<block info>}}

### Simple DNSCrypt | [simplednscrypt.org](https://simplednscrypt.org/)
* * *
Télécharger, installer, configurer.

– Tutoriel: [DNSCrypt - Protégez-vous de l'espionnage/censure des DNS](https://invidious.snopyta.org/ttprp24ksaU?t=902) <span class="gris">10min 37s (à partir de 15min jusqu'à 23min 37s)</span>

Dans cette vidéo, il utilise le résolveur ipredator mais ce n'est pas obligé. Cette [liste de résolveurs](liste-de-resolveurs.html) propose des recommandations.

À noter aussi qu'il force le TCP. C'est souvent plus lent et les avis sur la sécurité ajoutée sont mitigés. Ce n'est pas forcément à faire.

Un fichier costaud permettant de bloquer la télémétrie, la publicité et les sites dangereux peut être utilisé afin de renforcer les filtres des résolveurs > Télécharger {{< dl "blacklist.txt" "/files/blacklist.txt" >}}.

{{</block>}}

## La manière plus simple
***

{{<block info>}}

Cette technique est informative et à utiliser en cas d'échec de la manière recommandée ci-dessus.

1.  Aller sur [dnsleaktest.com](https://dnsleaktest.com/). Faire un test, il devrait apparaître votre FAI.
2.  Menu Démarrer > Paramètres > Réseau et Internet > Ethernet > Modifier les options d'adaptateur > Ethernet ou Wi-Fi (réseau(x) utilisé(s), ou faire pareil pour les deux) > Clic droit > Propriétés > Décocher Protocole Internet Version 6 > Clic sur Protocole Internet version 4 > Propriétés > Utiliser l'adresse de serveur DNS suivante > Taper `176.103.130.130` en préférée et `176.103.130.131` en secondaire.
3.  Cocher Valider les paramètres en quittant > OK > Fermer

Ensuite pour chiffrer les requêtes, dans Firefox <u>(uniquement!)</u>:

1.  Firefox > Préférences > Général > Paramètres réseaux (en bas) > Paramètres > Cocher: Activer le DNS par HTTPS > Choisissez Custom > Taper `https://dns.adguard.com/dns-query`
2.  Vérifier qu'il n'y pas plus son FAI en relançant [dnsleaktest.com](https://dnsleaktest.com/)

Le résolveur AdGuard et le serveur "default" est désormais utilisé.

Il n'est pas toujours facile de choisir un autre résolveur de cette manière, de plus les requêtes ne seront chiffrés que dans Firefox et dans aucune autre application ayant accès à Internet.

{{</block>}}

{{</tab>}}

{{< tab "macOS" >}}

## La manière recommandée
***

Cette technique utilise quelques lignes de commande (CLI) dans le Terminal. C'est très facile mais inhabituel pour la plupart des personnes donc il faut être attentif. Il n'existe pas encore de logiciel avec une interface simple sur macOS pour chiffrer ses DNS. Veiller à toujours copier les espaces et les majuscules.

N.B: La session "administrateur" est la session principale, celle qui contrôle l'ordinateur. Si il y a plusieurs sessions sur l'ordinateur, le mot de passe de l'administrateur sera nécessaire. (Si il n'y a pas de mot de passe, appuyer juste sur la touche Entrée le moment venu ou créer un mot de passe avant de commencer.)

{{<block info>}}

1.  Aller sur [dnsleaktest.com](https://dnsleaktest.com/). Faire un test, il devrait apparaître votre FAI.
2.  Télécharger [DNSCrypt-proxy](https://github.com/DNSCrypt/dnscrypt-proxy/releases). Dernière version et celle pour macOS (ex. [dnscrypt-proxy-macos-2.0.44.zip](https://github.com/DNSCrypt/dnscrypt-proxy/releases/download/2.0.44/dnscrypt-proxy-macos-2.0.44.zip) )
3.  Décompresser le fichier .zip dans /Applications/ et renommer le dossier en `dnscrypt-proxy`
4.  Télécharger et placer les 3 fichiers suivant dans le dossier /Applications/dnscrypt/
    1.  {{< dl "dnscrypt-proxy.toml" "/files/dnscrypt-proxy.toml" >}}
    2.  {{< dl "blacklist.txt" "/files/blacklist.txt" >}} (bloque une quantité de télémétrie, pub, traqueurs, sites dangereux, etc.)
    3.  {{< dl "whitelist.txt" "/files/whitelist.txt" >}} (pour accéder quand même à un site s'il est bloqué par la blacklist personnelle ou du résolveur DNS)
5.  Ouvrir Préférences Système > Réseau > Wi-Fi ou Ethernet (réseau(x) utilisé(s), ou faire pareil pour les deux) > Avancé > DNS > Serveurs DNS > (sauvegarder les adresses si besoin) > `–` à tout > puis `+` > taper: `127.0.0.1` > OK > Appliquer
6.  Ouvrir Applications > Utilitaires > Terminal
7.  Taper `sudo -s` > Touche Entrée
8.  Taper le mot de passe de la session administrateur (c'est invisible mais ça écrit) > Touche Entrée
9.  Taper `cd /Applications/dnscrypt-proxy` > Touche Entrée (si jamais le dossier Applications à un autre nom remplacer-le (avec majuscules si il y a))
10.  Taper `./dnscrypt-proxy` > Touche Entrée
11.  Doit apparaître quelque chose comme `[NOTICE] dnscrypt-proxy is ready - live servers: 3`
12.  C'est connecté, ne pas fermer le Terminal.
13.  Vérifier que ça marche bien en allant un peu sur Internet.
14.  Aller sur [dnsleaktest.com](https://dnsleaktest.com/) et vérifier que votre FAI n'est plus dans la liste.
15.  Si c'est bon, appuyer sur les touches `CTRL + C` dans le Terminal pour stopper DNSCrypt.
16.  Taper `./dnscrypt-proxy -service install` > Touche Entrée (pour qu'il démarre à chaque fois que l'ordi s'allume).
17.  Puis `./dnscrypt-proxy -service restart` > Touche Entrée (pour le redémarrer).

N.B.: Il se peut qu'il mette quelques secondes au démarrage ou en sortie de veille de l'ordi pour accéder à Internet.

{{</block>}}

### Si ça ne marche pas

1.  Dans le Terminal, appuyer sur les touches `CTRL + C` pour arrêter le processus.
2.  Si le Terminal est fermé: Ouvrir Applications > Utilitaires > Moniteur d'activité > Chercher `dnscrypt-proxy` dans la barre de recherche > cliquer dessus > X en haut à gauche pour quitter le processus.
3.  Reprendre toute la manip en respectant bien les espaces dans les lignes de commandes.

### Si ça ne marche toujours pas

1.  Se faire aider par quelqu'un ou chercher sur le net [une solution](https://github.com/DNSCrypt/dnscrypt-proxy/wiki/Installation).
2.  Sinon abandonner..
    1.  Appuyer sur les touches `CTRL + C` dans le Terminal > taper `exit` > Entrée.
    2.  Si le Terminal est fermé: Applications > Utilitaires > Moniteur d'activité > Chercher `dnscrypt-proxy` dans la barre de recherche > cliquer dessus > `X` en haut à gauche pour quitter le processus.
    3.  Supprimer le dossier dnscrypt dans Applications et vider la corbeille.
3.  Retournez dans Préférences Système et enlever le DNS `127.0.0.1` en appuyant sur `–`. Les anciens numéros sauvegardés au début réapparaissent, sinon les réécrire.
4.  Utiliser la méthode suivante et n'utiliser que Firefox pour la navigation.

## La manière plus simple
***

{{<block info>}}
Cette technique est informative et à utiliser en cas d'échec de la manière recommandée ci-dessus.

1.  Aller sur [dnsleaktest.com](https://dnsleaktest.com/). Faire un test, il devrait apparaître votre FAI.
2.  Ouvrir Préférences Système > Réseau > Ethernet ou Wi-Fi (réseau(x) utilisé(s), ou faire pareil pour les deux) > Avancé > DNS
3.  Serveurs DNS > `–` à tout > Puis: `+` > Taper `176.103.130.130` > + > `176.103.130.131` > Ok
4.  \> Appliquer

Ensuite pour chiffrer les requêtes, dans Firefox <u>(uniquement!)</u>:

1.  Firefox > Préférences > Général > Paramètres réseaux (en bas) > Paramètres > Cocher: Activer le DNS par HTTPS > Choisissez Custom > Taper `https://dns.adguard.com/dns-query`
2.  Vérifier que ce sont les serveurs ci-dessus qui sont interrogés sur [dnsleaktest.com](https://dnsleaktest.com/).

Le résolveur AdGuard et le serveur "default" est désormais utilisé.

Il n'est pas toujours facile de choisir un autre résolveur de cette manière, de plus les requêtes ne seront chiffrés que dans Firefox et dans aucune autre application ayant accès à Internet.

{{</block>}}

{{</tab>}}

{{< tab "Linux" >}}

{{<block info>}}

### Simple DNSCrypt | [simplednscrypt.org](https://simplednscrypt.org/)
***

Télécharger, installer, configurer.

– Tutoriel: [DNSCRYPT - Protégez-vous de l'espionnage/censure des DNS](https://invidious.snopyta.org/ttprp24ksaU?t=1422) <span class="gris">14min (à partir de 23min 42s)</span>

{{</block>}}

Dans cette vidéo, il utilise le résolveur ipredator mais ce n'est pas obligé. Cette [liste de résolveurs](liste-de-resolveurs.html) propose des recommandations.

À noter aussi qu'il force le TCP. C'est souvent plus lent et les avis sur la sécurité ajoutée sont mitigés. Pas forcément à faire.

Un fichier costaud permettant de bloquer la télémétrie, la publicité et les sites dangereux peut être utilisé afin de renforcer les filtres des résolveurs > télécharger {{< dl "blacklist.txt" "/files/blacklist.txt" >}}


{{</tab>}}

{{< tab "Android" >}}

{{<block info>}}

### Nebulo | [F-Droid](https://fdroid.frostnerd.com/fdroid/repo?fingerprint=74BB580F263EC89E15C207298DEC861B5069517550FE0F1D852F16FA611D2D26) | [Play Store](https://play.google.com/store/apps/details?id=com.frostnerd.smokescreen)
***

1.  Installer et configurer.
2.  Nebulo ressemble à DNSCloak pour iOS, la configuration doit être similaire.
3.  Choisir les résolveurs [listés ici.](liste-de-resolveurs.html)
4.  Si une blacklist est utilisable, télécharger {{< dl "blacklist.txt" "/files/blacklist.txt" >}} et la choisir. Cela bloquera les pubs, la télémétrie, les traqueurs, les sites dangereux, sur le téléphone et dans les apps.
5.  Cave Canem manque de documentation, si besoin voir le [Git de Nebulo](https://git.frostnerd.com/PublicAndroidApps/smokescreen/blob/master/README.md).

{{</block>}}
{{</tab>}}

{{< tab "iOS" >}}

Télécharger {{< dl "blacklist.txt" "/files/blacklist.txt" >}} et le mettre sur le téléphone (Documents, Cloud ou autre). Optionnel mais recommandé pour renforcer le bloquage de la publicité et des traqueurs dans les app et le reste du téléphone. DNSCloak agira ensuite comme un pare-feu.

{{<block info>}}

### DNSCloak | [App Store](https://itunes.apple.com/fr/app/dnscloak-dnscrypt-client/id1452162351)

* * *

1.  Installer et ouvrir
2.  Paramètres (3barres en haut à gauche)
3.  General options > Connect on Demand > Cocher ; Cache response > Cocher
4.  Blacklists & Whitelist > Enable Blacklist > Sélectionner le fichier blacklist.txt à son emplacement.
5.  Advanced options > Strict mode > Cocher ; Block IPv6 > Cocher
6.  Retour
7.  Dans "Search" taper les différents résolveurs à trouver de cette [liste](liste-de-resolveurs.html)
8.  Puis pour chacun d'entre eux: Toucher > Utiliser.

{{</block>}}

{{</tab>}}

{{</tabs>}}

{{< prev relref="/docs/outils/dns" >}} < DNS {{< /prev >}}
{{< next relref="/docs/outils/dns/options-de-serveur" >}} Options de serveur > {{< /next >}}
