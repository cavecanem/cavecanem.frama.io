---
title: "Liste de résolveurs"
weight: 7
bookToc: false
---

# Liste de résolveurs
***

Les résolveurs proposés ici proposent des serveurs vraisemblablement plus sûrs que celui de votre FAI actuel et sont régulièrement recommandés.

{{<hint danger>}}

**Rappel:** Les FAI (depuis 2018 en France) enregistrent sans limite toutes les requêtes Internet et retiennent <u>tout</u> ce qui est visité sur Internet dans un registre.

**Poubelle:** Son FAI, Cloudflare, NextDNS, OpenDNS, Quad9, GoogleDNS, etc.

{{</hint>}}

{{<hint warning>}}

Éviter d'utiliser l'ipv6 (ralentissements, stabilité et sécurité laissant à désirer)

{{</hint>}}

{{<block info>}}

### Résolveurs recommandés
***
-   [**BlahDNS**](https://blahdns.com/) (DoH / DNSCrypt / DoT) Allemagne, Finlande, Japon, Singapour  
No logs | DNSSEC | Filters ads + trackers + malwares
-   [**cz.nic**](https://www.nic.cz/odvr/) (DoH / DoT) Prague, République Tchèque
-   [**Brahma-world**](https://dns.brahma.world) (DoH / DoT) Allemagne - Nuremberg  
No logs | DNSSEC | Filters ads + trackers + malware domains
-   [**Lelux.fi**](https://lelux.fi/resolver/) (DoH) Luxembourg, Paris, France  
No logs | DNSSEC | No filter
-   [**Applied Privacy**](https://appliedprivacy.net) (DoH / DoT) Vienne, Autriche  
No logs | DNSSEC | No filter
-   [**Snopyta**](https://snopyta.org) (DoH / DoT) Finlande  
No logs | DNSSEC

{{</block>}}

Chercher ces noms dans les applications DNSCrypt, DNSCloak ou Nebulo, et se connecter à leurs serveurs.

Sur macOS, ces serveurs sont utilisés dans la configuration .toml pour DNSCrypt-proxy. À télécharger dans Configurations > Méthode recommandée.  
Pour ignorer cette configuration et régler la liste soi-même > [GitHub dnscrypt-proxy/wiki](https://github.com/DNSCrypt/dnscrypt-proxy/wiki/Configuration).

{{< prev relref="/docs/outils/dns/options-de-serveur" >}} < Options de serveur {{< /prev >}}
