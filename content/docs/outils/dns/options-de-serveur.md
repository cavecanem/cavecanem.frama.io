---
title: "Options de serveur"
weight: 6
bookToc: false
---
# Options de serveur
***

Chaque résolveur propose à ses serveurs DNS une série particulière de règles, d'options qu'il est bon de connaître pour choisir ensuite un serveur en adéquation avec les résultats que l'on souhaite obtenir.

### DoH (DNS over HTTPS)
***

Le trafic DNS est chiffré par le protocole HTTPS.

### DNSCrypt
***

Le trafic DNS est chiffré par le protocole DNSCrypt.

### DoT (DNS over TLS)
***

Le trafic DNS est chiffré par le protocole TLS. (plus complexe à mettre en place)

### DNSSEC
***

DNSSEC pour DNS Sécurisé, est un label délivrant une chaîne de confiance qui authentifie la véracité des sites Internet.

### No logs (sans registres)
***

Aucun registre des requêtes n'est tenu. Il n'y pas de moyen de le vérifier, il s'agit uniquement de confiance.

### Filters Ads, Trackers, Malware
***

Filtre les publicités, les traqueurs, et les sites connus pour utiliser des _malware_ sur leurs utilisateurs.

### No filters (sans filtres)
***

Les sites Web ne souffrent d'aucune censure morale, d'état ou de FAI, tout est affiché, sans filtres.

### Family (Famille)
***

Bloque l'ouverture des sites explicites, violents ou pour adultes. Utile pour un appareil utilisé par un enfant par exemple.

### La localisation du DNS
***

-  À choisir en dehors des USA et autres pays appliquant des [lois de divulgation des clés](/docs/start/lois-divulgation.html).
-  Plus le serveur est proche de sa position géographique, plus rapide sera la navigation.

{{<block sain>}}

### Pratique saine

* * *

Opter pour un serveur DNS avec les options:

**DoH** _ou_ **DNSCrypt** + **DNSSEC** + **No-log** + **Filters Ads**, **Trackers**, **Malware** _ou_ **No-filters** _ou_ **Family** + **En dehors des USA** + **Proche de chez soi**

{{</block>}}


{{< prev relref="/docs/outils/dns/configuration" >}} < Chiffrement DNS {{< /prev >}}
{{< next relref="/docs/outils/dns/liste-de-resolveurs" >}} Liste de résolveurs > {{< /next >}}
