---
title: "Emails"
bookToc: false
bookCollapseSection: true
weight: 60
description: L'email est une technologie non sécurisée permettant de communiquer entre utilisateurs. Choisir un service sécurisé ne devrait pas être optionnel. Quels sont les services qui respectent la confidentialité des messages de leurs utilisateurs ?
---

# Emails
***

Un fournisseur de service email est un choix primordial quand il s'agit de confidentialité et de vie privée aujourd'hui.

Un fournisseur "gratuit" (Gmail, Outlook, Yahoo, AOL, Apple, Microsoft etc.) se finance avec la récupération légale des données de ses utilisateurs et la revente de ses renseignements. Le gouvernement américain à un accès dans chacune d'elles pour récupérer toutes les données et contacts des utilisateurs du monde entier.

## Avec un service classique
***

-  Des traqueurs pistent l'utilisateur (clics, navigation, heure, temps, actions, etc.).
-  Des robots analysent le contenu des contacts, des calendriers.
-  Des robots analysent les messages et fichiers (textes, images, audios, etc.).
-  Les métadonnées obtenues sont fichées, stockées puis revendues à des tiers.
-  Utilisation de la publicité (et la plus ciblée possible).
-  Utilise des _backdoors_ (portes dérobées) laissant les services américains entrer et collecter les données.
-  Accepte sur demande d'ouvrir à des tiers les boîtes email.


## Trois exemples
***

1.  [Gmail a été pris en flagrant délit](https://www.iiro.eu/vie-privee-gmail/) de don à des tiers d'un accès complet aux courriels des utilisateurs.
2.  Des documents déclassifiés du programme de surveillance PRISM révèlent qu’Apple, Microsoft, Yahoo, Google et AOL donnent aux organismes de surveillance américains un accès unilatéral à leurs serveurs pour effectuer « une surveillance approfondie des communications en direct et des informations stockées ».
3.  [Yahoo a également été attrapé](https://thehackernews.com/2016/10/yahoo-email-hacking.html) en train de scanner des emails en temps réel pour des agences de surveillance américaines en 2016.

## Attaques par email
***
Le _phishing_ (hameçonnage) est une technique à l'ancienne encore très utilisée. Elle consiste à imiter l'adresse et l'email d'un service (banque, assurance maladie, impôts, Ebay, Netflix etc.) pour pousser l'utilisateur à cliquer sur un lien amenant à un site copié du service.

En entrant ses données sur le site, l’utilisateur les offre inconsciemment aux fraudeurs.

Il arrive également, que ce soit une personne fictive qui nous contacte, l'idée est la même.

La parade mise en place par les fournisseurs d'email consiste en un _anti-spam_ puissant qui référence les adresses email frauduleuses et les place directement dans un dossier "indésirables". Cependant certains mails passent parfois entre les mailles du filet et peuvent atterrir dans la boîte mail principale.

Il faut donc toujours vérifier soi-même que l'adresse email est bien l'adresse officielle du service. Si besoin, le vérifier sur le site officiel ou téléphoner au service avant d'ouvrir ou de cliquer sur le contenu de l'email.

Bloquer le chargement automatique des fichiers et des images dans les emails limite également l'ouverture de virus ou programmes espions qu'ils peuvent contenir.

## Pollution des emails
***

Les emails créent une pollution gigantesque. Chaque email étant copié au minimum deux fois (1 par utilisateur) sur un serveur, les besoins de stockage et d'énergie augmentent de façon exponentielles.

– Voir [E-mail, cloud, data center: ces clics qui polluent](https://invidious.snopyta.org/watch?v=JvwiMNcg9sw) <span class="gris">3min 33s</span>

<u>Tous les fichiers en ligne</u> (posts des réseaux sociaux, messages instantanés, emails, vidéos, Cloud etc.) sont stockés sur des serveurs. S'ils ne sont pas supprimés, ils demandent des ressources énergétiques. Si Internet était un pays aujourd'hui, il serait le 3<sup>ème</sup> plus gros pollueur du monde.

Supprimer régulièrement ses fichiers numériques devenus inutiles est bon pour l'environnement et limite la possibilité qu'ils soient utilisés par des tiers.

{{<block sain>}}

### Pratique Saine
***

-  Favoriser un fournisseur qui pratique le respect de la vie privée de ses utilisateurs.
-  Toujours utiliser le [chiffrement de bout en bout](chiffrement/de-bout-en-bout.html) ou [PGP](chiffrement/manuel/pgp.html) pour envoyer des documents et des messages contenant des informations privées.
-  Ne jamais envoyer de mots de passe par email > voir [ici](partage-de-fichiers/textes.html).
-  Choisir de ne pas charger les images automatiquement, le faire manuellement quand on a vérifié l'adresse de l'expéditeur.
-  Opter pour un nettoyage régulier des emails inutiles. Ce stockage consomme énormément d'énergie pour rien. Supprimer ces emails limite également la fuite de ses données.
-  Ne jamais donner à qui que ce soit son adresse email principale.
-  Utiliser toujours un [alias](/docs/outils/emails/alias.html) et partitionner ses activités (travail, famille, réseaux sociaux, _shopping_, etc.).
-  <u>Payer pour ne pas être le produit</u>. On utilise une boîte email quotidiennement, les prix sont vraiment abordables et les infrastructures ne fonctionnent pas gratuitement.
-  Payer aussi pour soutenir les bonnes pratiques. Gmail, Outlook, et consorts, ne sont pas gratuits. Ils se rétribuent financièrement grâce au fichage, à l'utilisation et à la revente de la vie personnelle de leurs utilisateurs.

{{</block>}}

{{<hint warning>}}

### Garder son service classique ?
***

Cela contribue à soutenir les dérives actuelles autour des données personnelles et celles de ses contacts et au monopole des GAFAM ainsi qu'à leur modèle économique néfaste.

Dans ce cas néanmoins, veiller à protéger <u>toutes</u> les données sensibles envoyées (messages, carte d'identité, facture, passeport, etc.).

-   [Chiffrer](/docs/outils/chiffrer.html) ses fichiers avant envoi.
-   Utiliser un service de [partage de fichiers](/docs/outils/partage-de-fichiers.html) chiffré de bout en bout.
-   Utiliser [PGP](/docs/outils/chiffrer/pgp.html).

Cela n'empêchera pas à Google ou Microsoft d'utiliser vos métadonnées.

{{</hint>}}

{{< next relref="/docs/outils/emails/fournisseurs" >}} Fournisseurs d'email > {{< /next >}}
