---
title: "Alias"
weight: 2
bookToc: false
description: À quoi servent les alias d'adresse email et comment les utiliser ?
---
# Alias
***

Peu de personne utilise et connaît l'utilité des alias et des redirections.

Un alias permet de garder son adresse email confidentielle en créant des redirections. En ne donnant que des alias, on peut compartimenter ses différentes activités sur Internet tout en protègeant sa véritable adresse email des attaques publicitaires ou de piratage.

Certaines personnes utilisent une boîte email "poubelle" pour récupérer les newsletters ou les inscriptions sur des sites.

Un alias ou une redirection est plus utile que la création d'un nouveau compte car les mails sont gérés depuis l'adresse principale, ce qui est beaucoup moins fastidieux.  
Il aide grandement à faire le tri dans sa boîte mail pour trouver et supprimer les emails inutiles et protège aussi contre les _spams_ et publicités le compte principal. Il peut ensuite permettre de trouver rapidement d'où provient une fuite en ligne.

Si un alias ou une redirection est utilisé uniquement pour Facebook par exemple, il sera facile de connaître la source des _spams_ ou publicités qui polluent sa boîte mail et donc d'y remédier en changeant d'alias et sans supprimer son compte principal.

## Exemples d'utilisation
***

Adresse principale: contact\@andreamartin.com

-  Alias pour la famille et les amis : hello\@andreamartin.com
-  Alias pour les achats en ligne : achats\@andreamartin.com
-  Alias pour newsletter : newsletter\@andreamartin.com
-  Alias pour réseaux sociaux : social\@andreamartin.com
-  Alias pour son travail : dentiste\@andreamartin.com

Ainsi:

-  Toutes ces adresses sont accessibles depuis le compte principal.
-  Le courrier est géré dans le compte principal.
-  On choisit quel alias utiliser pour écrire ou répondre.
-  On choisit un alias pour trier facilement et supprimer les messages inutiles.
-  On désactive un alias pour couper court à la liste de diffusion associée et ne plus recevoir d'emails.
-  Si un des contacts est piraté, notre alias ne corrompt pas notre boîte email principale.

{{<hint warning>}}

**Attention :** On ne peut souvent pas supprimer un alias sans supprimer son compte email; On peut seulement le désactiver. Il faut donc bien choisir son nom et sa fonction avant de le créer.  
Le mieux est de garder les alias fournis avec sa boîte mail pour le professionnel ou le courant et d'utiliser des outils de redirections pour les formulaires d'inscriptions en ligne par exemple.

{{</hint>}}

{{<block info>}}

### SimpleLogin | [simplelogin.io](https://simplelogin.io/)
***
SimpleLogin est un outil _open source_ qui aide à protéger son adresse email en offrant des alias permanents.

Version gratuite :

-  Jusqu'à 15 alias.
-  Pour 1 adresse email.
-  Réception et envoi d'emails depuis les alias.
-  Bande passante illimitée.
-  Application Android et iOS et extension pour navigateurs.

{{</block>}}

{{<block info>}}

### AnonAddy | [anonaddy.com](https://anonaddy.com/)
***
AnonAddy est un très bon outil _open source_ permettant de créer des redirections.

Version gratuite :

-  Alias standards illimités.
-  20 alias non liés à son pseudo d'inscription (pour plus d'anonymat).
-  Pour 2 adresses email.
-  Réception et envoi d'emails depuis les alias.
-  Avec une limitation de 10MB/mois de bande passante (ce qui est peu).
-  Extension pour navigateurs mais pas d'application mobile.

{{</block>}}

{{<block info>}}

### EmailEngine | [emailengine.io](https://www.emailengine.io/)
***
Ce service permet de créer une quantité illimité d'alias pour ses boîtes emails. Ce service est plutôt fait pour les entreprises et pour les personnes ayant un compte sur Gmail, Outlook, Aol, Yahoo, etc.
Il est moins cher d'utiliser les alias fournis par son fournisseurs email pour les particuliers, ou d'en acheter en suppléments.

-  5$ / mois

{{</block>}}

{{< prev relref="/docs/outils/emails/fournisseurs" >}} < Fournisseurs d'emails {{< /prev >}}
{{< next relref="/docs/outils/emails/clients" >}} Client d'emails > {{< /next >}}
