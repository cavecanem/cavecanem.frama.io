---
title: "Clients email"
weight: 3
bookToc: false
---
# Clients email

* * *

La plupart des internautes utilisent aujourd'hui un _Webmail_ (application en ligne) pour gérer ses emails.

Un client email est un programme qui permet de gérer sa boîte mail de la même manière qu'un _Webmail_, mais sur son ordinateur ou téléphone.

Il permet à l'utilisateur de consulter, d'écrire ou d'envoyer ses emails sans se connecter au site Web. Le client email connecte l'utilisateur directement au serveur.

**Avantages:**
-  Il n'y plus besoin de l'app ou du site Internet pour gérer ses emails.
-  Si l'on n'est pas connecté à Internet, on peut quand même accéder aux emails déjà récupérés, n'importe où, sans dépendre d'une connection.

## Protocoles
***

Les clients d'email utilisent principalement les protocoles de récupération: IMAP et POP3. Ces deux protocoles ont leurs avantages et inconvénients, il faut donc bien choisir celui le plus approprié à son utilisation.

### POP3
***
Télécharge les emails en les supprimant du serveur. Il sont enregistrés uniquement sur l'appareil qui l'aura récupéré en premier.

Si c'est l'ordinateur qui récupère le message, il l'enregistre, et le supprime du serveur. Il ne sera pas disponible sur le téléphone ou par _Webmail_. Si l'on supprime un email, vu qu'il n'existe que sur l'appareil, il sera définitivement supprimé.

Le temps de suppression du serveur après récupération via POP3 peut être réglé dans les paramètres du _Webmail_.

Le POP3 reste un protocole rapide et robuste, particulièrement utile si l'on ne consulte son courriel qu'à partir d'un seul appareil.

### IMAP
***
Permet de synchroniser ses emails avec le serveur et tous ses appareils. Les emails sont téléchargés sur l'appareil mais pas supprimés du serveur.

Tout ce qui est fait sur un appareil (lectures, suppressions, création de dossier, rangements, etc.) sera fait sur le serveur, et également sur tous les autres appareils instantanément.

L'IMAP permet donc une facilité de synchronisation entre tous ses appareils.

{{<tabs "OS">}}

{{<tab "Windows, macOS, Linux">}}

{{<hint danger>}}**Poubelle:** Apple Mail, Outlook Express, Windows Mail, etc.{{</hint>}}

{{<block info>}}

### ThunderBird | [thunderbird.net](https://www.thunderbird.net/fr/)
***

Client email _open source_.

– Tutoriel d'installation: https://invidious.snopyta.org/watch?v=MUfCMfbjaVw

##### Enigmail | [enigmail.net](https://www.enigmail.net/index.php/en/)
***
Cette extension ajoute [OpenPGP](/docs/outils/chiffrer/pgp.html) pour (dé)chiffrer ses mails depuis Thunderbird.
– Tutoriel: [Chiffrez vos e-mails avec Thunderbird et Enigmail](https://invidious.snopyta.org/watch?v=gurLRymW-7c&list=PLHj4WuEG4h_qBNHCOdLCygf8iIwwPeQD7&index=4&t=0s) <span class="gris">18min 32s</span>

Enigmail sera bientôt entièrement intégré à Thunderbird dans la version 78 courant été 2020. [Source](https://blog.thunderbird.net/2019/10/thunderbird-enigmail-and-openpgp/)


{{</block>}}

{{<hint warning>}}

**Attention:** Les emails sont désormais synchronisés ou enregistrés sur l'ordinateur. Pour les protéger des intrusions, il faut [chiffrer au repos](/docs/start/chiffrement/au-repos.html) son disque dur.

{{</hint>}}

{{</tab>}}

{{<tab "Android">}}

{{<hint danger>}}**Poubelle:** Gmail, Outlook Express, Windows Mail, etc.{{</hint>}}

{{<block info>}}

### K-9 Mail | [k9mail.app](https://k9mail.app/)
***

Client email indépendant et _open source_.

{{</block>}}

{{</tab>}}

{{<tab "iOS">}}
{{<hint danger>}}**Note:** Thunderbird n'est pas encore développé pour les mobiles{{</hint>}}

-  Privilégier l'application de Protonmail ou de Tutanota.
-  Si besoin d'un client, Apple Mail est déjà installé. Il n'est pas recommandé, mais il n'y a pas d'alternatives non propriétaires et _open source_ actuellement.

{{</tab>}}
{{</tabs>}}

## Environnement
***

En utilisant un _Webmail_, avec un navigateur ou avec une [application mobile](/docs/outils/applications.html), les emails restent stockés uniquement sur le serveur du service (en Cloud). À chaque clic pour ouvrir un dossier ou un email, cela génère du trafic Internet: le navigateur (ou l'app) appelle le serveur, puis le serveur renvoie l'information.

Cela semble être instantané mais en réalité, la demande à déjà traversé une dizaine de serveurs relais et plusieurs pays pour arriver au service et revenir à l'utilisateur.

Le trafic Internet génère une demande considérable d'énergie: de l'électricité consommée par les serveurs au refroidissement par climatisation des data-center.

On prend le nombre de clics sur le site de la boîte mail d'un utilisateur et on le multiplie par le nombre d'utilisateurs dans le monde qui ont la même pratique est le résultat est un gaspillage d'énergie colossal et complètement aux antipodes d'une lutte écologique.

Avec un client d'email, les emails seront téléchargés ou synchronisés depuis le serveur sur l'appareil une fois. Quand on les consulte, on ne génère pas de trafic supplémentaire inutile et on utilise moins son forfait Internet.

{{<block sain>}}

### Pratique saine
***

-  Utiliser un client pour éviter un gaspillage d'énergie et d'argent.
-  Chiffrer son disque dur au repos quand on utilise un client.
-  Ne pas enregistrer son mot de passe sur l'ordinateur. L'écrire à chaque ouverture, pour verrouiller l'accès à ses emails.
-  Le client peut également proposé un mot de passe supplémentaire à l'ouverture si besoin.

{{</block>}}

{{< prev relref="/docs/outils/emails/alias" >}} < Alias {{< /prev >}}
{{< next relref="/docs/outils/emails/suppression-mail" >}} Supprimer son compte > {{< /next >}}
