---
title: "Fournisseurs"
weight: 1
bookToc: false
---
# Fournisseurs d'emails sécurisés

* * *

Choisir un fournisseur d'emails sécurisé est également un geste essentiel lorsque l'on tient à préserver sa vie privée.

Il en va aussi bien pour les particuliers que pour les entreprises.

{{<hint danger>}}

Pour sa propre protection des données, une entreprise ne doit jamais utiliser autre chose qu'un fournisseur sécurisé pour ses échanges.

{{</hint>}}

## Les fournisseurs listés ici

* * *

-  Opèrent dans des pays qui n'appliquent pas [la loi de divulgation des clés](/docs/start/lois-divulgation.html).
-  ProtonMail opère, en plus, en dehors de toute [alliance de surveillance](/docs/start/alliances.html).
-  Utilisent les protocoles de chiffrement en transit SSL TLS.
-  N'utilisent pas la vente des données personnelles pour se financer.
-  N'analysent pas les fichiers (règles de confidentialité strictes).
-  Ne stockent pas les adresses IP lors d'envoi d'email.
-  Chiffrent au repos les emails, contacts et calendriers.
-  Ajoutent la possibilité de chiffrer les messages et d'utiliser PGP.
-  Sont, dans leur ensemble, quasiment 100% _open source_.
-  MailBox propose un ensemble Cloud et Bureau virtuel comme Gmail ou Outlook si besoin de centraliser ce genre de services.

## À savoir:

* * *

Le (dé)chiffrement ne fonctionne pas si votre contact est sur Gmail, Outlook, Yahoo, AOL, Free etc. Néanmoins, il peut être utilisé [PGP](/docs/outils/chiffrer/pgp.html) sinon un lien est spécialement généré par le fournisseur que le contact pourra utilisé pour échanger en sécurité (ProtonMail et Tutanota le font).

Le déchiffrement des données ne fonctionne que si le destinataire utilise le même protocole de chiffrement que l'expéditeur (OpenPGP par ex).

Tutanota utilisant un protocole propriétaire et ProtonMail et MailBox utilisant OpenPGP, ils ne peuvent pas lire les emails chiffrés mutuellement.  
Ils peuvent toutefois échanger des emails non sécurisés. Le (dé)chiffrement peut se faire par un lien spécialement créé ou par une extension PGP.

Tutanota semble moins versatile.

## À noter:

* * *

-  4€/mois c'est le prix d'une bière... et c'est 48€/an.
-  1€/mois c'est le prix d'une baguette... et c'est 12€/an.
-  Possibilité d'essayer avant d'acheter (période ou version gratuite).
-  ProtonMail et MailBox proposent un service de migration pour copier votre ancienne boîte mail et vous faire suivre les nouveaux emails (pas les versions gratuites et en prévision pour Tutanota).

{{<hint danger>}}
**Poubelle:** Gmail, Outlook, Yahoo, AOL, Free, Apple, Microsoft etc.
{{</hint>}}

{{<block info>}}

### ProtonMail | [protonmail.com](https://protonmail.com/fr/)

* * *

-  Depuis 2013
-  Serveurs en Suisse
-  En Français
-  Option gratuite mais limitée
-  4€/mois pour 5GB
-  Bitcoin, CB, Paypal
-  5 [alias](alias.html)
-  Utiliser son nom de domaine, sinon @protonmail.com ou @pm.me
-  Chiffrement de bout-en-bout OpenPGP
-  Envoyer des messages chiffrés à des destinataires extérieurs (via page Web)
-  <span class="orange">[ProtonMail Bridge](https://protonmail.com/bridge/) pour utiliser un client email (ThunderBird par ex.) avec IMAP/POP3</span>
-  <span class="orange">Le sujet du mail n'est pas chiffré</span>
-  Labels, filtres personnalisés et dossiers illimités
-  Importation des contacts et calendriers et de son ancienne boîte mail.
-  Application mobile iOS, Android.

{{</block>}}

{{<block info>}}

### Mailbox | [mailbox.org](https://mailbox.org/en/)

* * *

-  Depuis 2014
-  Serveurs en Allemagne
-  <span class="orange">En Anglais ou Allemand</span>
-  1€/mois pour 2GB (extensible)
-  Bitcoin, CB, Paypal</span>
-  <span class="orange">3 [alias](alias.html)</span>
-  Utiliser son nom de domaine sinon @mailbox.org
-  Chiffrement de bout-en-bout OpenPGP
-  Envoyer des messages chiffrés à des destinataires extérieurs (via Mailvelope)
-  Supporte IMAP/POP/SMTP/ActiveSync pour utiliser un client email (ThunderBird par ex.)
-  Synchronisation mobile (calendrier, contacts)
-  <span class="orange">Pas d'application mobile, possibilité d'utilisé un client email mobile.</span>
-  Inclus un bureau virtuel sécurisé: email, calendrier, contact, stockage cloud 100Mo (extensible contre paiement supp.), tâches, fichiers de textes et de présentations, et Webchat.
-  <span class="orange">Connexion au Bureau virtuel et Cloud par navigateur uniquement</span>
-  Fonctionne en 100% Energie verte

{{</block>}}

{{<block info>}}

### Tutanota | [tutanota.com](https://tutanota.com/fr/)

* * *

-  Depuis 2011
-  Serveurs en Allemagne
-  En Français
-  Option gratuite mais limitée
-  1€/mois pour 1GB ou 4€/mois pour 10GB
-  Bitcoin, CB, Paypal
-  5 [alias](alias.html)
-  Utiliser son nom de domaine, sinon @tutanota ou @tuta.io
-  <span class="orange">N'utilise pas OpenPGP mais un protocole propriétaire</span>
-  Envoyez des messages chiffrés à des destinataires extérieurs (via page Web)
-  <span class="orange">Pas d'IMAP/POP3/SMTP pour utiliser un client email (ThunderBird par ex.) sur l'ordinateur</span> mais application pour Windows, macOS et Linux.
-  Chiffrement total des messages, même le sujet
-  <span class="rouge">Importation de son ancienne boîte mail <u>prévue</u></span>
-  <span class="orange">Délai de 48h pour l'approbation de sa boîte mail</span>
-  Application mobile iOS, Android
-  Fonctionne en 100% énergie verte

{{</block>}}


{{< prev relref="/docs/outils/emails" >}} < Emails {{< /prev >}}
{{< next relref="/docs/outils/emails/alias" >}} Alias > {{< /next >}}
