---
title: "Supprimer son compte"
bookToc: false
weight: 4
---
# Supprimer son compte email
***

Il y a deux procédures recommandées pour changer de fournisseur.

## 1. Le transfer de messagerie
***

Utiliser le transfer de son ancienne boîte mail vers celle du nouveau fournisseur.

Mailbox et ProtonMail le propose, Tutanota va le proposer dans le futur.

Le nouveau fournisseur récupère tous les emails et pièces jointes de l'ancienne boîte mail.

1.  Tous les anciens emails seront transférer sur un nouveau serveur.
2.  Le nouveau compte aura besoin d'une quantité de stockage suffisante pour accueillir la totalité des emails et pièces jointes de l'ancienne boîte mail.

**Avantages:**
-  Permet de changer rapidement de fournisseur.
-  Garder ses vieux emails dans sa nouvelle boîte et accessibles en ligne.

**Inconvénients:**
-  Ne permet pas de faire le tri de tous les déchets de l'ancien compte.
-  Peut demander l'achat d'un compte avec un gros volume de stockage.

Il est recommandé de faire le tri de ses déchets d'emails avant de les transférer pour éviter d'avoir à payer plus cher si ce n'est pas nécessaire.

## 2. Partir sur une base saine
***

On supprime du serveur les anciens emails et on les sauvegarde sur son ordinateur.

On pourra les trier localement, sur son disque dur, ensuite; supprimer les déchets, archiver les emails importants.

**Avantages:**
-  Démarrer sur une base propre, sans déchets.
-  Utiliser un compte gratuit ou un compte moins cher.

**Inconvénient:**
-  Les anciens emails ne seront pas sur la nouvelle boîte et inaccessibles en ligne.

{{<block info>}}

**Marche à suivre:**

1.  S'assurer que le disque dur de son ordinateur a la place pour récupérer tous les emails. En se connectant au Webmail, on voit en général la taille de stockage utilisée.
1.  Télécharger et installer [Thunderbird](/docs/outils/emails/clients.html) sur l'ordinateur.
1.  Ouvrir Thunderbird, et connecter son compte Webmail avec **POP3**. Thunderbird va télécharger sur l'ordinateur et supprimer tous les emails et pièces jointes du serveur.
1.  Une fois tous les emails récupérés, les emails seront enregistrés sur l'ordinateur, le serveur sera normalement vide.
1.  Vérifier que tous les messages ont bien étés sauvegardés.
1.  En se connecter au Webmail, vérifier que les emails ont étés supprimés, sinon le faire manuellement.
1.  Faire le tri sur son ordinateur dès que possible, pour libérer de l'espace de stockage.

**Attention:**

Si jamais IMAP est utilisé au lieu de POP3, les emails seront synchronisés avec le serveur, rappel > [Protocoles](/docs/outils/emails/clients.html). Pour les sauvegarder il faudra sélectionner un à un les emails que l'on souhaite enregistrer sur son disque dur puis faire Fichier > Enregistrer comme > Fichier. Il sera donc beaucoup plus long de vider le serveur.

{{</block>}}

## Fermer son compte
***

Encore une étape primordiale, la fermeture de l'ancien compte.

Comme avec un déménagement, il faut avertir ses contacts. Il y a plusieurs possibilités.

{{<block info>}}

### Remettre à plus tard la suppression définitive du compte.
***
1.  Configurer un message d'absence dans son Webmail en indiquant que l'adresse à changé. Il sera automatiquement envoyé à chaque fois qu'une personne contactera l'ancienne adresse.
1.  Configurer la copie des emails qui continuent d'arriver sur l'ancienne boîte dans la nouvelle. Cela s'appelle "Transfert d'email", ou _Email forwarding_.
1.  Si le compte est utilisé pour des sites importants (impôts, assurances, banques, etc.), penser à changer l'adresse email sur les comptes des services. L'utilisation d'un [alias](alias.html) peut être bienvenue.
1.  Supprimer le compte quand on estime que la période de transition a suffisamment durée.

**Conseils:**

-  Éviter de mettre la nouvelle adresse dans le message d'absence si l'on tient à la garder très confidentielle.
-  En configurant l'_email forwarding_ avec l'ancien fournisseur, il connaîtra donc la nouvelle adresse.
-  Sans ces deux options, aller voir de temps en temps sur l'ancienne adresse qui a cherché à nous écrire et envoyer manuellement la nouvelle de manière sécurisée (voir ci-dessous).

{{</block>}}

{{<block info>}}

### Supprimer instantanément l'ancien compte.
***
L'adresse n'existera plus, les personnes envoyant un email auront un retour automatique que l'adresse n'existe plus.

Si le compte est utilisé pour des sites importants (impôts, assurances, banques, etc.), penser à changer l'adresse email sur les comptes des services avant une suppression complète. L'utilisation d'un [alias](alias.html) peut être bienvenue.

{{</block>}}

## Communiquer sa nouvelle adresse
***

Envoyer manuellement la nouvelle adresse à tous ses contacts par des moyens sécurisés:
-  [Messagerie Instantanée sécurisée](/docs/outils/im.html)
-  [Partage de fichiers textes](/docs/outils/partage-de-fichiers/textes.html)
-  Avec le nouveau fournisseur, en écrivant un email <u>chiffré</u>.

{{< prev relref="/docs/outils/emails/clients" >}} < Clients email {{< /prev >}}
