---
title: "Messageries instantanées"
bookToc: false
bookCollapseSection: true
weight: 70
description: De nos jours, communiquer avec ses contacts de manière sécurisée et confidentielle ne peut seulement se faire en passant par des messageries instantanées sécurisées.
---

# Messageries instantanées (IM)
***

La communication courante et instantanée (par sms, mms, ou via certaines messageries) communiquent les messages en clair et utilisent également tout un tas de métadonnées d'utilisateurs. L'opérateur téléphonique ou l'entreprise peut avoir accès et utiliser tout ce qui est échangé, ainsi que les liens unissant les personnes entre elles.

Aujourd'hui en France, les plus courantes et au coeur de la communication quotidienne, sont principalement détenues par Facebook (WhatsApp, Facebook Messenger et Instagram), Google (Hangout) et Microsoft (Skype).

Les sms restent une forme de communication instantanées très utilisée mais absolument pas confidentielle.

Pour être sécurisées et résistantes aux pressions et censures des états ou des entreprises, la messagerie instantanée doit à présent passer par Internet, utiliser le chiffrement de bout en bout et, mieux encore, la décentralisation, au travers de réseaux fédérés ou en pair à pair (P2P) par exemple.

Ces applications sont de plus en plus nombreuses et fonctionnent de mieux en mieux. Cependant, elles ne sont pas encore utilisées massivement car moins promues que leurs homologues issues des GAFAM utilisant leur capitaux imbattables. Également, certaines sont moins facile d'utilisation pour le grand public que les messageries populaires à éviter.

{{<section>}}
