---
title: "Centralisées"
bookToc: false
weight: 1
summary: Services contrôlés par une seule entité, qui ne fonctionnent qu'avec une seule application et autour d'un serveur central.
---

# Messageries instantanées centralisées
***

Services contrôlés par une seule entité, qui ne fonctionnent qu'avec une seule application et autour d'un serveur central.

**Avantages:**

-  De nouvelles fonctionnalités et des modifications mises en œuvre plus rapidement.
-  Plus facile au démarrage et pour trouver des contacts.

**Inconvénients:**

-  Nécessite l'obligation pour les utilisateurs d'installer le même client (application).
-  Le service n'est pas à l'abris d'être racheté ou de devenir une cible pour les gouvernements, les pirates ou la censure.
-  La propriété, la politique de confidentialité et les opérations du service peuvent changer facilement lorsqu'une seule entité le contrôle, ce qui peut compromettre le service par la suite.

{{<block sain>}}

### Gestes sains
***
-  Être au courant du fonctionnement et des pratiques du service.
-  Rester informé des changements.
-  Quitter et changer de service si nécessaire.

{{</block>}}

{{<hint danger>}}

**Poubelle:** WhatsApp, Facebook Messenger, Telegram, Skype, Wire, Threema, Google Hangouts, Lime, WeChat, Instagram, Viber, Snapchat  etc.

{{</hint>}}

{{<block info>}}

### Signal | [signal.org](https://www.signal.org/fr/)
***

-  Fonctionne sur Android, iOS, Windows, macOS, Linux.
-  Visio-conférence pour les ordinateurs en cours de développement. Source : [blog Signal](https://signal.org/blog/desktop-calling-beta/)

Créé en 2013 par [Moxie Marlinspike](https://moxie.org/), le lancement en 2015 et le développement sont financés par les dons. Signal est une organisation _non-profit_ qui a regroupé TextSecure et RedPhone, créés également par Marlinspike en 2010, en une seule application.

Signal est la seule messagerie instantanée centralisée, simple, sécurisée et en qui on peut avoir confiance pour l'instant.

L'application a été fondamentalement pensée pour ne jamais pouvoir lire d'informations personnelles. Elle souhaite palier aux problèmes de confidentialité et de fonctionnement des messageries [P2P](/docs/outils/im/p2p.html) avec un serveur entre les utilisateurs ne pouvant pas être utilisé par un tiers et révéler la vie des utilisateurs. Pour ça, elle a développé un système d'échange chiffré de bout en bout très sophistiqué pour les messages, les visio-conférences et les appels, qui rend le serveur et le service complètement _zero-knowledge_, fait pour ne jamais connaître quoi que ce soit.

Signal est _open source_, le code de l'application est donc transparent et régulièrement vérifié par des pairs et des experts indépendants pour repérer la moindre faille et la faire corriger. Rapport: [A Formal Security Analysis of the Signal Messaging Protocol (2019)](https://eprint.iacr.org/2016/1013.pdf).

L'organisation vit et développe Signal grâce aux dons, elle n'a donc aucun intérêt à cibler les utilisateurs ou leurs données. De plus elle est sous un régime fiscal _non-profit_ qui l'empêche d'être vendue.

Contrairement aux autres messageries populaires, Signal ne connaît pas nos contacts, notre graphe social, nos groupes de discussions, notre appartenance à un groupe et qui en fait parti, notre nom et avatar de profil, nos données de localisation, nos recherches de gif, etc. Elle n'inclue aucun traqueur ni outils d'analyses et aucune publicité.

Pour les contacts, une fonction de hachage cryptographique brouille les numéros de téléphone avant de les envoyer au serveur, ceci afin de trouver quels sont les utilisateurs communs en utilisant un service conçu pour protéger la confidentialité des contacts.

[Démontré en 2016](https://www.aclu.org/blog/national-security/secrecy/new-documents-reveal-government-effort-impose-secrecy-encryption?redirect=blog/free-future/new-documents-reveal-government-effort-impose-secrecy-encryption-company), si un gouvernement exige que Signal remette le contenu ou les métadonnées d'un message ou la liste des contacts d'un utilisateur, il n'y a rien à remettre, hormis la date d'installation et la dernière date de connexion.

En 2015, Edward Snowden le recommande [sur twitter](https://twitter.com/Snowden/status/661313394906161152).

En 2018, Brian Acton, l'un des fondateurs de WhatsApp, qui l'a revendu à Facebook et a quitté le groupe, a fait une dotation de 50 millions de dollars pour financer le développement et est désormais le président exécutif de la [Fondation Signal](https://signalfoundation.org/).

En février 2020, la [Commission européenne](https://www.ouest-france.fr/high-tech/la-messagerie-signal-est-elle-plus-securisee-que-whatsapp-ou-telegram-pour-preserver-sa-vie-privee-6753799) l’a recommandée à ses équipes, en particulier pour sécuriser les échanges avec des personnes extérieures à l’organisation.  
Les journalistes de l’AFP témoignent également d’installations massives chez les politiques, et – dans une moindre mesure – parmi leurs sources judiciaires ou policières.

En juin 2020, [Signal réagit rapidement](https://signal.org/blog/blur-tools/) aux manifestations américaines pour protéger les citoyens participants au changement. Supprimant déjà automatiquement toutes les métadonnées des images, elle ajoute un système pour flouer les visages, les tatouages, les plaques d'immatriculation, présents sur les photos <u>directement sur le téléphone</u> et donc avant même qu'elles n'atteignent le serveur contrairement à Instagram par exemple.

Pour simplifier la recherche de ses contacts et l'utilisation de Signal, l'utilisateur doit avoir un numéro de téléphone pour s'inscrire. Le numéro n'est pas utilisé autrement que pour communiquer entre utilisateur et n'est pas revendu à des tiers pour qu'ils se financent (ce qui est moins probable avec WhatsApp). Il est toutefois possible d'utiliser un [numéro de téléphone](/docs/outils/inscriptions/numeros-tel.html) secondaire qu'il faudra communiquer à ses contacts si l'on veut.. De toute façon, Signal n'est pas fait pour l'anonymat mais pour protéger la confidentialité de ses discussions et sa votre vie privée. Sur ce point, aucune n'atteint ce niveau de protection dans la catégorie des messageries centralisées.

-  Il est recommandé d'utiliser Signal plus que toute autre messagerie centralisée.
-  [Faire un don](https://signal.org/donate/) pour les aider à continuer de protéger les données personnelles.
-  Ne plus utiliser les messageries populaires qui utilisent et vendent vos informations personnelles et privées.

{{</block>}}

{{<hint warning>}}

### Wire

* * *

Au niveau du design, quand on voit comment les messageries instantanées sont dessinées aujourd'hui, Wire est dans les choux.

Bien plus préoccupant, Wire a fait l'autruche durant _"une semaine d'interrogatoire avant de confirmer finalement qu'elle avait changé de holding et qu'elle serait désormais une société basée aux États-Unis, alors qu'elle s'efforçait de prendre pied sur le marché des entreprises. Cette nouvelle est venue s'ajouter à celle selon laquelle Wire avait accepté plus de 8 millions de dollars de capital-risque de la part de Morpheus Ventures, ainsi que d'autres investisseurs."_ Source: [blog.privacytools.io 19/11/19](https://blog.privacytools.io/delisting-wire/)

Cette volonté de s'implanter aux États-Unis alors que tout était encore en Suisse il y a peu de temps, laisse planer de grands doutes sur la sécurité des utilisateurs et le futur de la messagerie.

{{</hint>}}

{{<hint danger>}}

### Telegram
***

-  L'application et le serveur ne sont pas _open source_, donc le système et sa sécurité sont invérifiables.
-  La compagnie ne fournit pas de rapport de transparence.
-  Le chiffrement de bout en bout n'est pas activé par défaut, ce que les utilisateurs ne savent pas.
-  L'entreprise est très opaque, le site internet ne donne aucune information sur ses dirigeants ou ses équipes.
-  On ne sait pas comment Telegram est réellement financé.
-  L'inscription demande un numéro de téléphone pour ensuite utiliser un pseudo.
-  La compagnie et l'application récoltent les données des utilisateurs.

Quand il s'agit de sécurité et de vie privée, il faut que des audits indépendants puissent vérifier qu'aucune _backdoor_ ou code malicieux ne soit présents pour certifier la sécurité des utilisateurs, que la source des revenus de l'entreprise soit clairement énoncé, que l'on sache qui gère réellement l'entreprise pour sentir leurs motivations, que le chiffrement de bout en bout ne soit pas une option.

Encore une fois avec Internet, on retrouve des sites qui ne vérifient pas ce qu'ils écrivent ou n'hésitent pas à se faire payer pour écrire des articles élogieux sur des applications malsaines et des internautes qui prônent Telegram sur Twitter, Facebook, Blogs et forums sans aucun arguments appuyés et valables. **Cette application ne coche aucune case affirmant qu'elle est une messagerie sécurisée.**

{{</hint>}}

## Comparatifs
***

-  [securemessagingapps.com](https://www.securemessagingapps.com/)
-  [securechatguide.org/effguide](https://securechatguide.org/effguide.html)

## WhatsApp, Facebook Messenger, Instagram
***

Pourquoi ne plus utiliser ces services ?

1.  Ils sont détenus par Facebook, qui se trouve au coeur d'un scandale sur la gestion des données personnelles au moins tous les ans.
2.  En donnant l'accès au carnet d'adresse, ils peuvent utiliser et revendre toutes les informations de nos contacts.
3.  Les métadonnées sont extraites, stockées et revendues à des tiers et le chiffrement de bout en bout de Whatsapp n'empêche pas cela.
4.  Leurs services et applications sont remplis de traqueurs qui suivent les gestes et les échanges des utilisateurs, mais aussi, afin suivre les tendances et réagir en fonction des changements afin de garder le contrôle et son emprise sur les usagers.
5.  Facebook se sert aussi des traqueurs pour vendre aux annonceurs de la publicité ciblée.
6.  Le risque d'être manipulé dans ces services est extrêmement fort ([cas Cambridge Analytica](https://invidious.snopyta.org/watch?v=ODfD3VYCoGU) par ex.).
7.  L'architecture est totalement fermée donc non _open source_. Le système et la sécurité des utilisateurs est donc volontairement invérifiable par des personnes ou des entreprises indépendantes.
8.  Des systèmes de publicités et de publicités ciblées sont au coeur des applications et en recherche d'implémentation sur WhatsApp ([Source](https://twitter.com/Olivier_Ptv/status/1130800085426683904)).
