---
title: "Fédérées"
bookToc: false
weight: 3
summary: Le service est déployé sur plusieurs serveurs indépendants capables d'échanger entre eux.
---

# Messageries instantanées fédérées
***

La fédération déploie le service sur plusieurs serveurs indépendants capables d'échanger entre eux, le principe est le même que pour les emails.

La fédération permet aux administrateurs système de contrôler leur propre serveur tout en faisant partie d'un réseau de communication plus large.

**Avantages:**


-  Permet un meilleur contrôle de ses données lorsqu'on utilise son propre serveur.
-  Permet de choisir à qui confier ses données en choisissant entre divers serveurs "publics".
-  Permet souvent d'utiliser des applications tierces qui peuvent offrir une expérience plus personnalisée ou plus accessible.
-  Généralement une cible moins profitable pour les gouvernements qui veulent installer des accès dérobés (_backdoors_) sur les serveur, car la chaîne est décentralisée. Le serveur peut être hébergé indépendamment de l'organisation qui développe le logiciel.
-  Les développeurs tiers peuvent contribuer au code et ajouter de nouvelles fonctionnalités, au lieu d'attendre qu'une équipe de développement privée le fasse.

**Inconvénients:**

-  L'ajout de nouvelles fonctionnalités est plus complexe, car elles doivent être normalisées et testées pour s'assurer qu'elles fonctionnent avec tous les serveurs du réseau.
-  Certaines métadonnées peuvent être disponibles (par exemple, des informations telles que "qui parle à qui", mais pas le contenu réel du message si le chiffrement de bout en bout est utilisé).
-  Les serveurs fédérés nécessitent de faire confiance à l'administrateur de son serveur. Il peut s'agir d'un amateur et non d'un "professionnel de la sécurité", et il se peut qu'il n'indique pas une politique de confidentialité ou des conditions de service détaillant la manière dont nos données sont utilisées.
-  Les administrateurs de serveurs peuvent choisir de bloquer d'autres serveurs, ce qui est une source d'abus non modérés ou enfreint les règles générales de comportement accepté. Cela entravera la capacité à communiquer avec les utilisateurs sur ces serveurs.

{{<block info>}}

### Element | [element.io](https://element.io)
***

[[matrix]](https://matrix.org) est un réseau qui permet à chacun d'utiliser l'application _open source_ qu'il préfère pour communiquer avec les utilisateurs ([> liste complète](https://matrix.org/clients-matrix/)).

Element (anciennement Riot) est la plus connue et celle qui a le plus de fonctionnalités, notamment le chiffrement de bout en bout. Elle supporte Android, iOS, Windows, macOS, Linux ainsi qu'une utilisation sans application via Firefox ou Chrome.

Element peut être un peu déroutant au début car elle n'est pas qu'une application de messagerie classique. Le réseau [matrix] permet les discussions habituelles avec ses amis, mais offre en plus l'accès à des réseaux plus étendus, en accédant à des groupes de discussions ou de travail. Cette seconde approche est assez similaire à l'utilisation de Reddit, Facebook, Discord, les forums plus classiques. Il n'y a pas uniquement besoin de se connaître pour pouvoir échanger.

Il est facile de créer des groupes privés ou publics, de discussions, de travail, pour s'informer, apprendre ou discuter sur des sujets, des actualités.

L'inscription se fait par le choix d'une instance (serveur) et d'un pseudonyme.

{{</block>}}

{{<block info>}}

### Status | [status.im](https://status.im/)
***

Status est une application de messagerie sécurisée, un porte-monnaie Ethereum (crypto-monnaie), un support d'applications décentralisées et un navigateur Web.

{{</block>}}
