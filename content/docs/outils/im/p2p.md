---
title: "P2P"
bookToc: false
weight: 1
summary: Services qui fonctionnent en pair à pair, sans serveur entre les utilisateurs, les données sont stockées sur les appareils.
---

# Messageries instantanées P2P
***

Ce type de service fonctionne en pair à pair (P2P), c'est à dire d'un utilisateur à un autre sans passer par un serveur. Les données sont stockées uniquement sur l'appareil des pairs.

**Avantages:**

-  Informations minimes exposées à des tiers.
-  Les plates-formes P2P modernes mettent en œuvre un chiffrement de bout en bout par défaut.
-  Contrairement aux modèles centralisés et fédérés, il n'y a pas de serveurs qui pourraient potentiellement intercepter et déchiffrer les transmissions.
-  Il n'y a pas besoin de numéro de téléphone pour l'utiliser, une adresse email ou un pseudo suffit.

**Inconvénients:**

-  Ensemble de possibilités réduites :
    -  Les messages ne peuvent être envoyés que lorsque les deux pairs sont en ligne.
    -  Augmente généralement l'utilisation de la batterie des appareils mobiles, car l'application doit rester connectée au réseau pour savoir qui est en ligne.
-  L'adresse IP et celle des contacts avec lesquels on communique peuvent être visibles si le logiciel n'est pas utilisé en conjonction avec un réseau autonome, tel que Tor ou I2P. (De nombreux pays disposent d'une forme de surveillance de masse et/ou de la conservation des métadonnées qui transitent).

{{<block info>}}

### Jami | [jami.net](https://jami.net/)
***

-   Messagerie instantanée et appel vidéo.
-   Toutes les communications sont chiffrées de bout en bout en utilisant TLS.
-   Rien n'est stocké ailleurs que sur l'appareil de l'utilisateur.
-   Windows, macOS, Linux, iOS, Android

{{</block>}}

{{<block info>}}

### Briar | [briarproject.org](https://briarproject.org/)
***

-   Échange en P2P (pair à pair).
-   Chiffrement de bout en bout.
-   Messages synchronisés via Tor avec Internet ou via Wi-Fi ou Bluetooth s'il n'y a pas d'Internet. Cette technologie est utile lorsque la disponibilité de l'Internet est en crise. Si un gouvernement ferme l'accès à Internet à sa population ou lors d'une catastrophe naturelle par exemple, les messages iront jusqu'au destinataire sans Internet, en utilisant le protocole Bluetooth ou Wi-Fi.
-   <span class="rouge">Uniquement sur Android pour l'instant.</span>

{{</block>}}

{{<block info>}}

### Tox | [tox.chat](https://tox.chat/)
***

-   IM, voix, vidéo, partage d'écran, partage de fichiers et groupes
-   Chiffrement de bout en bout
-   Différents clients: qTox, uTox, Antox, Toxic
-   <span class="rouge">Aucun client pour iOS</span>

{{</block>}}
