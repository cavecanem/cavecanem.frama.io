---
title: "Inscriptions en ligne"
bookToc: false
bookCollapseSection: true
weight: 1
description: Quand on s'inscrit sur Internet, quels renseignements donne-t-on et lesquels ne faut-il pas donner ?
---

# Inscriptions en ligne
***

Créer un compte dans l'univers numérique ne doit pas être considéré comme quelque chose d'anodin. Toutes les informations transmises au service peuvent nuire gravement à sa confidentialité et au respect de sa vie privée.

La création de compte est pratique dans certains cas, par exemple, quand on est très actif sur un service, il permet de l'utiliser plus efficacement.

Mais aujourd'hui, c'est aussi un moyen privilégié par les entreprises pour récupérer le plus d'informations précises sur leurs utilisateurs. Le but étant de revendre les données personnelles recueillies à des sociétés tierces plus chères que si elles étaient anonymes ou moins nombreuses.

Pour éviter de devenir une cible précise pour les annonceurs, les gouvernements, les entreprises ou les pirates, il est recommandé de ne jamais donner sa véritable identité. Que ce soit sur Facebook, Instagram et consort, Gmail, Outlook, la vente en ligne ou encore lors de l'installation et l'activation des systèmes d'exploitation Windows, macOS, iOS et Android.

{{<block sain>}}

### Pratique Saine
***

-  Ne pas créer de compte quand ce n'est pas obligatoire, ou strictement nécessaire.
-  Lire les CGU des services pour connaître à quelles fins vont être utilisées les données personnelles.
-  Toujours renseigner le strict minimum nécessaire au fonctionnement du service.
-  Tromper le service avec un pseudonyme, un alias, voire une fausse identité.
-  Utiliser une adresse email temporaire et un numéro de téléphone secondaire si besoin.

Ne jamais fournir l'un des renseignements réels suivants s'il n'est pas <u>strictement nécéssaire</u> (livraisons par exemple) :

-  Son nom et prénom
-  Son adresse postale
-  Son numéro de téléphone
-  Sa date de naissance
-  Son adresse mail personnelle
-  Son sexe
-  Son orientation sexuelle
-  Son visage (photo et reconnaissance faciale)
-  Ses empreintes digitales
-  Sa carte d'identité
-  Son numéro de carte bancaire

{{</block>}}

{{< next relref="/docs/outils/inscriptions/lire-les-cgu" >}} Lire les CGU > {{< /next >}}
