---
title: "Cartes bancaires"
bookToc: false
weight: 5
description: Protéger ses cartes bancaires en ligne lors d'une inscription.
---

# Cartes bancaires
***

Les cartes bancaires lient directement un service avec sa véritable identité.

Il n'est pas nécessaire de renseigner sa carte bancaire sur les services car elle figure rarement dans les champs requis lors d'une inscription.

Pour le paiement en ligne voir le chapitre dédié > [Paiements en ligne](/docs/outils/paiements.html).

## Fausse carte bancaire
***

Il est bon de savoir que l'on peut se servir de fausses cartes bancaires dans les cas, très rares, où un service obligerait ce renseignement à l'inscription alors qu'on ne souhaite rien acheter.

{{<block info>}}

### Fake Card Generator | [fake-card-generator.com](https://fake-card-generator.com/fr/)
***

Ce site permet de générer de fausses cartes bancaires.

-  Faux nom et prénom, adresse, numéro, pays, CVV, date d'expiration.
-  Visa, Mastercard, Discover, JCB, American Express.

{{</block>}}
