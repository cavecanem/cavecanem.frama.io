---
title: "Emails temporaires"
bookToc: false
weight: 3
description: Utiliser des emails temporaires pour réduire le risque de suivi en ligne, de spam, d'espionnage et de piratage.
---

# Emails temporaires
***

Les services acceptant de moins en moins les pseudonymes, l'adresse email est devenue l'outil d'identification principal pour créer un compte en ligne.

Bien qu'elle ne soit pas forcément liée à votre véritable identité, elle est en revanche quasiment toujours revendue par les services en ligne à des entreprises qui rachètent et regroupent les informations disponibles un peu partout.

L'effet direct est la réception régulière de _spams_ et de publicités sur sa boîte email principale.  
L'effet indirect est un risque accru d'identification et de suivi.

Pour éviter cela, utiliser un [alias](/docs/outils/emails/alias.html) ou un email temporaire lors d'une inscription.

Contrairement à l'alias, une adresse email temporaire finira par se détruire d'elle-même. Cela permet de l'utiliser sur des sites douteux ou pour lesquels un compte, qui nous est d'aucune utilité, est obligatoire pour accéder au contenu. Au bout de quelques heures, l'adresse sera détruite sans effort.

Parmi les applications possibles : Newsletter pour code promotionnels, inscriptions aux WiFi publics, forums, sites web et blogs qui bloquent le contenu aux non-inscrits.

L'email temporaire permet donc de s'inscrire en sécurité, sans que son adresse réelle, ou un alias, soit rempli de _spams_ ou puisse compromettre son identité.

## Exemple
***
Je veux lire ou télécharger un contenu sur un forum qui est bloqué car je n'ai pas de compte. Une adresse email est requise pour l'inscription et je n'ai aucune utilité d'un tel compte. J'utilise une adresse temporaire me permettant de m'inscrire sans risques.  
Si besoin, je peux récupérer le code de vérification d'email rapidement et ainsi valider la création du compte.

Le site n'utilisera jamais mon email principal ou un de mes alias pour revendre mes informations personnelles ou m'envoyer de la publicité.

## Services
***

Il existe beaucoup de service du genre qui n'ont pas forcément de bonnes pratiques. Ceux-ci sont les plus recommandés.

Il se peut que les noms de domaines de ces adresses soient bloqués par certains sites, il faut jongler entre eux si c'est le cas.

{{<hint danger>}}**Attention :** Ne jamais utiliser pour l'authentification à deux facteurs (2FA) d'un service ! Pour le 2FA, utiliser un [alias](/docs/outils/emails/alias.html) ou un [numéro de téléphone secondaire](/docs/outils/emails/alias.html).{{</hint>}}

{{<block info>}}

### TempMail | [temp-mail.org](https://temp-mail.org/fr/)
***
-  Génération aléatoire d'email temporaire en ligne.
-  Boîte email supprimée à la fermeture de la page.
-  Page fonctionnant sur tous les navigateurs.
-  Application iOS et Android également disponibles.
-  Service gratuit.

{{</block>}}

{{<block info>}}

### Guerilla Mail | [guerrillamail.com](https://www.guerrillamail.com/fr/)
***
-  Génération aléatoire ou personnalisée d'adresses email temporaires.
-  Permet de conserver ses adresses email personnalisées et de supprimer chacune en un clic, à n'importe quel moment.
-  Les emails reçus sont supprimés de la boîte automatiquement après 1h, qu'ils soient lus ou pas.
-  Aucune création de compte n'est requise.
-  Service _open source_ gratuit.

Tutoriel :

1.  Utiliser l'adresse aléatoire ou la modifier.
1.  Sauvegarder son adresse dans un carnet ou dans sa tête.
1.  Copier la "Scrambled Adress" pour brouiller l'adresse précédemment créée et éviter que quelqu'un puisse s'y connecter ultérieurement.
1.  Coller la "Scrambled Adress" lors d'une inscription en ligne.
1.  Les emails reçus apparaîtrons sur la page principale.
1.  Si la page a été fermée, entrer à nouveau l'adresse pour accéder au contenu de la boîte.
1.  Lorsque l'adresse n'est plus utile, cliquer sur le bouton "Oubliez-moi".

{{<hint danger>}}**Attention :** Car aucun mot de passe n'est requis, Guerilla Mail ne doit pas servir à recevoir du contenu personnel sensible. Une personne ayant connaissance de l'adresse peut également accéder à la boîte de réception.{{</hint>}}

{{</block>}}

{{<block info>}}

### Yadim | [yadim.dismail.de](https://yadim.dismail.de/)
***
Yadim est un site d'emails jetables propulsé par dismail.de.
-  Les emails sont supprimés après 3 jours passé dans la boîte de réception
-  Adresse modifiable avant l'arobase.
-  Possibilité de revenir sur l'adresse créée en sauvegardant le lien de la page.
-  Service _open source_ et gratuit.

{{<hint danger>}}**Attention :** Car aucun mot de passe n'est requis, Yadim ne doit pas servir à recevoir du contenu personnel sensible. Une personne ayant connaissance de l'adresse peut également accéder à la boîte de réception.{{</hint>}}

{{</block>}}

{{< prev relref="/docs/outils/inscriptions/tromper" >}} < Tromper le service {{< /prev >}}
{{< next relref="/docs/outils/inscriptions/numeros-tel" >}} Numéros de téléphone > {{< /next >}}
