---
title: "Lire les CGU"
bookToc: false
weight: 1
description: Lire les conditions générales d'utilisations (CGU) pour être informé sur l'utilisation de ses données personnelles par le service.
---

# Lire les CGU
***

Tous les services ont des Conditions Générales d'Utilisation (CGU), en anglais : _Terms of service_. Également très importante, et parfois à part, la politique de confidentialité, en anglais : _Privacy Policy_.

Ces informations doivent obligatoirement être présentes et sont généralement affichées en bas de page sur les sites Internet ou consultables au moment de l'inscription.

Bien qu'elles soient souvent bien trop fastidieuses à lire (d'ailleurs, plus c'est long, plus c'est mauvais signe), dans le doute, il faut quand même faire l'effort d'aller voir le chapitre sur la politique de collecte et d'utilisation des données fournies par l'utilisateur disponible soit dans les CGU soit dans la politique de confidentialité.

Celles-ci éclairent grandement sur ce que le service est disposé à faire de nos données et métadonnées, donc, du respect de sa vie privée.

Cave Canem vérifie chaque CGU des services proposés sur le site afin d'être sûr qu'elles répondent au mieux à un respect des données personnelles des utilisateurs.

Deux exemples aux antipodes :  
Chez Framasoft : [https://framasoft.org/fr/legals/](https://framasoft.org/fr/legals/).  
Chez Facebook : [https://fr-fr.facebook.com/about/privacy](https://fr-fr.facebook.com/about/privacy).


{{< prev relref="/docs/outils/inscriptions" >}} < Inscriptions {{< /prev >}}
{{< next relref="/docs/outils/inscriptions/tromper" >}} Tromper le service > {{< /next >}}
