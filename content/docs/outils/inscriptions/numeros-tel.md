---
title: "Numéros de téléphone"
bookToc: false
weight: 4
description: Quels sont les risques lorsque l'on renseigne son numéro de téléphone en ligne et comment le protéger ?
---

# Numéros de téléphone
***

Le numéro de téléphone est un des renseignements très demandé par les entreprises aujourd'hui car il est très précieux.

Un numéro de téléphone est quasiment toujours lié à la véritable identité du possesseur et change beaucoup plus rarement qu'une adresse email. La plupart des pays, sous couvert de protection contre le terrorisme, ont rendu obligatoire l'apport d'une carte d'identité lors de la contraction d'un forfait téléphonique, empêchant ainsi l'achat anonyme de carte SIM. De fait, notre numéro de téléphone permet de nous identifier presque aussi bien qu'une carte d'identité ou une carte bancaire.

Notre numéro de téléphone est régulièrement requis pour recevoir un code de vérification. Par exemple, sur Facebook, Google, Outlook, Twitter, etc. les utilisateurs sont souvent sollicités et presque obligés de donner un numéro lors de l'inscription, sous couvert de sécurité. Ces services n'ont pas à récupérer nos numéros sachant qu'ils n'en ont aucunement besoin pour fonctionner. Il s'agit ni plus ni moins d'une tentative de fichage et de vente de nos numéros à des sociétés publicitaires.

De plus, les numéros de téléphone n'ayant jamais été prévus pour s'inscrire en ligne, ils sont devenus une cible pour les pirates et le _SIM jacking_ ou _SIM swap_. Facilement, une personne peut récupérer le numéro de quelqu'un d'autre, le transférer sur une nouvelle carte SIM afin de recevoir les sms, et ainsi, ré-initialiser les mots de passe de nos comptes en ligne. Source : [Twitter suspend les tweets par SMS suite au piratage du compte de son patron](https://www.generation-nt.com/twitter-tweet-sms-piratage-jack-dorsey-actualite-1968145.html), 05/09/19.

Autres sources :
-  _You Gave Facebook Your Number For Security. They Used It For Ads._, [eff.org](https://www.eff.org/deeplinks/2018/09/you-gave-facebook-your-number-security-they-used-it-ads), 27/09/18.
-  _Facebook Doubles Down On Misusing Your Phone Number_, [eff.org](https://www.eff.org/deeplinks/2019/03/facebook-doubles-down-misusing-your-phone-number), 04/03/19.
-  _Twitter "Unintentionally" Used Your Phone Number for Targeted Advertising_, [eff.org](https://www.eff.org/deeplinks/2019/10/twitter-unintentionally-used-your-phone-number-targeted-advertising), 09/11/19.
-  _How Twitter's Default Settings Can Leak Your Phone Number_, [eff.org](https://www.eff.org/deeplinks/2020/02/how-twitters-default-settings-can-leak-your-phone-number), 05/02/20.


## Numéro de téléphone fictif
***

La plupart des sites de vente en ligne demandent le numéro de ses clients. Ce n'est pas pour nous appeler en cas de problème et rarement pour que le livreur nous appelle en arrivant. Ils revendent l'information.

Ne pas s'embêter, renseigner un faux numéro, 01 00 00 00 00, par exemple. Et si l'on est joueur, le numéro de téléphone du site en question.

## Numéro de téléphone temporaire
***

Un numéro de téléphone temporaire peut permettre de contourner aussi le problème si le numéro doit être vérifié par SMS.

Malheureusement, ils sont tellement utilisés, qu'il est de moins en moins possible, et recommandé, de s'inscrire avec sur des sites comme Google, Facebook, etc. Ils peuvent tout de même être utiles sur des sites moins connus et moins sensibles demandant une vérification par SMS.

{{<hint danger>}}**Attention :** Pour recevoir un code de vérification uniquement. Ne jamais utiliser pour l'authentification à deux facteurs (2FA) ou pour recevoir des informations personnelles et sensibles !{{</hint>}}

{{<block info>}}

Attention, les sms reçus sont publics ! Utilisation à vos risques.

-  [quackr.io](https://quackr.io/)
-  [smsnator.com](https://smsnator.com/)
-  [temp-sms.org](https://temp-sms.org/)
-  [freephonenum.com](https://freephonenum.com/)

{{</block>}}

## Numéro de téléphone secondaire
***

Dans le cas où le numéro doit être vérifié et qu'un numéro temporaire ne fonctionne pas, prendre un numéro secondaire reste la seule option aujourd'hui.

Également, pour vendre des objets sur leboncoin ou autre, ne pas utiliser son numéro principal.

Il y a deux façon de faire :
-  Acheter une carte SIM prépayé ou un forfait sans engagement en bureau de tabac/taxiphone/grande surface/en ligne. Une carte d'identité sera demandée à l'achat ou à l'activation de la SIM.
-  Acheter un autre numéro en ligne. Certains services, comme Gmail, ne fonctionnent pas avec des numéros virtuels (VOIP). Il faut obligatoirement passer par un service qui vend des numéros liés à de vraies cartes SIM.

Sans engagement, ces numéros pourront être résiliés et changés facilement si ils sont compromis.

{{<block info>}}

### Crypton | [crypton.sh](https://crypton.sh/)
***

Permet d'obtenir un numéro de téléphone valide afin de valider n'importe quel compte en ligne. Il semblerait que ce service puisse procurer un numéro anonymement, sans adresse email, en paiement par crypto-monnaie et en passant par Tor.

-  Location de SIM privée au Royaume-Uni.
-  Numéro Anglais uniquement (pour l'instant).
-  Paiement possible en crypto-monnaie.
-  Service disponible via Tor à l'adresse .onion en bas à gauche sur leur page.
-  Inscription par email optionnelle.
-  8€/mois débité sur le crédit disponible sur son compte.
-  Recevoir et envoyer des sms uniquement. La réception de sms est gratuite. L'envoi des sms est facturé depuis le crédit disponible sur son compte et le prix varie selon le pays de destination.
-  Il n'est pas garanti que la vérification par sms marche pour tous les services car le numéro provient du Royaume-Uni. Sans VPN et un compte associés à une localisation au Royaume-Uni, des services pourraient rejeter l'utilisation du numéro. 
-  Ce service est propriétaire et n'est pas _open source_.
{{</block>}}

{{<hint warning>}}**Attention :** Les techniques ci-dessous ne sont pas anonymes car son numéro de téléphone principal et un paiement par carte bancaire seront demandés et laisseront des traces.{{</hint>}}

{{<block info>}}

### MySudo | [mysudo.com](https://mysudo.com/)
***

MySudo est un service permettant d'obtenir entre autre :
-  Des numéros de téléphone secondaire américains ou canadiens.
-  Des adresses email secondaires.
-  Des cartes bancaires virtuelles (réservé aux résidents américains pour l'instant).

Un compte gratuit et deux comptes payant sont disponibles.

Sudo Platform propose une grande quantité de produits d'appel pour brouiller les pistes sur Internet. Il est déconseillé d'utiliser tous les produits proposés par l'entreprise Anonyome Labs. Centraliser ses informations sensibles n'est jamais une bonne idée. Se contenter d'utiliser le service pour un numéro secondaire si besoin.

-  Il n'est pas garanti que la vérification par sms marche pour tous les services comme Gmail, Apple, Microsoft, WhatsApp etc.
-  Ce service est propriétaire et n'est pas _open source_.

{{</block>}}

{{<block info>}}

### OnOff | [onoff.app](https://www.onoff.app/)
***

Application mobile permettant d'acheter d'autres numéros de téléphone, dans différents pays et de les utiliser au travers de l'application mobile ou du site Internet.

-  Il n'est pas garanti que la vérification par sms marche pour tous les services comme Gmail, Apple, Microsoft, WhatsApp etc. Source : [onoff.app](https://support.onoff.app/hc/fr/articles/207053109-Puis-je-utiliser-un-num%C3%A9ro-onoff-pour-valider-mon-compte-WhatsApp-Facebook-Viber-)
-  Ce service est propriétaire et n'est pas _open source_.


{{</block>}}

{{< prev relref="/docs/outils/inscriptions/emails-temporaires" >}} < Emails temporaires {{< /prev >}}
{{< next relref="/docs/outils/inscriptions/cartes-bancaires" >}} Cartes bancaires > {{< /next >}}
