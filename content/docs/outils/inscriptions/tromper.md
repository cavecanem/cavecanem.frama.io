---
title: "Tromper le service"
bookToc: false
weight: 2
description: Comment tromper les services en ligne qui demandent des renseignements personnels ?
---

# Tromper le service
***

La meilleure barrière à l'utilisation de nos données personnelles sur Internet est tout simplement de tromper sur tout, partout, tout le temps. Changer son adresse IP avec un VPN, utiliser des alias et emails temporaires, chiffrer ses fichiers, utiliser des machines virtuelles pour accéder à Internet et bien sûr mentir sur son identité est fortement recommandé. Moins un service en sait sur soi mieux c'est.

Comme écrit plus haut, il ne faut jamais donner d'indices sur sa véritable identité. Ces informations risquent de mener à soi au travers d'entreprises qui achètent et regroupent les informations collectées. Ce qui nous transforme en cible pour les annonceurs, les gouvernements, les entreprises et les pirates.

## Fausse identité
***

Dans certains cas où l'utilisation d'un pseudonyme ne suffirait pas, il sera donc utile d'inventer une identité complète.

{{<block info>}}

### Créer ses identités
***
-  Opter pour un nom et un prénom fictif.
-  Utiliser une fausse adresse.
-  Utiliser une fausse date de naissance.
-  Utiliser un [alias](/docs/outils/emails/alias.html) ou un [email temporaire](/docs/outils/inscriptions/emails-temporaires.html) selon le service.
-  Utiliser un faux numéro de téléphone. Si un numéro de téléphone valide est obligatoire, utiliser numéro de téléphone différent de votre numéro principal (voir [Numéros de téléphone](/docs/outils/inscriptions/numeros-tel.html)).
-  Ne donner aucun renseignement réel aux questions secrètes.
-  Utiliser une photo de profil qui ne vous montre pas.
-  Lors d'une vérification à deux facteurs (2FA), utiliser un autre alias (si c'est possible par email), sinon, un numéro de téléphone secondaire.

Comme ces informations pourraient être redemandées par le service, en cas de perte de mot de passe par exemple, il faut apprendre son ou ses pseudonymes par cœur ou utiliser un carnet pour s'en souvenir si besoin ultérieur.

{{</block>}}

{{<block info>}}

### Fake-it | [fake-it.ws](https://fake-it.ws/fr/)
***
Ce site permet de générer en un clic, à partir d'un pays sélectionné, toute une identité fictive tout à fait vraisemblable.

{{</block>}}

{{<block info>}}
### Not Real Human | [notarealhuman.com](http://notarealhuman.com/)
***
Site de visages générés par ordinateur afin de créer un avatar d'une personne qui n'existe pas.

{{</block>}}

{{< prev relref="/docs/outils/inscriptions/lire-les-cgu" >}} < Lire les CGU {{< /prev >}}
{{< next relref="/docs/outils/inscriptions/emails-temporaires" >}} Emails temporaires > {{< /next >}}
