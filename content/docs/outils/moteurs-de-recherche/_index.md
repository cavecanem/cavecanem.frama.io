---
title: "Moteurs de recherche"
bookToc: false
weight: 2
description: Pour faire ses recherches sur Internet, les moteurs de recherches sont multiples et ne se valent pas concernant le respect de la vie privée de ses utilisateurs.
---

# Moteurs de recherche
***

Les moteurs de recherche classiques pistent, récupèrent et vendent nos informations de navigation et de recherches.

Ils enregistrent pour toujours:

-  L'adresse IP.
-  La date et l'heure de la requête.
-  La localisation de l'appareil au moment de la recherche.
-  Les termes de recherche de la requête.
-  Les métadonnées de l'utilisateur.

De plus:

-  Un ID de cookie est déposé dans le dossier des cookies du navigateur.
-  Il identifie notre appareil de manière unique.  
Grâce à lui, le fournisseur peut remonter jusqu'à vous à partir d'une simple demande de recherche.
-  Ils enferment l'utilisateur dans une bulle filtrante.

## Bulle filtrante
***

Pour affiner au mieux les résultats, les moteurs de recherche classiques utilisent des algorithmes qui se servent de l'analyse de nos comptes ou de nos habitudes (recherches régulières, inspection des cookies, _Like_ et _Follow_ sur les réseaux sociaux, etc.).

Cela a pour effet des résultats plus "précis" lors des recherches. Mais cette bulle contribue à fermer à l'utilisateur des résultats plus riches et plus diversifiés.

Le problème est le même qu'avec Facebook ou Youtube : cette bulle imposée a pour effet de créer un entre-soi et une pensée à sens unique basée sur des recherches ou des comportements tronqués. Les résultats qui ne collent pas forcément à nos habitudes sont ignorés ou placés plus loin dans la liste. Les utilisateurs, avec la même recherche, n'ont pas accès aux mêmes résultats. Ceux-ci diffèrent selon des critères cachés qui peuvent être de l'ordre de l'orientation politique ou sexuelle, raciale, de genre, etc.

{{<block sain>}}

### Pratique saine
***

Utiliser un moteur de recherche privé:

-  Pas d'utilisations de nos données.
-  Pas d'enregistrement de nos recherches.
-  Pas de publicités.
-  Pas de pistage.
-  Pas de bulle filtrante.
-  Pas d'enregistrement de localisation.
-  Pas d'identifiant unique.


{{</block>}}

{{<hint danger>}}

**Poubelle:** Google, Bing, Yahoo, etc.

{{</hint>}}

Tous les moteurs ci-dessous lancent les recherches sur les plus connus et en donne le résultat tout en faisant office d'intermédiaire et donc de part-feu.

{{<block info>}}

### DuckDuckGo | [duckduckgo.com](https://duckduckgo.com)
***

-  Aucune censure dans l'affichage des résultats.
-  Un des moteurs les plus réputés
-  Service Américain
-  Résultats de Bing et Yahoo.

{{</block>}}

{{<block info>}}

### Qwant | [qwant.com](https://www.qwant.com/)
***

-  Interface et résultats plus ou moins similaires à Google mais protègeant la vie privée.
-  Service Français

{{</block>}}

{{<block info>}}

### Swisscows | [swisscows.com](https://swisscows.com/)
***

-  Excellente politique de confidentialité
-  Moteur de recherche pour la famille : la violence et la pornographie sont exclus de ses résultats.
-  Financement par les dons et les sponsors (qui peuvent avoir une bannière publicitaire).
-  Service et _Datacenter_ Suisses

{{</block>}}

{{<block info>}}

### Startpage | [startpage.com](https://www.startpage.com/)
***

-  Résultats de Google mais protégeant de la vie privée.
-  Service Hollandais
-  Attention, System1 a partiellement acquis Startpage en décembre 2018. Cette entreprise américaine a une longue histoire de traque des utilisateurs à des fins publicitaires. Bien qu'ils se soient bien défendus ([ici entre autre](https://support.startpage.com/index.php?/Knowledgebase/Article/View/1277/0/startpage-ceo-robert-beens-discusses-the-investment-from-privacy-one--system1)) sur le fait que Startpage restera un moteur de recherche respectueux de la vie privée il faut rester sur ses gardes.

{{</block>}}

Si besoin absolue et récurrente de faire une recherche Google:

-  Utiliser Startpage.
-  Ou utiliser un second navigateur qui n'est pas lié à ses activités en ligne (achats, réseaux sociaux, etc.) pour compartimenter la recherche.
-  Moins recommandé que la compartimentation, ajouter à Firefox l'extension: [Google search link fix](https://addons.mozilla.org/firefox/addon/google-search-link-fix/). Cela empêche les pages de Google et Yandex de modifier les liens des résultats quand on clique dessus. C'est utile lorsque l'on copie les liens pour les envoyer, mais cela empêche aussi les moteurs de recherche d'enregistrer les clics.
