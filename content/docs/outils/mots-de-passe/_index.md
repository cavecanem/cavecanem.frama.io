---
title: "Mots de passe"
bookCollapseSection: true
bookToc: false
weight: 4
description: Choisir ses mots de passe semble évident. Pourtant la majorité des internautes ne savent pas comment utiliser et gérer des mots de passe forts et ainsi éviter le piratage ou l'espionnage de leurs comptes.
---

# Mots de passe
***

Les mots de passe sont la base de la vie numérique. Quelqu'un qui en prend possession peut provoquer des dégâts énormes sur notre vie réelle.

## Comment et pourquoi?
***

Cette vidéo montre assez bien comment sont crackés régulièrement les mots de passe en Brute Force Attack: [Cracker des mots de passe](https://invidious.snopyta.org/watch?v=Z8nGpUOQPaA) <span class="gris">12min 34s</span>

Un tour sur les réseaux sociaux peut aider à définir une base de mots à tester en premier lieu, date de naissance, nom du chien, ville natale, prénom du conjoint etc.

Pour cracker un mot de passe court contenant des mots connus, on utilise une attaque par dictionnaire. Ces mots sont testés en combinaison pour trouver rapidement un mot de passe.

Donc plus le mot de passe est long et complexe, plus il faut du temps à un ordinateur pour calculer les possibilités.

Le temps augmente encore quand les mots n'existent pas dans un dictionnaire, et que des symboles, chiffres, majuscules et minuscules sont présents.

Mot de passe complexe : **R\#mQh2 74w%\*ML QyhKU@J** (crack en + de 10000 siècles)

Phrase de passe complexe : **J'en-ai-raf-de-ce-mdp-sur-insta!** (crack en + de 10000 siècles)

Pour vérifier la force d'un mot de passe : [password.kaspersky.com](https://password.kaspersky.com/fr/)

## Même mot de passe partout
***

Lorsqu'une attaque se produit, comme régulièrement sur les serveurs ou par email, les informations récupérées sont utilisées ou revendues sur le marché noir (noms, adresse, adresse mail, mot de passe, numéro de CB, etc.).

La combinaison adresse mail + mot de passe est testée sur tous les services courants pour entrer sur un compte, l'utiliser ou revendre: le compte et les informations importantes qu'il contient, et parfois, pour demander une rançon.

D'où l'importance de mots de passe toujours différents afin de ne pas avoir l'ensemble de ses comptes compromis.

{{<block sain>}}

### Pratique saine

* * *

-   Choisir des mots de passe complexes <u>et différents</u> pour chaque site.
-   Plus les informations sont sensibles, plus le mot de passe doit être long.
-   Éviter, si ce n'est pas utile, de créer un compte sur un site (Vente en ligne par ex.).
-   Stocker de manière sûre ses mots de passe > voir [Gestionnaires](docs/outils/mots-de-passe/gestionnaires.html).
-   Choisir un mot ou une phrase de passe maître complexe pour sécuriser l'ensemble de ses mots de passe.
-   Vérifier la force d'un mot de passe: [password.kaspersky.com/fr/](https://password.kaspersky.com/fr/)
-   Faire une sauvegarde de ses mots de passe (dans un carnet caché chez soi par ex.) si une perte numérique survient.
-   Ne jamais stocker de mots de passe sur un navigateur (Safari/Firefox/Chrome/Edge etc.).
-   Changer immédiatement une mauvaise méthode ou un mauvais gestionnaire pour tous ses mots de passe.
-   <u>Vérifier régulièrement</u> avec son e-mail si des mots de passe sont compromis pour les changer: [haveibeenpwned.com](https://haveibeenpwned.com/) ou [monitor.firefox.com](https://monitor.firefox.com/).
-   Si c'est le cas, changer immédiatement le mot de passe. Et, si cette combinaison a été utilisée ailleurs, changer son mot de passe sur tous les services en question.

{{</block>}}

{{< next relref="/docs/outils/mots-de-passe/gestionnaires" >}} Gestionnaires de mots de passe > {{< /next >}}
