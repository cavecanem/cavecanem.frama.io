---
title: "Gestionnaires"
bookToc: false
---
# Gestionnaires de mots de passe
***

Gérer ses mots de passe, quand ils sont tous différents est assez compliqué. C'est pourquoi les gestionnaires de mots de passe sont utilisés.

Il permettent d'enregistrer ses mots de passe, de créer des sauvegardes et de générer facilement des mots ou phrase de passe complexes.

Ils sont eux-mêmes sécurisés par un mot ou une phrase de passe que l'utilisateur définie et doit utiliser pour ouvrir le gestionnaire. Ce mot ou phrase de passe maître doit être mémorisable mais également complexe. Si il saute, tout le reste saute.

{{<hint danger>}}
**Poubelle:** 1Password, iCloud Keychain, Enpass, LastPass, DashLane, etc.</span>

Il ne faut jamais utiliser les services de sauvegarde de mots de passe des navigateurs. Ils sont beaucoup trop facile à cracker pour récupérer intégralement tout ce qui est stocké.
{{</hint>}}

Voici les trois solutions les plus recommandées pour la création et la gestion de ses mots de passe:

{{<block info>}}

### Cerveau, carnet

* * *

Pour éviter tout stockage numérique.

-   Enregistrer chaque mot de passe dans sa mémoire avec une formule.
-   Utiliser un carnet (à condition de ne pas le perdre ou se le faire voler)
-   Utiliser [Master Password](https://masterpassword.app/) avec Internet (Générateur, 1 seul mot de passe maître et aucune base de donnée)

Il y existe de multiples formules pour générer ses mots de passe complexes seul:

– [Mots de passes forts + sécurisés : Recette pour les créer et mémoriser](https://invidious.snopyta.org/watch?v=pHZEDlJJ9lY) <span class="gris">6min 14s</span>  
– [Comment créer un mot de passe sécurisé et s'en rappeler ?](https://invidious.snopyta.org/watch?v=24HlriEAm_A) <span class="gris">3min 55s</span>  
– [Diceware](https://duckduckgo.com/?t=ffab&q=diceware+password&atb=v217-5__&iax=videos&ia=videos) (Lancer de dés) - Diverses videos

\> Trouver sa propre recette et l'appliquer.

{{</block>}}

{{<block info>}}

### Bitwarden | [bitwarden.com](https://bitwarden.com/)

* * *

Stockage en ligne (Cloud) pour un accès simplifié sur tous ses appareils.

_open source_ et très simple d'utilisation, il génère des mots de passe complexes et sauvegarde en chiffrant de bout-en-bout les informations.

Bitwarden est l'une des solutions les plus simples et les plus sûres pour stocker ses identifiants et mots de passe tout en les gardant synchronisés entre tous ses appareils.

Une fonction de remplissage automatique est également disponible pour simplifier la copie dans les navigateurs ou applications.

– Tutoriel: [www.youtube.com/watch?v=bzeL-zwCA1Y](https://invidious.snopyta.org/watch?v=bzeL-zwCA1Y) <span class="gris">25min 44s</span>

{{</block>}}

{{<block info>}}

### KeePassXC | [keepassxc.org](https://keepassxc.org/)

* * *

Stockage sur ordi, clé USB, ou autre pour éviter le stockage en Cloud.

KeePassXC est une branche communautaire de KeePassX, le portage
multiplateforme de KeePass. Son objectif est de l'étendre et de
l'améliorer avec de nouvelles fonctionnalités et des correctifs pour
fournir un gestionnaire de mots de passe moderne, multiplateforme et
_open source_ riche en fonctionnalités.

Simple d'utilisation et se stocke en local (disque dur, clé usb par
exemple).

– Tutoriel: [www.youtube.com/watch?v=oPRqO8b3n1w](https://invidious.snopyta.org/watch?v=oPRqO8b3n1w) <span class="gris">18min 10s</span>

Il peut tout de même s'utiliser en Cloud si besoin (chercher des tutoriels si intéressé).

{{</block>}}

{{< prev relref="/docs/outils/mots-de-passe" >}} < Mots de passe {{< /prev >}}
{{< next relref="/docs/outils/mots-de-passe/plus" >}} Plus encore > {{< /next >}}
