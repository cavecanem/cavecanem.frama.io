---
title: "Plus encore"
bookToc: false
---
# Protections supplémentaires

* * *

Il arrive que les attaques pour récupérer des informations soient menées par un _keyloger_ (enregistreur de touche).

Un _keyloger_ permet:

-  d'enregistrer toutes les touches d'un clavier qui sont appuyées.
-  de faire des captures d'écran à chaque clic de la souris.
-  de connaître la position de la souris ou des doigts sur un téléphone ou une tablette.
-  de capturer ce qui est enregistré dans le presse papier (quand on fait un copier-collé).

Ce qui veut dire, que même avec l'utilisation d'un gestionnaire de mots de passe sécurisé, il est encore possible pour un intrus de connaître nos mots de passe.

Démonstration: [Clavier virtuel ou gestionnaire de mot de passe ?](https://invidious.snopyta.org/watch?v=n-ckIi7QC2I) <span class="gris">11min 15s</span>

Même si ce sont des cas plus extrêmes et rares, ils existent. Lorsque l'on a besoin de se protéger, mieux vaut savoir comment le faire efficacement.

## Parades
***

1.  Avec Bitwarden, il est possible de remplir automatiquement le mot de passe sans utiliser un copier-coller. Avec l'extension installée dans le navigateur, l'ouvrir lorsqu'on est sur le site qui les demande et sélectionner le mot de passe en cliquant sur celui-ci, l'extension remplit automatiquement les champs. Sinon faire un clic droit dans le champ puis Bitwarden > Auto-fill.
1.  Enregistrer uniquement dans le gestionnaire, les mots de passe des comptes qui ne peuvent pas avoir d'impact négatif sur sa vie réel.
1.  Pour tous les sites vraiment sensibles (banque, réseaux sociaux, emails, etc.), utiliser un mot de passe complexe que l'on retient dans sa tête ou dans un carnet, voir [gestionnaires > cerveau, carnet](gestionnaires.html).
1.  Utiliser un clavier virtuel pour écrire ses mots de passe. La virtualisation du clavier permettra d'éviter un peu plus la possibilité que les touches soient enregistrées.

{{<tabs "OS">}}
{{<tab "Windows">}}

Menu Démarrer > Exécuter > Taper `osk` > Touche Entrée

ou

Menu Démarrer > Tous les programmes > (Accessoires >) Options d'ergonomie > Clavier visuel

{{</tab>}}
{{<tab "macOS">}}

1.  Préférences Système > Clavier > Afficher les visualisateurs de clavier ... > Cocher

1.  Barre de menu en haut à droite > Clic sur la nouvelle icône > Afficher le visualisateur de clavier

{{</tab>}}
{{</tabs>}}

{{< prev relref="/docs/outils/mots-de-passe/gestionnaires" >}} < Gestionnaires {{< /prev >}}
