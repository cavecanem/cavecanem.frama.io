---
title: "Navigateurs"
bookCollapseSection: true
bookToc: false
weight: 2
description: Pour naviguer en toute sécurité et confidentialité, un bon navigateur, configuré, avec des extensions installées est un choix essentiel.
---

# Navigateurs
***

C'est un choix essentiel pour naviguer sur Internet en sécurité, sans traqueurs, sans publicités, de manière rapide et en utilisant moins les ressources de ses appareils.

Les navigateurs ne sont pas tous égaux:
-  Chrome, par exemple, cache tous les principaux paramètres permettant à l'utilisateur de protéger sa vie privée et communique ainsi à Google les informations des utilisateurs.
-  Safari, même plus sécurisé, communique aussi des informations à Apple.

Les navigateurs, même plus sécurisés à la base, ont toujours besoin d'être configurés et d'avoir des extensions qui augmentent leur puissance de protection.

Le choix de bonnes extensions est primordiale pour bloquer les contenus indésirables comme les traqueurs ou la publicité par exemple. En les bloquant, ils ne seront pas chargés. L'utilisateur gagne en sécurité, en confidentialité et en temps d'affichage des pages.

## Le cache
***

Le cache permet de mettre de côté des éléments d'un site Internet: images, logos, menu, fontes, arrière-plan, etc.. La navigation sur le site n'aura pas à charger à chaque fois ces éléments, ce qui permet une navigation plus rapide.

Le navigateur vide automatiquement au bout de plusieurs heures un élément qu'il a mis en cache. Il est parfois nécessaire de le vider soi-même pour nettoyer tout élément subsistant.

## Les cookies
***

Les cookies stockent également des informations, si ce n'est qu'ils se préoccupent moins des éléments graphiques.

La principale fonction est de permettre au serveur d'un site Internet de stocker lui-même des informations dans l'ordinateur de l'utilisateur pour les réutiliser à chacune des connexions au serveur: adresses mail, mots de passe, noms d'utilisateurs, préférences du site, contenus des paniers d'achat, recherches, etc. Ils permettent d'éviter à l'utilisateur de devoir configurer ou de retaper des informations récurrentes, comme une authentification par exemple.

Stockés sous forme de texte, ces renseignements peuvent être utilisés par le site mais également par d'autres sites Internet sous certaines conditions qu'il a lui-même décidé. Ainsi Google peut permettre à ses cookies d'interagir avec tous ses autres services, mais également avec des services associés tiers, sites personnels ou d'entreprises, annonceurs publicitaires, sites de vente en ligne, sites partenaires etc.

Les navigateurs permettent de configurer la durée de stockage des cookies et même de les désactiver, ce qui a, par contre, pour effet de bloquer certaines fonctionnalités.

Il est donc important de configurer le comportement des cookies et de les supprimer régulièrement pour éviter que ces informations ne soient utilisées par d'autres services ou sites malveillants (récupération de noms d'utilisateurs, mots de passe, liste de recherche, panier d'achat etc.) Une [navigation privée](/docs/outils/navigateurs/navigation-privee.html) supprimera automatiquement les cookies de la session à sa fermeture (explications à la page suivante).

## L'historique de navigation
***

L'historique aide les utilisateurs à revenir sur leurs navigations récentes.

N'importe quelle personne utilisant le navigateur sur un ordinateur peut connaître l'historique de son propriétaire. De plus, certains cookies sont configurés pour pouvoir regarder l'historique de l'utilisateur.

Il est donc recommandé de vider régulièrement son historique et de créer des marque-pages (ou signets) si besoin.

## Marque-pages
***

Les marque-pages aident à mémoriser les adresses des sites pour les retrouver plus facilement.

De manière générale, il ne faut pas d'accéder à un site Internet en passant par un moteur de recherche quand ce n'est pas strictement nécessaire.

1.  On évite que le moteur (Google, Bing, Yahoo, etc.) enregistre et piste la recherche et les clics inutilement.
2.  On évite de créer du trafic Internet supplémentaire qui gaspille à force énormément d'énergie électrique pour rien.

## HTTPS
***

Il y a plusieurs années les sites commençaient par www puis http et maintenant https.

Ce sont des protocoles de communication avec le serveur qui héberge le site. HTTPS est un protocole de [chiffrement en transit](chiffrement/en-transit.html) qui protège la communication avec un site Web, il est représenté par un cadenas vert à gauche de l'adresse du site Web.

Des entreprises délivrent les certificats prouvant que le protocole est bien établi, quand celui-ci expire, le gérant du site doit renouveler son certificat, et les internautes risquent d'être avertis que le site n'est plus sécurisé si cela n'a pas été fait.

Https chiffre ce que l'on renseigne sur un site, par exemple: ses numéros de carte bancaire, mots de passe, identifiants, le texte sur un blog, etc.

Https empêche ainsi que tout individu qui se placerait au milieu de la communication puisse voir, en clair, ce qui est écrit.

Ne jamais utiliser de sites qui ne sont pas en https lorsque l'on renseigne une information confidentielle.

{{<hint danger>}}

**Attention:** Un site HTTPS sécurise uniquement la communication entre l'utilisateur et le serveur. Ce n'est pas pour autant que le site est lui, sécurisé. Il ne faut pas confondre.

{{</hint>}}

{{<block sain>}}

### Pratique saine
* * *

-  Utiliser un navigateur bien configuré et qui contient des extensions solides et fiables.
-  Utiliser le mode de navigation privée le plus possible.
-  Ne jamais stocker ses mots de passe dans son navigateur > voir le chapitre [Mots de Passe](mots-de-passe.html).
-  Ne jamais demander à un site de retenir ses identifiants (cookies).
-  Vider régulièrement le cache, l'historique et les cookies si ce n'est pas automatique.
-  Ne jamais donner d'informations confidentielles sur un site qui n'a pas http<u>**s**</u>://
-  Ne pas remplir son navigateur d'extensions redondantes.
-  Ne jamais installer des extensions non sécurisées (traqueurs assurés).
-  Éviter le passage inutile et traçable par un moteur de recherche. Faire des marque-pages pour accéder directement aux sites récurrents ou écrire l'adresse entièrement.

{{</block>}}

{{< next relref="/docs/outils/navigateurs/navigation-privee" >}} Navigation privée > {{< /next >}}
