---
title: "Choix de navigateurs"
weight: 2
bookToc: false
---

# Choix de navigateurs
***

Il est recommandé d'utiliser des navigateurs _open source_ comme Tor pour son anonymat et Firefox lorsque l'anonymat est moins nécessaire.

**Firefox** est un projet non commercial, maintenu principalement par des volontaires. Ce statut permet à Firefox de créer un navigateur sans avoir à collecter des données sur ses utilisateurs pour se financer.

**Tor** est basé sur Firefox et souvent associé au _Dark Net_, mais il sert surtout à naviguer de manière anonyme. C’est d’ailleurs sa fonction principale. Tor est très utile dans les pays où la liberté est limitée et Internet censuré. Il est plus difficile d’utilisation, car des sites peuvent ne pas fonctionner parfaitement, mais il est possible de l’utiliser quotidiennement.

Le navigateur se connecte au réseau Tor, un réseau d’ordinateurs volontaires qui masquent notre connexion. Cela a pour effet secondaire de ralentir le temps de chargement des pages.

Il ne faut pas utiliser le réseau Tor pour se connecter sur des sites importants (banque, achat en ligne, impôts, etc). Le privilégier dans le cas de recherches privées (santé, questions personnelles, recherches, journalisme). Pour les réseaux sociaux, ne pas se connecter via Tor à moins que le compte ait été créé complètement anonymement et qu'il n'ait jamais été utilisé en dehors de Tor, sinon l'identité sera révélée.

{{< tabs "OS" >}}
{{< tab "Windows, macOS, Linux" >}}

{{<hint danger>}}
**Poubelle :** Google Chrome, Safari, Edge, Opera, Internet Explorer, etc.
{{</hint>}}

{{<block info>}}

### Tor | [torproject.org](https://www.torproject.org/fr/)
***

-   Sécurité maximale et anonymat.
-   <span class="orange">Plus lent que la moyenne</span> (fait de l'anonymisation de la navigation).

{{</block>}}

{{<block info>}}

### Firefox | [mozilla.org](https://www.mozilla.org/fr/firefox/new/)
***

-   <span class="orange">Firefox doit être configuré et avoir des extensions pour protéger correctement l'utilisateur.</span>
-   Une fois fait, très bon, rapide et sécurisé.

{{</block>}}

Autres options :
-   [Brave](https://brave.com/fr/) | <span class="orange">Configuration et extensions nécessaire.</span> Une fois fait, très bon, rapide et sécurisé.
-   [Iridium](https://iridiumbrowser.de) | Remplace Google Chrome avec des bons paramètres de sécurité. <span class="orange">Configuration et extensions nécessaires.</span>

{{< /tab >}}

{{< tab "Android" >}}

{{<hint danger>}}**Poubelle :**  Google Chrome, Edge, Opera, etc.{{</hint>}}

{{<block info>}}

### Tor | [torproject.org](https://www.torproject.org/fr/)
***

-   Sécurité maximale et anonymat.
-   <span class="orange">Plus lent que la moyenne</span> (fait de l'anonymisation de la navigation).

{{</block>}}

{{<block info>}}

### Firefox | [mozilla.org](https://www.mozilla.org/fr/firefox/new/)
***

-   <span class="orange">Firefox a besoin d'être configuré et d'avoir des extensions pour être efficace.</span>
-   Une fois fait, très bon, rapide et sécurisé.

{{</block>}}

Autres options :

-   [Bromite](https://www.bromite.org/) | Très bon navigateur _open source_.
-   [Firefox Focus](https://play.google.com/store/apps/details?id=org.mozilla.focus&hl=fr) | Bonne sécurité intégrée.
-   [Brave](https://brave.com/fr/) | <span class="orange">Configuration et extensions nécessaires.</span> Une fois fait, très bon, rapide et sécurisé.
-   [DuckDuckGo Privacy Browser](https://duckduckgo.com/app) | Bonne sécurité intégrée.

{{< /tab >}}

{{< tab "iOS" >}}

{{<hint danger>}}

**Poubelle :** Google Chrome, Opera, etc.



**À noter :**

-  Il est <u>impossible de supprimer Safari</u>.
-  Il est possible de définir un autre navigateur par défaut que Safari <u>seulement depuis iOS 14</u>.
-  Il est impossible d'ajouter des extensions pour les navigateurs dans iOS.

Il est donc recommandé d'installer au minimum Firefox Focus et Firefox; si Onion ne vous semble pas indispensable.

{{</hint>}}

{{<block info>}}

### Onion | [onionbrowser.com](https://onionbrowser.com/)
***

-   Tor pour iOS
-   Sécurité maximale et anonymat.
-   <span class="orange">Plus lent que la moyenne</span> (fait de l'anonymisation de la navigation).

{{</block>}}

{{<block info>}}

### Firefox Focus | [App Store](https://itunes.apple.com/app/id1055677337)
***

-   Léger, rapide. Bonne sécurité intégrée.
-   Ajoute de la sécurité à Safari > [Tutoriel](https://support.mozilla.org/fr/kb/firefox-focus)

{{</block>}}

{{<block info>}}

### Firefox | [mozilla.org](https://www.mozilla.org/fr/firefox/new/)
***

-   <span class="orange">Configuration minimale nécessaire.</span>

{{</block>}}

Autres options :
-   [DuckDuckGo Privacy Browser](https://duckduckgo.com/app) | Léger, rapide. Bonne sécurité intégrée.
-   [Brave](https://brave.com/fr/) | <span class="orange">Configuration minimale nécessaire.</span>

{{< /tab >}}
{{< /tabs >}}

{{< prev relref="/docs/outils/navigateurs/navigation-privee" >}} < Navigation privée {{< /prev >}}
{{< next relref="/docs/outils/navigateurs/configurations" >}} Configurations > {{< /next >}}
