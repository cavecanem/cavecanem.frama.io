---
title: "Configurations"
weight: 3
bookToc: false
---

# Configurations

* * *

La configuration de ses navigateurs est nécessaire pour naviguer de manière sécurisé.

{{< tabs "OS" >}}

{{< tab "Windows, macOS, Linux" >}}

{{<block info>}}

## Tor

* * *

Après l'avoir installé et ouvert, aller dans Préférences, puis:

<u>Privacy & Security:</u>
-   History > Always use private browsing mode > <span class="vert">Cocher</span>
-   Address Bar > <span class="vert">Tout décocher</span>
-   Permissions > Localisation/Camera/Micro/Notifications > Settings > <span class="vert">Cocher Block new requests asking to access your location</span>
-   Permissions > Block Websites from automatically playing sound/Block pop-up windows/Warn you when Websites try to install add-ons/Prevent accessibility services from accessing your browser > <span class="vert">Tout cocher</span>
-   Tor Browser Data Collection and Use > <span class="vert">Tout décocher</span>
-   Sécurity > <span class="vert">Cocher Safest</span>
-   Deceptive Content and Dangerous > <span class="vert">Tout cocher</span>
-   Certificates > <span class="vert">Cocher Ask every time</span>

Pas de connaissance pour l'instant sur la sécurité ajoutée avec des extensions dans Tor..

{{</block>}}

{{<block info>}}

## Firefox

* * *

Après l'avoir installé et ouvert, aller dans Préférences, puis:

<u>Général:</u>

-   Firefox comme navigateur principal: <span class="vert">Oui</span>
-   Installation des mises à jour automatique : <span class="vert">Oui</span>

<u>Recherche:</u>

-   Moteur de recherche par défaut: [DuckDuckGo](https://duckduckgo.com/) ou [Qwant](https://www.qwant.com/) ou [StartPage](https://www.startpage.com/)
-   Moteur de recherche One-Click: [DuckDuckGo](https://duckduckgo.com/), [Qwant](https://www.qwant.com/), [StartPage](https://www.startpage.com/)  
<span class="rouge">> Poubelle: Google, Bing, Yahoo, etc.</span>

<u>Sécurité et vie privée:</u>

-   Protection contre le traçage: <span class="vert">Strict</span> (recommandé, mais si vous observez des problèmes: normal)
-   Envoyer aux site "Do not Track": <span class="vert">Toujours</span>
-   Cookies et données de site: Supprimer quand Firefox se ferme: <span class="vert">Oui</span>
    -   (par contre quand on a l'habitude d'aller 20 fois par jours sur un site et de demander de retenir le mot de passe, cette option l'effacera à chaque fermeture. Ce n'est vraiment pas recommandé de faire retenir ses mots de passe à son navigateur, mais c'est vous qui voyez.)
-   Historique: <span class="vert">Le mieux c'est jamais</span>, faire des marques-pages. <span class="vert">Sinon penser à l'effacer régulièrement.</span>
-   Collection de données Firefox: <span class="vert">Tout décocher.</span>
-   Sécurité: <span class="vert">Tout cocher.</span>
-   Certificats: <span class="vert">Toujours demander.</span>

<u>À côté de la barre d'adresse</u> > Si il y a Pocket <span class="vert">> Clic droit > Supprimer ou désinstaller et ne pas utiliser ce service.</span>

Pour pousser encore plus loin > [about:config](https://privacytools.dreads-unlock.fr/#about_config)

<span class="rouge">Ce n'est pas fini. Il faut installer des extensions</span>

{{< button relref="/docs/outils/navigateurs/extensions" >}} > Installer des extensions {{< /button >}}

{{</block>}}

{{< /tab >}}

{{< tab "Android" >}}

{{<block info>}}

## Firefox

* * *


À venir, pas encore d'informations détaillées

Faire à quelque chose près la même chose que pour Windows, macOS, Linux et installer des extensions.

{{< button relref="/docs/outils/navigateurs/extensions" >}} > Installer des extensions {{< /button >}}

{{</block>}}

{{<block info>}}

## Firefox Focus

* * *

Après l'avoir installé et ouvert, aller dans Paramètres, puis:

<u>Protection contre le pistage:</u>

-   Bloquer les traqueurs pub: <span class="vert">Cocher</span>
-   Bloquer les traqueurs stats: <span class="vert">Cocher</span>
-   Bloquer les traqueurs de réseaux sociaux: <span class="vert">Cocher</span>
-   Bloquer les autres traqueurs de contenu: <span class="vert">Cocher</span> Si problèmes de navigation, décocher.

<u>Envoyer des données d'utilisation:</u> <span class="vert">Décocher</span>

<u>Moteur de recherche:</u> [DuckDuckGo](https://duckduckgo.com/) ou [Qwant](https://www.qwant.com/) ou [StartPage](https://www.startpage.com/)

{{</block>}}

{{< /tab >}}

{{< tab "iOS" >}}

{{<block info>}}

## Firefox Focus

* * *

Après l'avoir installé et ouvert, aller dans Paramètres, puis:

<u>Protection contre le pistage:</u>

-   Bloquer les traqueurs pub: <span class="vert">Cocher</span>
-   Bloquer les traqueurs stats: <span class="vert">Cocher</span>
-   Bloquer les traqueurs de réseaux sociaux: <span class="vert">Cocher</span>
-   Bloquer les autres traqueurs de contenu: <span class="vert">Cocher</span> Si problèmes de navigation, décocher.

<u>Envoyer des données d'utilisation:</u> <span class="vert">Décocher</span>

<u>Moteur de recherche:</u> [DuckDuckGo](https://duckduckgo.com/) ou [Qwant](https://www.qwant.com/) ou [StartPage](https://www.startpage.com/)

<u>Intégration Safari:</u> <span class="vert">Cocher</span>

{{</block>}}

{{<block info>}}

## Safari

* * *

Safari ne peut pas être supprimé. C'est l'unique navigateur par défaut. Il communique beaucoup trop d'informations personnelles avec Apple. Son utilisation régulière est déconseillée.

Dans Réglages iPhone > Safari

<u>Rechercher</u>

- Moteur de Recherche > <span class="vert">DuckDuckGo</span>
- Précharger le meilleur résultat > <span class="vert">Décocher</span>

<u>Général</u>
-  Bloquer les pop-up > <span class="vert">Cocher</span>
-  Bloqueurs de contenu > Firefox Focus > <span class="vert">Cocher</span>
-  Téléchargements > Supprimer la liste des téléchargements > <span class="vert">Après un jour</span> ou <span class="vert">un téléchargement réussi</span>

<u>Onglets</u>

- Fermer les onglets > Choisir (<span class="vert">Après un jour</span> est recommandé)

<u>Confidentialité et sécurité</u>

-  Bloquer tous les cookies > <span class="vert">Cocher</span>Alerte si Website frauduleux > <span class="vert">Cocher</span>
-  Recherche Apple Pay > <span class="vert">Décocher</span>
-  Effacer historique, données de site > <span class="vert">Effacer régulièrement</span>

<u>Réglages des sites Web</u>

-  Lecteur > Tous les sites Web > <span class="vert">Décocher</span>
-  Bloqueur de contenu > Tous les sites Web > <span class="vert">Cocher</span>
-  Appareil photo > <span class="vert">Demander ou Refuser</span>
-  Micro > <span class="vert">Demander ou Refuser</span>
-  Position > <span class="vert">Demander ou Refuser</span>

<u>Avancé</u>- Javascript > <span class="orange">Cocher</span> ou <span class="vert">Décocher</span> mais posera des problèmes sur beaucoup de sites.- Automatisation à distance > <span class="vert">Décocher</span>

<u>Experimental Features</u>:- Automatic HTTPS upgrade > <span class="vert">Cocher</span>- Disable 3rd-Party Cookies > <span class="vert">Cocher</span>

{{</block>}}

{{<block info>}}

## Firefox

* * *

Après l'avoir installé et ouvert, aller dans Paramètres, puis:

<u>Recherche</u>

-   Moteur de recherche par défaut > [DuckDuckGo](https://duckduckgo.com/) ou [Qwant](https://www.qwant.com/) ou [StartPage](https://www.startpage.com/)
-   Autres moteurs de recherche > <span class="vert">Décocher tout</span>

<u>Accueil:</u>

-   Tendances sur Pocket > <span class="vert">Décocher</span>

<u>Bloquer les fenêtres popup</u> > <span class="vert">Cocher</span>

<u>Identifiants et mots de passe</u> > <span class="vert">Tout décocher</span>

<u>Gestion des données</u>

-   Historique de navigation > <span class="vert">Décocher est recommandé</span>
-   Cache > <span class="vert">Décocher est recommandé</span>
-   Cookies > <span class="vert">Décocher est recommandé</span> mais peut poser des soucis de fontionnement pour certains sites.
-   Protection contre le pistage > <span class="vert">Cocher</span>
-   Effacer mes traces > <span class="vert">À faire régulièrement</span> si l'historique, le cache et les cookies sont cochés.

<u>Fermer les onglets privés</u> > <span class="vert">Cocher</span>

<u>Protection contre le pistage</u>

-   Protection renforcée > <span class="vert">Cocher</span>
-   Niveau de protection > <span class="vert">Strict</span>

<u>Envoyer des données d'utilisation</u> > <span class="vert">Décocher</span>

{{</block>}}

{{< /tab >}}

{{< /tabs >}}

{{< prev relref="/docs/outils/navigateurs/choix-navigateurs" >}} < Choix de navigateurs {{< /prev >}}
{{< next relref="/docs/outils/navigateurs/extensions" >}} Extensions > {{< /next >}}
