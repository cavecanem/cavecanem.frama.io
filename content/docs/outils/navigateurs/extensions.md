---
title: "Extensions"
weight: 4
bookToc: false
---

# Extensions

* * *

Les extensions permettent au navigateur d'augmenter considérablement sa protection. Elles doivent être installées en supplément.

Les extensions suivantes existent pour Firefox (mais aussi pour Brave et Iridium en version Chrome si besoin).

Il est inutile d'installer plusieurs extensions qui font la même chose (ralentissements).

{{< tabs "OS" >}}

{{< tab "Windows, macOS, Linux" >}}

{{<hint danger>}}

**Poubelle:** AdBlocker, AdBlock Plus, Ghostery, uBlock etc.

Ghostery récolte les données de navigation pour les revendre aux publicitaires. AdBlock Plus vend des passe-droit pour passer outre le bloqueur. Lourds et ralentissent l'ordinateur.
{{</hint>}}

{{<hint sain>}}

Configurer d'abord le navigateur correctement > [Configurations](configurations.html)

{{</hint>}}

### À installer ensuite:

{{<block info>}}

### 1. uBlock Origin | [getublock.com](https://getublock.com/)
* * *
(uBlock <u>Origin</u> ! Ne pas confondre avec uBlock qui est une saloperie.)

Bloqueur de publicités et de traqueurs _open source_. Léger et efficace, il ménage le processeur et la mémoire vive.

Ajouter puis > Autoriser en navigation privée

{{</block>}}

{{<block info>}}

### 2. Privacy Badger | [privacybadger.org](https://privacybadger.org/)
* * *
Apprend à bloquer les traqueurs invisibles, complète uBlock Origin.

Ajouter puis > Autoriser en navigation privée

{{</block>}}

{{<block info>}}

### 3. HTTPS Everywhere | [eff.org/https-everywhere](https://www.eff.org/https-everywhere)
* * *
Redirige toujours un site vers sa version sécurisée http<u>**s**</u> (lorsqu'elle existe) pour chiffrer les communications sur les sites (données bancaires, fiches client, mots de passe, etc..) entre l'appareil et le serveur.

Ajouter puis > Autoriser en navigation privée

{{</block>}}

{{<block info>}}

### 4. Decentraleyes | [decentraleyes.org](https://decentraleyes.org/)
* * *
Protège du pistage lié aux diffuseurs de contenus « gratuits », centralisés (Polices, Scripts, Icônes, ..).

Accélère de nombreuses requêtes (Google Hosted Libraries et autres) en les servant localement, allégeant la charge des sites. Complète les bloqueurs de contenus habituels.

Ajouter puis > Autoriser en navigation privée

{{</block>}}

Ces quatres extensions sont indispensables pour une bonne protection.

Si besoin de plus : [privacytools.io>addons](https://privacytools.dreads-unlock.fr/#addons)

{{< /tab >}}

{{< tab "Android" >}}

{{<hint danger>}}

**Poubelle:** AdBlocker, AdBlock Plus, Ghostery, uBlock etc.

{{</hint>}}

{{<hint sain>}}

Configurer d'abord le navigateur correctement > [Configurations](configurations.html)

{{</hint>}}

### À installer ensuite:

{{<block info>}}

### 1. uBlock Origin | [getublock.com](https://getublock.com/)
* * *
(uBlock <u>Origin</u> ! Ne pas confondre avec uBlock qui est une saloperie.)

Bloqueur de publicités et de traqueurs. Il ménage le processeur et la mémoire vive.

Ajouter puis > Autoriser en navigation privée

{{</block>}}

{{<block info>}}

### 2. Privacy Badger | [privacybadger.org](https://privacybadger.org/)
* * *
Apprend à bloquer les traqueurs invisibles, complète uBlock Origin.

Ajouter puis > Autoriser en navigation privée

{{</block>}}

{{<block info>}}

### 3. HTTPS Everywhere | [eff.org/https-everywhere](https://www.eff.org/https-everywhere)
* * *
Redirige toujours un site vers sa version sécurisée http<u>**s**</u> (lorsqu'elle existe) pour chiffrer les communications sur les sites (données bancaires, fiches client, mots de passe, etc..) entre l'appareil et le serveur.

Ajouter puis > Autoriser en navigation privée

{{</block>}}

{{<block info>}}

### 4. Decentraleyes | [decentraleyes.org](https://decentraleyes.org/)
* * *
Protège du pistage lié aux diffuseurs de contenus « gratuits », centralisés (Polices, Scripts, Icônes, ..).

Accélère de nombreuses requêtes (Google Hosted Libraries et autres) en les servant localement, allégeant la charge des sites. Complète les bloqueurs de contenus habituels.

Ajouter puis > Autoriser en navigation privée

{{</block>}}

Ces quatres extensions sont indispensables pour une bonne protection.

Si besoin de plus : [privacytools.io>addons](https://privacytools.dreads-unlock.fr/#addons)

{{< /tab >}}

{{< tab "iOS" >}}

Comme il est impossible d'ajouter des extensions avec iOS:

{{<block info>}}

1.  [Installer](choix-navigateurs.html) Onion, Firefox Focus et Firefox pour iOS.
2.  [Configurer](configurations.html) Firefox Focus, Safari et Firefox.
3.  [Configurer les DNS](/docs/outils/dns.html) avec l'app DNSCloak.

{{</block>}}

Puis:

{{<hint sain>}}

-   Naviguer le plus possible avec Firefox Focus ou en [navigation privée](navigation-privee.html) dans Firefox.
-   Firefox Focus bloque les traqueurs et cookies tiers; et également dans Safari > [Tutoriel](https://support.mozilla.org/fr/kb/firefox-focus)
-   Onion et Firefox Focus suppriment automatiquement l'historique le cache, et les cookies à la fermeture.
-   Si besoin de sécurité maximale et/ou d'anonymat, utiliser Onion (sans [VPN](/docs/outils/vpn.html)).

{{</hint>}}

{{< /tab >}}

{{< /tabs >}}

{{< prev relref="/docs/outils/navigateurs/configurations" >}} < Configurations {{< /prev >}}
{{< next relref="/docs/outils/navigateurs/fingerprint" >}} Fingerprint > {{< /next >}}
