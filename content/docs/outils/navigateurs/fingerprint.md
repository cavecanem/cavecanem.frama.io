---
title: "Fingerprint"
weight: 5
bookToc: false
---
# Fingerprint
***

_Browser fingerprinting_ (prise d'empreinte digitale du navigateur) est une méthode de suivi qui utilise les navigateurs Web.

Cette méthode analyse et suit les informations de configuration de l'appareil et du navigateur visibles par les sites Web plutôt que d'utiliser les méthodes de suivi traditionnelles (adresses IP ou cookies uniques).

Les prises d'empreintes digitales du navigateur sont à la fois difficiles à détecter et extrêmement difficiles à déjouer. Il est bon de savoir qu'elles existent et de comprendre leur fonctionnement si l'on souhaite les limiter.

## Fonctionnement
***

Lorsque l'on charge une page Web, on diffuse automatiquement certaines informations au site Web que l'on visite - ainsi qu'à tous les traqueurs intégrés au site (tels que ceux qui diffusent des publicités). Le site peut choisir d'analyser notre navigateur en utilisant JavaScript, Flash, HTML5 et d'autres méthodes.

Il peut rechercher la version du navigateur, les différentes polices installées, la langue qui a été définie, le fuseau horaire, la configuration du navigateur et les extensions installées, les dimensions de la fenêtre du navigateur, la résolution de l'écran, le modèle d'ordinateur, le système d'exploitation etc.

Le site peut alors créer un profil très précis et quasi unique lié aux caractéristiques associées du navigateur, plutôt qu'avec un cookie de suivi spécifique.

Si le navigateur est unique, il est alors possible pour les traqueurs en ligne (Google, Facebook, annonceurs, etc.) de nous identifier même sans installer de cookie de suivi. Même si le traqueur ne connaît pas notre nom, il pourra recueillir un dossier très personnel des sites Web qui sont visités et de ce qui est fait en ligne.

La suppression des cookies n'aidera pas à stopper le suivi, car ce sont les caractéristiques de la configuration du navigateur et de l'ordinateur qui sont analysées.

## Dangers
***

Les dangers sont évidents et assez vicieux.

Un des dangers le plus classique est de se croire protégé en ayant caché son adresse IP derrière un [VPN](/docs/outils/vpn.html) alors que les traqueurs ne suivent plus l'adresse IP mais l'empreinte digitale du navigateur. Si l'empreinte digitale du navigateur est trop unique et qu'un service, Google ou Facebook par exemple, est ouvert, il saura tout de suite à qui appartient cette empreinte digitale et donc qui est l'utilisateur. Que son VPN soit actif, ou pas.

## Parades
***

Les parades ont chacune leurs avantages et leurs inconvénients.

Les empreintes digitales des navigateurs étant une méthode assez puissante pour suivre les utilisateurs sur Internet, la seule stratégie de défense est de se fondre le plus possible dans la masse en ayant des configurations les plus communes au monde. Ces mesures peuvent être prises avec les navigateurs existants et des configuration très poussées permettant de faire mentir son navigateur. Le problème : elles sont très laborieuses à mettre en place pour la plupart des utilisateurs.

Dans la pratique, la protection la plus réaliste est l'utilisation du navigateur Tor, qui a déployé beaucoup d'efforts pour réduire l'empreinte digitale du navigateur.

Pour une utilisation quotidienne avec Firefox, et plutôt que d'essayer d'obtenir une empreinte digitale qui rentrerait dans le lot, la meilleure option est d'utiliser des [extensions _open source_ et libres](/docs/outils/navigateurs/extensions.html) comme Privacy Badger ou uBlock Origin qui bloqueront certains (mais malheureusement pas tous) des domaines qui tentent d'effectuer des empreintes digitales, et pour les plus téméraires, d'utiliser l'extension [NoScript](https://noscript.net/) qui réduit considérablement la quantité de données disponibles pour le _fingerprinting_.

### Utiliser le navigateur Tor

Le projet Tor a déployé des efforts considérables pour essayer de "standardiser" les différentes caractéristiques des navigateurs afin d'éviter qu'ils ne soient utilisés pour suivre les utilisateurs de Tor.

Tor comprend maintenant des correctifs pour empêcher les empreintes digitales des polices (en limitant les polices que les sites Web peuvent utiliser) et les empreintes digitales du _Canvas_ (en détectant les lectures des objets HTML5 Canvas et en demandant aux utilisateurs de les approuver).

Tor peut également être configuré pour bloquer agressivement le JavaScript, notamment avec [NoScript](https://noscript.net/).

Prises ensemble, ces mesures font du navigateur Tor une défense solide contre la prise d'empreintes digitales. Malheureusement, la navigation avec Tor est actuellement beaucoup plus lente que la navigation avec Firefox et peut toutefois ne pas être totalement infaillible face à une prise d'empreinte.

### Désactiver JavaScript

Sa désactivation est une défense puissante contre la prise d'empreintes digitales du navigateur. La détection passe souvent par JavaScript, le couper empêche les principales méthodes d'être utilisées. Malheureusement, JavaScript est nécessaire pour que de nombreux sites fonctionnent correctement.

Il existe au moins deux façons de bloquer l'utilisation de JavaScript par certains sites tout en permettant à d'autres de l'utiliser.

1. [NoScript](https://noscript.net/) est plus un outil pour les utilisateurs avertis qu'une solution pour tous : il bloquera JavaScript partout et permettra de réactiver manuellement uniquement les scripts souhaités pour le site en question. C'est beaucoup de travail, et cela nécessite de bonnes intuitions lorsqu'un site ne fonctionne pas parce que JavaScript est désactivé.
2. Les bloqueurs de publicités courants ont tendance à être assez bons pour bloquer les publicités et traqueurs pouvant utiliser JavaScript. Contrairement aux publicités, les scripts de suivi ou d'empreintes digitales sont généralement invisibles. Même si les utilisateurs activent des fonctionnalités qui mettent l'accent sur la protection de la vie privée, certains traqueurs peuvent encore passer inaperçus.

### Essayer d'utiliser un navigateur "non rare"

La façon la plus évidente d'essayer d'empêcher la prise d'empreintes digitales du navigateur est de choisir un navigateur "standard", "commun". Il s'avère que cela est étonnamment difficile à faire. Il semble que le candidat le plus probable serait la dernière version de Chrome (qui ne devrait pas être utilisé) fonctionnant sur une version moderne de Windows (qui ne devrait pas non plus être utilisé). Mais même ainsi, beaucoup de ces navigateurs Chrome sur Windows se distinguent les uns des autres par l'énorme gamme de versions de plugins et de polices de caractères qui peuvent être installées avec eux. Les premières générations de navigateurs pour smartphones étaient relativement difficiles à identifier, mais à mesure que ces appareils se sont diversifiés et ont pris en charge un plus grand nombre de fonctions, ils sont devenus très faciles à identifier.

### Éviter d'installer trop d'extensions

Plus le navigateur a d'extensions, plus il peut devenir unique. Outre le fait que le trop plein d'extensions ralentit et contient parfois des traqueurs, il est donc très important de ne pas le surcharger.

Si par nécessité, certaines extensions doivent être installées pour qu'un outil de travail ou autre fonctionne, veiller après utilisation à les désactiver dans les préférences du navigateur.

## Tester son unicité
***

[https://coveryourtracks.eff.org/](https://coveryourtracks.eff.org/) est un outil combinant plusieurs techniques de prise d'empreintes digitales de navigateur afin d'analyser l'unicité ou non de ses configurations.

{{< prev relref="/docs/outils/navigateurs/extensions" >}} < Extensions {{< /prev >}}
