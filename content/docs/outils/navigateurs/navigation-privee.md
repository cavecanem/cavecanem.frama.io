---
title: "Navigation privée"
weight: 1
bookToc: false
---
# Le mode Navigation Privée

* * *

Un mot semble ici important car ce mode est souvent mal compris ou même pas du tout utilisé.

## Fonction
***

La navigation privée (ou mode Incognito selon les navigateurs) bloque certaines extensions et traqueurs, vide tous les cookies, le cache et l'historique de navigation à la fermeture.

La navigation n'est pas anonyme, elle est juste mieux configurée pour respecter la confidentialité de l'utilisateur.

Elle a été mise en place pour palier à un navigateur mal configuré ou qui ne peut pas l'être (au bureau, Internet cafés, sur des ordinateur tiers, certains téléphones etc.).

Que le navigateur soit bien configuré ou non, la navigation privée est une barrière supplémentaire qu'il est recommandée d'adopter.

## Exemples d'utilisation
***

-   Pour effectuer des recherches (prix de billets d'avion par ex.) et ne pas voir le prix augmenter quelques heures ou jours plus tard (au fait, c'est quand on nettoie pas ses cookies).
-   Pour se connecter à certains sites (ex. Facebook, Google) pour qu'ils ne puissent pas utiliser de traqueurs inter-sites leur permettant de récupérer tout l'historique et le contenu des navigations effectuées (oui, ils font ça sinon).

{{<block info>}}

### Activation
***

Pour utiliser le mode de navigation privée dans son navigateur:

Menu > Nouvelle fenêtre de navigation privée

{{</block>}}

{{<block sain>}}

### Pratique saine
* * *

-  Utiliser le plus possible la navigation privée.
-  Utiliser toujours la navigation privée lors de recherches, de connexions à des réseaux sociaux, de sites d'achats en ligne, de connexion à des sites sensibles (boîtes mail, banque, impôts, etc.)

{{</block>}}

{{<hint danger>}}
**Attention:** Ce mode ne rend pas invisible ou anonyme.
{{</hint>}}

{{< prev relref="/docs/outils/navigateurs" >}} < Navigateurs {{< /prev >}}
{{< next relref="/docs/outils/navigateurs/choix-navigateurs" >}} Choix de navigateurs > {{< /next >}}
