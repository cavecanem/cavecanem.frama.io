---
title: "Systèmes d'exploitation"
bookCollapseSection: true
bookToc: false
weight: 200
description: Tous les appareils numériques, pour fonctionner, utilisent un système d'exploitation. Le respect de la confidentialité de leurs utilisateurs varie grandement entre chaque. Comment les configurer pour l'accroître ?
---
# Systèmes d'exploitations
***

Un système d'exploitation, _Operating System_ (OS) en anglais est un logiciel permettant de réaliser des actions sur un appareil.

Ils existent une multitude d'OS dont les plus connus sont Windows, macOS, Android, iOS.

Linux n'est pas un sytème d'exploitation en soit mais plus un regroupement d'OS dont Ubuntu, Fedora et Debian sont les plus connus.

{{< section >}}
