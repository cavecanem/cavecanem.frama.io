---
title: "Android"
bookToc: false
weight: 3
summary: Système d'exploitation à la base libre et _open source_ racheté par Google. Il est l'OS le plus utilisé au monde sur les appareils mobiles et impose l'utilisation de produits Google.
---

# Android
***

## Surveiller ses applications
***

Les applications mobiles étant la plupart remplies de mouchards, de traqueurs, et d'accès aux informations sensibles de l'appareil, il est important d'installer le moins possible d'applications et privilégier l'accès aux services par un [navigateur](/docs/outils/navigateurs/choix-navigateurs.html) bien configuré.

{{<hint danger>}}

Lorsqu'une app demande l'accès aux contacts, calendriers, localisation, c'est un potentiel danger vis à vis de sa vie privée. Il faut bien saisir l'importance que ce type d'accord implique.

{{</hint>}}

**Rappels :** Lire le chapitre [Applications mobiles](/docs/outils/applications.html).

{{<block info>}}

### Exodus Privacy | [exodus-privacy.eu.org](https://exodus-privacy.eu.org/fr/)
***

Analyse les problèmes de vie privée des applications Android. L'app proposée par le site peut également analyser celles présentes sur le téléphone.

{{</block>}}

## Installer un App Store alternatif
***

Voir le chapitre [Applications mobiles](/docs/outils/applications.html) à la section correspondante à Android.

## Éviter d'utiliser un VPN
***

Si ce n'est pas [nécessaire](vpn.html), ce qui est souvent le cas, il est plus efficace de configurer ses DNS.

À moins d'avoir un [bon fournisseur de VPN](vpn/fournisseurs.html), l'application du VPN peut contenir des traqueurs et risque d'utiliser le GPS, le Wi-Fi, ou la triangulation, ce qui n'est pas recommandée.

## Configurer ses DNS
***

Télécharger et configurer Nebulo  > voir [DNS](dns.html)

Une bonne blacklist permettra de bloquer les traqueurs, la télémétrie et les publicités des applications et du téléphone.

## Augmenter ses paramètres de confidentialité
***

1.  Désactiver la localisation des apps et utilitaires système <u>quand ce n'est pas strictement nécessaire</u>.
2.  Refuser ou désactiver l'accès aux Contacts, Calendriers, Rappels, Photos, Bluetooth, Micro, Reconnaissance vocale, Appareil Photo, Notes, Fichiers et dossiers, etc. <u>pour toutes les apps dont ce n'est pas strictement nécessaire</u>.
      -   Si besoin ultime d'accès Micro ou à l'Appareil Photo, l'activer sur demande et recouper aussitôt que c'est terminé. Ça limite la fuite des données (comme les publicités liées aux discussions du bar de la veille par exemple).
3.  Désactiver l'analyse des données du téléphone.
4.  Désactiver la publicité personnalisée
      -   Dans les paramètres des app quand c'est possible.
      -   Dans le système si c'est possible.
5.  Ne pas utiliser l'assistant vocal.
6.  Ne jamais utiliser la reconnaissance faciale et les empreintes digitales.
7.  Chiffrer ses données automatiquement en utilisant un code de verrouillage ([tutoriel](https://www.astuces-aide-informatique.info/669/chiffrer-un-telephone-android))
8.  Couper le Bluetooth et le Wi-Fi lorsque ce n'est pas <u>strictement nécessaire</u>. Des services les utilisent pour pister les utilisateurs (ex. à Londres dans des poubelles dans la rue [Source BBC](https://www.bbc.co.uk/news/technology-23665490) ).
9.  Activer le Wi-Fi et le Bluetooth lorsque c'est <u>strictement nécessaire</u>. Les couper juste après utilisation.
10.  Faire les mises à jour régulièrement pour les applications saines pour corriger les failles de sécurité découvertes.
11.  Éviter le cumul d'applications gratuites et futiles. Elles sont toutes remplies de traqueurs et aspirent les métadonnées.
12.  Désactiver la télémétrie autant que possible
      -   Dans les paramètres des app. Ne jamais partager les données avec les développeurs (ce n'est jamais collecté anonymement même s'ils le disent).
13.  Utiliser OpenStreetMAp à la place de Google Maps. C'est _open source_, ne collecte pas les données de localisations et fonctionne hors-ligne.
      -   [MAPS.ME](https://mapsme.fr/)
      -   [OsmAnd](https://osmand.net/)
14.  Installer Firefox, configurer et installer les extensions, voir [Navigateurs > Android](navigateurs.html).
15.  Utiliser une messagerie sécurisée comme [Signal](https://www.signal.org/fr/). <u>Arrêter d'écrire et d'appeler avec Facebook Messenger, Whatsapp, Instagram, Skype, Zoom etc.</u>
16.  Faire ses recherches avec
      -   [DuckDuckGo](https://duckduckgo.com/)
      -   [StartPage](https://www.startpage.com/)
      -   [Qwant](https://www.qwant.com/).
17.  N'utiliser aucune application de Google, Apple, Microsoft et Facebook. Si besoin d'utiliser leur service, passer par le navigateur autant que possible.
18.  Sinon faire des [Progressive Web App](https://invidious.snopyta.org/watch?v=heSvwQgEMLM) avec Firefox pour remplacer les applications de réseaux sociaux.
      -   Utilise le service via Firefox en créant une app sur l'écran d'accueil.
      -   Contourne l'obligation de partager l'accès à toutes les données du téléphone pour pouvoir l'utiliser.
      -   Firefox gèrera mieux le bloquage des traqueurs, des publicités, etc.
19.  Utiliser un navigateur brûleur pour éviter le hijack par Javascript. Si un lien de hameçonnage est ouvert par mégarde (message, email, etc.) il sera neutralisé. Évite aux pirates de récupérés des données ou des comptes.
      -   Installer Firefox Focus.
      -   Le choisir comme navigateur par défaut.
      -   Dans les préférences, bloquer tous les traqueurs, les polices Web, le javascript.
      -  Il s'ouvrira en premier lors d'un clic sur un lien, permettant de vérifier d'abord le contenu. Si c'est bon, copier le lien pour l'ouvrir dans Firefox.
20.  Être minimaliste. Plus il y a d'app sur le téléphone, plus l'appareil et l'utilisateur sont exposés. Faire le ménage, ne garder que l'essentiel.
21.  Chercher s'il existe une meilleure application pour faire la même chose que l'app actuelle.
      -   Une alternative [FOSS](/docs/start/code-source.html) sur l'app store [F-Droid](https://f-droid.org/).
      -   Qui respecte la vie privée: [GDPR Compliant](https://privacytrust.com/gdpr/privacy-by-design-gdpr.html) ou [Privacy by Design](https://fr.wikipedia.org/wiki/Protection_de_la_vie_priv%C3%A9e_d%C3%A8s_la_conception) par exemple.


## Quitter Android
***

Android est _open source_ mais impose aux utilisateur l'éco-système Google, ce qui le rend dangereux en terme de confidentialité.

Avec un téléphone autre qu'un iPhone, il est possible de changer d'OS pour ne plus utiliser Google Android. Des alternatives _open source_ existent mais dépendent également du modèle de téléphone pour être installées.  
[https://many.tuxphones.com](https://many.tuxphones.com/) répertorie en partie les OS utilisables sur différents modèles de téléphone.

**Attention :** Pour pouvoir installer un OS sur son téléphone, ce dernier doit être débloqué.

{{<block info>}}

### Graphene OS | [grapheneos.org](https://grapheneos.org)
***

GrapheneOS est le système d'exploitation le plus sûr pour sa confidentialité et sa vie privée, et le plus mis à jour actuellement.

-  Basé sur sur l'Android Open Source Project et débarrassé de tout Google.
-  Libre et _open source_, conçu pour la sécurité et la confidentialité des utilisateurs avancés et experts.
-  Offre actuellement une sécurité matérielle forte pour une sélection ciblée d'appareils.
-  Uniquement compatible avec certains appareils [listés ici](https://grapheneos.org/faq#device-support).

<span class="orange">Pour des raisons de sécurité, il est recommandé de ne pas utiliser les versions tierces de GapheneOS pour les appareils non pris en charge. Ce sont seulement des versions de tests pour développeurs.</span>

{{</block>}}

{{<block info>}}

### CalyxOS | [calyxos.org](https://calyxos.org)
***

Ce qui sépare GrapheneOS et CalyxOS :

1.  Il est plus rapide, plus facile et donc plus agréable à utiliser que GrapheneOS.  
2.  Lors de l'installation, il laisse le choix à l'utilisateur d'installer [microG](https://microg.org/), ce qui permet alors de pouvoir installer des applications Google (Google map etc.) si l'utilisateur le souhaite.
3.  Orienté grand public. Il n'y a donc pas besoin d'être un expert pour l'utiliser et être correctement protégé.

-  Basé sur sur l'Android Open Source Project.
-  Libre et _open source_, conçu pour la sécurité et la confidentialité de l'utilisateur grand public.
-  Android sans mouchards.
-  La sécurité et la confidentialité sont configurées par défaut en incluant entre autres déjà Tor, Signal, DuckDuckGo Privacy Browser, etc.
-  Mises à jour de sécurité régulières.
-  CalyxOS est conçu en tenant compte des besoins des défenseurs des droits de l'homme, des journalistes, des avocats et des groupes d'activistes politiques et sociaux.
-  Uniquement compatible avec certains appareils [listés ici](https://calyxos.org/get/).

{{</block>}}

{{<block info>}}

### Lineage OS — [lineageos.org](https://lineageos.org/)
***

Si le téléphone est [compatible](https://download.lineageos.org/).

-  Existe depuis 2016
-  Basé sur sur l'Android Open Source Project et régulièrement mis à jour.
-  Libre et _open source_ conçu pour avant tout pour la sécurité et la confidentialité de l'utilisateur.
-  Augmente la longévité des téléphones (car ils restent compatibles plus longtemps notamment).
-  Apps _open source_.

<span class="orange">Pour des raisons de sécurité, il est recommandé de ne pas utiliser les versions tierces de LineageOS pour les appareils non pris en charge.</span>

{{</block>}}

{{<block info>}}

### Ubuntu Touch | [ubuntu-touch.io](https://ubuntu-touch.io/fr_FR/)
***

-  Depuis 2011.
-  Respectueux de la vie privée.
-  Pas de portes dérobées.
-  Téléphones compatibles : [liste](https://devices.ubuntu-touch.io/).

{{</block>}}

{{< prev relref="/docs/outils/os" >}} < Systèmes d'exploitation {{< /prev >}}
