---
title: "iOS"
bookToc: false
weight: 4
summary: iOS, premier système d'exploitation sur _smartphone_ est développé par Apple qui en est propriétaire. Les dernières versions ne peuvent être utilisées que sur certains iPhone, également propriété d'Apple.
---

# iOS
***

## Surveiller ses applications
***

Les applications étant la plupart remplies de mouchards, de traqueurs, et d'accès aux informations sensibles de l'appareil, il est important d'installer le moins possible d'applications et privilégier l'accès aux services par un [navigateur](navigateurs/choix-navigateurs.html) bien configuré.

{{<hint danger>}}

Lorsqu'une app demande accès aux contacts, calendriers, localisation, c'est un potentiel danger vis à vis de sa vie privée. Il faut bien saisir l'importance que ce type d'accord implique.

{{</hint>}}

{{<block info>}}

### Exodus Privacy | [exodus-privacy.eu.org](https://exodus-privacy.eu.org/fr/)
***

Analyse les problèmes de vie privée des applications Android, mais la plupart étant également sur iOS, cela peut être instructif.

{{</block>}}

## Éviter d'utiliser un VPN
***

Si ce n'est pas nécessaire, ce qui est souvent le cas, il est plus efficace de configurer ses DNS.

À moins d'avoir un [bon fournisseur](vpn/fournisseurs.html), l'application du VPN peut contenir des traqueurs et risque d'utiliser le GPS, le Wi-Fi, ou la triangulation ce qui n'est pas recommandée.

## Configurer ses DNS
***

Télécharger et configurer [DNSCloak](https://apps.apple.com/us/app/dnscloak-secure-dns-client/id1452162351). > [DNS](dns.html)

Une bonne _blacklist_ permet de bloquer tous les traqueurs et les publicités des applications et du téléphone.

## Augmenter ses paramètres de confidentialité
***

1.  Désactiver la localisation des apps et utilitaires système <u>quand ce n'est pas strictement nécessaire</u>.
    -  Réglages > Confidentialité > Service de localisation > Régler pour chaque app et Services système
1.  Désactiver l'accès aux Contacts, Calendriers, Rappels, Photos, Bluetooth, Micro, Reconnaissance vocale, Appareil Photo, Santé, HomeKit, Médias et Apple Music, Recherche, Fichiers et dossiers, Mouvements et forme, <u>pour toutes les apps dont ce n'est pas strictement nécessaire</u>.
    -  Réglages > Confidentialité
    -  Si besoin ultime d'accès Micro ou Appareil Photo, l'activer sur demande et recouper aussitôt que c'est terminé. Ça limite la fuite de vos données (évite entre autre les publicités liées aux discussions au bar la veille par exemple).
1.  Désactiver la télémétrie autant que possible
    -  Réglages > Confidentialité > Analyse et amélioration: Tout décocher
    -  Dans les paramètres des app. Ne jamais partager les données avec les développeurs (ce n'est jamais collecté anonymement même s'ils le disent).
1.  Désactiver la publicité personnalisée dans les app (quand c'est possible) et également dans iOS
    -  Réglages > Confidentialité > Publicité > Suivi publicitaire limité: Cocher
    -  Réinitialiser l'identifiant de publicité régulièrement.
1.  Ne pas utiliser Siri.
    -  Réglages > Siri et recherche > Tout décocher
1.  Ne jamais utiliser la reconnaissance faciale et les empreintes digitales.
    -  Réglages > Touch ID et code > Utiliser Touch ID > Tout décocher
1.  Chiffrer vos données automatiquement en utilisant un code de verrouillage.
    -  Réglages > Touch ID et code > Activer le code
1.  Couper le Bluetooth et le Wi-Fi lorsque ce n'est pas <u>strictement nécessaire</u>. Des services les utilisent pour pister les utilisateurs (ex. à Londres dans des poubelles dans la rue [Source BBC](https://www.bbc.co.uk/news/technology-23665490) ).
    -  Réglages > Bluetooth > Décocher
    -  Réglages > Wi-Fi > Décocher
    -  Attention, à partir du centre de contrôle ils sont mis en pause et non éteints.
1. Activer le Wi-Fi et le Bluetooth lorsque c'est <u>strictement nécessaire</u>. Les couper juste après utilisation.
1.  Faire les mises à jour régulièrement pour les applications saines pour corriger les failles de sécurité découvertes.
1.  Éviter le cumul d'applications gratuites et futiles, elles sont toutes remplies de traqueurs et aspirent les métadonnées..
1.  Utiliser OpenStreetMAp à la place de Google Maps. C'est _open source_, ne collecte pas les données de localisations et fonctionne hors-ligne.
    -   [MAPS.ME](https://mapsme.fr/)
    -   [OsmAnd](https://osmand.net/)
1.  Installer des navigateurs pour contrer Safari > [Navigateurs > iOS](navigateurs/choix-navigateurs.html).
1.  Utiliser une messagerie sécurisée comme [Signal](https://www.signal.org/fr/). <u>Arrêter d'écrire et d'appeler avec Facebook Messenger, Whatsapp, Instagram, etc.</u>
1.  Faire ses recherches avec
    -   [DuckDuckGo](https://duckduckgo.com/)
    -   [StartPage](https://www.startpage.com/)
    -   [Qwant](https://www.qwant.com/)
1.  N'utiliser aucune application de Google, Apple, Microsoft et Facebook. Si besoin de leur service, passer par le navigateur autant que possible.
1.  Sinon faire des [Progressive Web App](https://invidious.snopyta.org/watch?v=KY0ixCLegvs) avec Safari pour remplacer les applications de réseaux sociaux:
    -   Utilise le service via le navigateur en créant une app sur l'écran d'accueil.
    -   Contourne l'obligation de partager l'accès à toutes les données du téléphone pour pouvoir l'utiliser.
    -   Vu qu'il n'est pas possible de créer des PWA avec autre chose que Safari, le mieux reste d'utiliser un marque page dans un [navigateur recommandé](navigateurs-choix.html) et de consulter le service dans le navigateur.
1.  Si les PWA ne sont pas utilisées, utiliser Safari comme un brûleur pour éviter le hijack par Javascript.. Si un lien de hameçonnage est ouvert par mégarde (message, email, etc.) il sera neutralisé. Évite aux pirates de récupérés des données ou des comptes.
    -   [Configurer Safari](navigateurs-configurations.html) et bloquer au max.
    -   Et dans Réglages > Safari > Avancé > Javascript > Décocher
1.  Être minimaliste. Plus il y a d'app sur le téléphone, plus l'appareil et l'utilisateur sont exposés. Faire le ménage, ne garder que l'essentiel.
1.  Chercher s'il existe une meilleure application pour faire la même chose que l'app actuelle.
    -   Une alternative [FOSS](/docs/start/code-source.html) compatible avec iOS.
    -   Qui respecte la vie privée: [GDPR Compliant](https://privacytrust.com/gdpr/privacy-by-design-gdpr.html) ou [Privacy by Design](https://fr.wikipedia.org/wiki/Protection_de_la_vie_priv%C3%A9e_d%C3%A8s_la_conception) par exemple.

{{< prev relref="/docs/outils/os" >}} < Systèmes d'exploitation {{< /prev >}}
