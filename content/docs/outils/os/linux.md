---
title: "GNU/Linux"
bookToc: false
weight: 5
summary: Linux est le noyau des systèmes d'exploitation Linux. Il est open source et fonctionne au travers de multiples distributions.
---

# GNU/Linux
***

GNU/Linux, régulièrement appelé Linux, est une variante du système d'exploitation GNU fonctionnant avec le noyau Linux. Il est _open source_ et fonctionne au travers de multiples distributions et logiciels libres et _open source_.

## Les environnements de bureau
---

Avant de choisir quelle distribution Linux utiliser, il est bon de connaître un peu les environnements de bureau disponibles. Ce sont eux qui offrent l'interface visuelle au système d'exploitation.

Outre le graphisme qui change un peu, c'est surtout l'utilisation et le "ressenti" qui diffère. Certaines fonctionnalités, paramètres, applications de base et options graphiques divergent selon les éditions.

Ils sont généralement proposés au moment de télécharger une distribution Linux. Certaines ne proposent qu'un seul environnement, d'autres plusieurs et dans ce cas il sera possible d'en changer ultérieurement sans tout devoir réinstaller.

Il existe beaucoup d'environnements de bureau mais les plus connus et utilisés sont GNOME, XFCE, KDE Plasma, LXDE ou encore Cinnamon.

### KDE Plasma
---
KDE Plasma est puissant, léger et très rapide. Il est ultra personnalisable et sera adéquat pour les créatifs et ceux qui souhaitent pouvoir tout gérer et tout modifier grâce aux multiples options disponibles.

### XFCE
---
XFCE se veut également ultra rapide et très peu gourmand sur les ressources système. Il est souvent mis en avant pour sa rapidité et sa fluidité. C'est un environnement bien pensé, qui offre un peu moins de personnalisation pour ceux qui ne souhaitent pas se perdre dans toutes les fonctionnalités de Plasma et tout changer en profondeur.

### LXDE
---
LXDE est encore plus léger que XFCE et permet d'utiliser une distribution Linux sur des ordinateurs très peu puissants.

### Cinnamon
---
Cinnamon est le bureau par défaut de Linux Mint, c'est une interface très proche de celle de Windows.
Personnalisable, il sera adéquat aux personnes utilisant leur ordinateur pour des tâches simples et efficientes.

### GNOME
---
GNOME, l'environnement par défaut d'Ubuntu, est d'une utilisation moins intuitive et plus gourmande en ressources que ceux ci-dessus. Ce n'est pas le plus recommandé pour débuter sur Linux.

## Distributions mères
---

Il faut savoir que Arch Linux, Debian, Gentoo, Red Hat, Slackware et Android sont les principales distributions mères.

De celles-ci découlent des centaines de variantes avec des options supprimées ou ajoutées afin d'être orientées en fonction de besoins spécifiques (grand public, développeurs, administration serveur, _smart TV_, _smartphone_, tablettes, etc.).

Pour avoir une idée de toutes les branches existantes, [voir ici](https://fr.wikipedia.org/wiki/Distribution_Linux#/media/Fichier:Linux_Distribution_Timeline.svg).

## Distributions recommandées pour débuter
***

{{<hint danger>}}

**Éviter :** Ubuntu

{{</hint>}}

{{<block info>}}

### Fedora Workstation | [getfedora.org](https://getfedora.org/)
***

Fedora Workstation est une distribution développée par le projet Fedora et basée sur Red Hat.

C'est une édition sécurisée, fiable et conviviale, développée pour les ordinateurs de bureau et portables. Elle utilise GNOME comme environnement de bureau par défaut mais il est possible de le choisir avec un autre environnement de bureau sur [spins.fedoraproject.org](https://spins.fedoraproject.org/).

{{</block>}}

{{<block info>}}

### Linux Mint | [linuxmint.com](https://linuxmint.com/)
***

Linux Mint est basée sur Ubuntu. C'est une branche qui est plus respectueuse de la vie privée que sa distribution mère. Son environnement de bureau par défaut est Cinnamon.

Linux Mint développe et propose également LMDE (Linux Mint Debian Edition), une version basée directement sur Debian afin d'éviter toute dépendance à Ubuntu à l'avenir.

Linux Mint Cinnamon permet de démarrer très simplement avec l'environnement Linux en étant débutant et venant de l'environnement Windows.

{{</block>}}

{{<block info>}}

### Manjaro | [manjaro.org](https://manjaro.org/)
***

Manjaro est basée sur Arch Linux. C'est une distribution beaucoup plus accessible que sa distribution mère. Elle est en _rolling release_, ce qui veut dire qu'elle se met à jour régulièrement sans jamais changer la version du système. Vu qu'il n'y pas de nouvelle version, il n'y aura pas d'installation à faire pour installer de nouvelles fonctionnalités, il suffit de faire les mises à jour.

C'est également une distribution respectueuse de la vie privée qui propose un environnement de bureau très simple à utiliser pour les débutants Linux.

{{</block>}}

{{<block info>}}

### PureOS | [pureos.net](https://pureos.net/)
***

Créé par les membres de la communauté [Purism](https://puri.sm/) qui fabrique téléphones et ordinateurs libres, sécurisés et confidentiels.

Cet distribution Linux est basée sur Debian et utilise uniquement GNOME. Simple à utiliser, elle est conçue en priorité pour protéger la confidentialité de l'utilisateur.

Elle n'est pas compatible avec des composants propriétaires pour éviter tout risque à ses utilisateurs. De fait, elle peut ne pas être compatible avec tous les ordinateurs si des _drivers_ sont propriétaires.

{{</block>}}

## Distributions recommandées pour utilisateurs avertis
***

{{<block info>}}

### Debian | [debian.org](https://www.debian.org/)
***

Debian est une distribution mère non commerciale. C'est un système très qualitatif, stable et sérieux issu d'une longue maturité. Il est entièrement composé de logiciels libres et _open source_, dont la plupart sont sous licence publique générale GNU.

L'ergonomie est devenue plus conviviale et permet un peu mieux aux non experts de l'utiliser. Ce n'est cependant pas une distribution pour commencer aisément sur Linux.

{{</block>}}

{{<block info>}}

### Qubes OS | [qubes-os.org](https://www.qubes-os.org/)
---
Qubes OS est un système qui permet de séparer ses activités quotidienne (travail, personnel, non sécurisé, etc.) dans des machines virtuelles.

Chaque machine virtuelle peut utiliser un OS différent (Fedora, Debian, Windows), en fonction des applications que l'on souhaite ou utiliser.

Avec un seul OS, il est donc permis de compartimenter directement et de manière sécurisée les différents aspects de sa vie personnelle.

Cet OS est très sécurisé mais demande une machine puissante.

Présentation et installation en français > [Playlist](https://invidious.snopyta.org/playlist?list=PLHj4WuEG4h_oh1y7EKsiYhjxT-Lmwtb_R)

{{</block>}}

## Applications
---
Concernant les applications différentes possibilités facilitent les installations sur Linux. Celles-ci permettent d'installer et de gérer des logiciels sans avoir à se soucier de lignes de commandes, des dépendances ou de la distribution Linux utilisée.

{{<hint danger>}}

**Éviter :** Snapcraft.

{{</hint>}}

{{<block info>}}

### AppImage | [appimage.org](https://appimage.org/)
---
AppImage est un des meilleurs moyens d'installer et de gérer ses applications sur Linux. Puissant, rapide, optimisé.

{{</block>}}

{{<block info>}}

### Flatpak | [flatpak.org](https://www.flatpak.org/)
---
Flatpak fait aussi bonne figure et est installé par défaut dans de nombreuses distributions (Fedora par exemple). Il permet de gérer correctement des applications sans passer par un magasin centralisé.

{{</block>}}

{{<block info>}}

### FlatHub | [flathub.org](https://flathub.org)
---
Flathub nécessite d'avoir Flatpak installé pour fonctionner.

C'est une centralisation d'applications disponibles en Flatpak. Cela permet de trouver plus facilement les fichiers d'installations.

{{</block>}}

{{<block info>}}

### Wine | [winehq.org](https://www.winehq.org/)
---

Wine (à l'origine un acronyme pour « Wine Is Not an Emulator » est une couche de compatibilité capable d'exécuter des applications Windows sur divers systèmes d'exploitation conformes à POSIX comme Linux, macOS et BSD.

Plutôt que de simuler Windows, Wine traduit les appels de l'API Windows en appels POSIX, éliminant la baisse de performance qu'engendre une machine virtuelle ou un émulateur. Il permet d'intégrer proprement les applications Windows à son bureau.

<span class="orange">Attention : toutes les applications et jeux windows ne fonctionneront pas forcément.</span>

{{</block>}}

{{<block info>}}

### PlayOnLinux | [playonlinux.com](https://www.playonlinux.com/fr)
---

PlayOnLinux se base sur le logiciel Wine, et profite de toutes ses possibilités, tout en évitant à l'utilisateur d'appréhender sa complexité et en exploitant certaines de ses fonctions avancées.

PlayOnLinux est un logiciel vous permettant d'installer et d'utiliser facilement de nombreux jeux et logiciels prévus pour fonctionner exclusivement sous Windows® de Microsoft®.

En effet, à l'heure actuelle, encore peu de jeux vidéo sont compatibles avec GNU/Linux, ce qui peut être un frein à l'adoption de ce système. PlayOnLinux apporte une solution accessible et efficace à ce problème, gratuitement et dans le respect du logiciel libre.

<span class="orange">Attention : toutes les applications et jeux windows ne fonctionneront pas forcément.</span>

{{</block>}}

{{< prev relref="/docs/outils/os" >}} < Systèmes d'exploitation {{< /prev >}}
