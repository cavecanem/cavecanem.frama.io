---
title: "macOS"
bookToc: false
weight: 2
summary: Un peu meilleur quant à la protection de la vie privée et largement moins utilisé que Windows, il ne peut être installé et utilisé que sur certains ordinateurs propriétaires d'Apple.
---

# macOS
***

macOS a beau être plutôt sécurisé et plus transparent que Windows sur la protection et la vie privée de ses utilisateurs, il n'en reste pas moins qu'il est un produit d'Apple, qui elle-même est une société américaine soumise à la coopération avec son gouvernement.

De plus, Apple a engagé depuis plusieurs années une politique visant à faire acheter des ordinateurs et téléphones toujours plus récents à ses consommateurs sans leur laisser la possibilité de changer les pièces, soit pour réparer, soit pour augmenter les performances. Ils délaissent ainsi complètement les acheteurs, qui, sans une machine récente, finissent par ne plus recevoir les correctifs et mises à jour de sécurité de leur système et de leurs applications. Apple crée volontairement des appareils voués à une obsolescence programmée.

{{<block sain>}}

En plus de tous les autres chapitres à faire, des précautions et des configurations supplémentaires sont à mettre en place.

{{</block>}}

## Installer un pare-feu
***

Permet de contrôler quelles applications peuvent envoyer ou recevoir des données via Internet.

Un pare-feu est intégré à macOS (Préférences système > Sécurité et confidentialité > Pare-feu) mais celui-ci n'est pas activé par défaut et ne gère pas les connexions sortantes.

{{<block info>}}

### Lulu | [objective-see.com](https://objective-see.com/products/lulu.html)
***

Pare-feu ultra léger et _open source_. Télécharger, installer et ouvrir.

-   Accepter les connexions pour les services Apple (ce n'est pas recommandé mais simplifie la tâche).
-   Demander pour tout le reste.
-   Une fois en marche, choisir d'accepter ou de refuser à chaque fois que c'est demandé selon si le programme a réellement besoin de communiquer avec l'extérieur.

Incertain ?

-  Refuser temporairement. Si ça bloque l'utilisation, relancer l'application en question et autoriser. Sinon à la prochaine ouverture refuser définitivement.
-  [Rechercher](moteurs-de-recherche.html) sur Internet afin de savoir à quoi ça sert.

{{</block>}}

## Augmenter ses paramètres de confidentialité
***

1.  Ouvrir **Préférences système > Sécurité et confidentialité > Confidentialité**
    -   Désactiver tous les accès de localisation, contacts, calendriers, micro, photos, caméra des applications et utilitaires système quand ce n'est pas <u>strictement nécessaire</u>.
    -   Dans **Usage et Diagnostiques** (ou analytics), décocher: Partage de diagnostiques avec Apple, Partage avec les développeurs, Partage avec iCloud
    -   Dans les dernières versions, dans **Publicité**, cocher: Limiter le traçage publicitaire, cliquer sur: Réinitialiser l'identifiant de publicité.
2.  Faire les mises à jour régulièrement pour corriger les failles de sécurité.
3.  Ne pas utiliser Siri.
4.  Mettre un autocollant opaque sur la caméra intégrée. L'enlever si besoin puis le remettre.
5.  Ne jamais utiliser la reconnaissance faciale et les empreintes digitales.
6.  Éviter le cumul d'applications.
7.  Désactiver la télémétrie autant que possible: ne jamais partager les données avec les développeurs, ce n'est jamais collecté anonymement même s'ils le disent.
8.  Utiliser [OpenStreetMAp](https://www.openstreetmap.org/) à la place de Google Maps. _open source_ et ne collecte pas les données de localisations entre autres.
9.  Ne jamais partager l'accès avec les contacts, les calendriers, les albums photo, l'accès au micro et à la caméra etc. avec tous les services qui ne sont pas de confiance: Facebook, Amazon, Whatsapp, Instagram, Messenger, les applications et logiciels qui ne le nécessitent pas. Si besoin ultime, l'activer sur demande et recouper aussitôt que c'est terminé.
10.  Utiliser [Signal](https://www.signal.org/fr/) pour les messages instantanés. Arrêter d'écrire avec Messenger, Whatsapp, etc.
11.  Utiliser [Linphone](https://www.linphone.org/) ou [Jisti](https://meet.jit.si/) pour la visio-conférence.
11.  Ne pas utiliser Microsoft Office mais [LibreOffice](https://fr.libreoffice.org/) ou [ONLYOFFICE](https://www.onlyoffice.com/fr/download-desktop.aspx).
12.  N'utiliser aucune application de Google.
13.  Chercher toujours s'il existe une alternative _open source_ <u>et</u> qui respecte la vie privée pour faire la même chose qu'une application ou un service propriétaire.  
[prism-break.org](https://prism-break.org/en/) ou [alternativeto.net](https://alternativeto.net/) (Filtres > macOS + OpenSource)

## Permettre l'installation d'applications hors App Store
***

Il existe beaucoup plus de programmes pour macOS que ceux présents dans l'App Store d'Apple. Pour pouvoir installer des applications externes à l'App Store, il faut les télécharger sur le site officiel de l'application.

![Message de Gatekeeper](/img/os/Gatekeeper_1.png "Message de Gatekeeper")

Certaines applications ne voudront tout de même pas s'installées si Apple ne les a pas signées. Ceci ne veut pas forcément dire qu'elles sont corrompues ou dangereuses, simplement que les développeurs n'ont pas payé une certification auprès d'Apple.

Il revient donc à l'utilisateur de savoir d'où l'application provient afin de comprendre les risques potentiels liés à l'installation. Si cela vient d'un téléchargement illégal par exemple, l'utilisateur est maître des risques qu'il encourt. Par contre, quand il s'agit de programmes sûrs, mais non-signés, la protection de macOS doit pouvoir être désactivée pour laisser l'utilisateur installer ce qu'il souhaite.

{{<block info>}}

### Pour désactiver partiellement Gatekeeper :
***

1.  Ouvrir Préférences Système > Sécurité et confidentialité > Général.
1.  Cliquer sur le cadenas en bas à gauche et entrer le mot de passe administrateur.
1.  Choisir "App Store et Développeurs identifiés".
1.  Ouvrir le programme d'installation.
1.  Une fenêtre s'ouvre indiquant que le programme ne peut pas être installé > OK
1.  Ouvrir Préférences Système > Sécurité et confidentialité > Général.
1.  Un bouton "Ouvrir quand même" est apparu > Cliquer dessus.
1.  Le programme d'installation s'ouvre.

![Paramètres de Gatekeeper](/img/os/Gatekeeper_2.png "Paramètres de Gatekeeper")


### Pour désactiver complètement Gatekeeper :
***

Pour les utilisateurs avancés ou tout simplement ceux qui en ont marre de ces messages, il est possible de désactiver Gatekeeper.

1.  Ouvrir Applications > Utilitaires > Terminal.
1.  Taper `sudo spctl --master-disable` > Entrée.
1.  Taper le mot de passe administrateur (c'est invisible mais ça écrit) > Entrée.
1.  Ouvrir Préférences Système > Sécurité et confidentialité > Général.
1.  Cliquer sur le cadenas en bas à gauche et entrer le mot de passe administrateur.
1.  Choisir "N'importe où".

### Pour remettre Gatekeeper :
***

1.  Ouvrir Applications > Utilitaires > Terminal.
1.  Taper `sudo spctl --master-enable`
1.  Taper le mot de passe administrateur (c'est invisible mais ça écrit) > Entrée.
1.  Ouvrir Préférences Système > Sécurité et confidentialité > Général.
1.  Cliquer sur le cadenas en bas à gauche et entrer le mot de passe administrateur.
1.  Choisir "App Store" ou "App Store et Développeurs identifiés".

{{</block>}}

## Quitter Apple et macOS
***

Même si macOS fait bien mieux que Windows10 en terme de confidentialité, il est recommandé d'utiliser un système libre et _open source_ vraiment respectueux de la vie privée de ses utilisateurs et des appareils qu'ils ont achetés. Une multitude de distributions Linux sont actuellement sur le marché, aussi bien pour les débutants que pour les initiés. Elles s'installent très bien sur n'importe quel PC ou Mac. Voir le chapitre [GNU/Linux](/docs/outils/os/linux.html).

{{< prev relref="/docs/outils/os" >}} < Systèmes d'exploitation {{< /prev >}}
