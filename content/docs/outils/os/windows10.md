---
title: "Windows 10"
bookToc: false
weight: 1
summary: Windows 10, le plus récent du système d'exploitation le plus populaire du monde. Propriété de Microsoft, cet OS est un cauchemar pour protéger sa vie privée et son intimité.
---

# Windows 10
***

Bon, Microsoft Windows...

Depuis Windows Vista la confidentialité de l'utilisateur est devenue difficile à maintenir privée chez Microsoft. Avec Windows 10, c'est même devenu un cauchemar pour garder sa vie privée.

{{<hint danger>}}

Même après avoir modifié les paramètres de confidentialité proposés, Windows 10 communique des informations concernant l'utilisateur 5 500 fois par jour avec Microsoft. Source: [thehackernews.com](https://thehackernews.com/2016/02/microsoft-windows10-privacy.html) 10/02/16

Microsoft détient aujourd'hui plus d'informations sur vous que les services de renseignement de votre pays.

{{</hint>}}

## Ce que Microsoft récupère tous les jours
***

**La synchronisation des données est activée par défaut**

-  L'historique de navigation et les sites Web ouverts.
-  Les paramètres des applications.
-  Les noms et mots de passe des points d'accès WiFi.

**Votre appareil est étiqueté avec un identifiant publicitaire unique**

-  Utilisé pour vous montrer des publicités personnalisées provenant de publicitaires tiers et de réseaux de publicités.

**Cortana peut collecter toutes vos données**

-  Les touches que vous tapez au clavier, les recherches et ce que votre microphone reçoit.
-  Les données de votre calendrier.
-  La musique que vous écoutez.
-  Vos données bancaires.
-  Vos achats.

**Microsoft peut collecter n'importe quelle donnée personnelle**
-  Votre identité.
-  Vos mots de passe.
-  Vos informations démographiques.
-  Vos intérêts et habitudes.
-  Vos données d'utilisation.
-  Vos contacts et relations.
-  Vos données de localisation.
-  Les contenus comme les e-mails, les messages instantanés, la liste des personnes vous appelant, les enregistrements audio et vidéo.

**Vos données peuvent être partagées**

-  Lorsque vous installez Windows 10, vous autorisez Microsoft à partager n'importe quelle information citée ci-dessus avec un tiers, avec ou sans votre consentement.

Cette vidéo de 2015 avant la commercialisation de Windows 10 est éclairante sur la façon dont Windows a été conçu : [Windows 10 - Un aspirateur de metadonnées](https://invidious.snopyta.org/watch?v=kBJn82TU63c) <span class="gris">12min 19s</span>

## Colmater les fuites
***

Des logiciels ont été créés pour empêcher Windows de communiquer les informations personnelles avec Microsoft.

Mais multiplier les tutoriels vidéos peut être nécessaire pour bien comprendre ce qu'il faut cocher ou non en fonction de son utilisation.

(ShutUp10 et Privacy10 sont assez similaires, il n'est pas nécessaire de cumuler ces deux programmes)

{{<block info>}}

### Privacy10 | [w10privacy.de](https://www.w10privacy.de/english-home/)
***

– Tutoriel : [Windows 10 - Colmater les fuites de données personnelles](https://invidious.snopyta.org/watch?v=I4yv2qXdN3U) <span class="gris">26min 38s</span>  
– Tutoriel : [Windows 10 privacy](https://invidious.snopyta.org/watch?v=rM3HpehVSvU) <span class="gris">11min 25s</span>

{{</block>}}

{{<block info>}}

### ShutUp10 | [oo-software.com](https://www.oo-software.com/fr/shutup10)
***

– Tutoriel : [ShutUp10 - Faites taire l'espionnage de Windows 10](https://invidious.snopyta.org/watch?v=IQxdsChT2MU) <span class="gris">8min 16s</span>

{{</block>}}

{{<block info>}}

### Gérer le cache
***

– Tutoriel: [Que "cache" Windows ?](https://invidious.snopyta.org/watch?v=OjsqZmTPn8I) <span class="gris">19min 54s</span>

{{</block>}}

Même avec les configurations des chapitres précédents et si une grande partie des fuites repérées seront colmatées, il est quand même recommandé de ne pas utiliser Windows si vous tenez à votre vie privée. Être anonyme avec Windows n'est même pas envisageable.

## À noter
***

La force de Microsoft est de s'être imposé au monde grâce à des partenariats avec les constructeurs d'ordinateur qui le pré-installe et le font payer à l'achat de la machine. Le problème est qu'ils ne laissent pas le choix à l'acheteur de le refuser ou d'utiliser autre chose.

La plupart des utilisateurs ne font pas plus sur Windows qu'ils ne feraient avec un autre système d'exploitation Linux. Il est donc vivement recommandé de passer sur une distribution Linux. Ce n'est pas plus compliqué, c'est bien plus puissant et sécurisé.

Il est vrai qu'avec le monopole créé par Microsoft, pour les jeux vidéos ou pour les professionnels qui utilisent des logiciels propriétaires c'est difficile de passer sur Linux car il n'y a pas encore de bonnes alternatives libres à ces programmes. Il est donc conseillé de partitionner ses activités : son travail sur Windows, et tout ce qui est personnel et d'ordre privé sur Linux.

Il est aussi bon de savoir qu'aujourd'hui, certaines applications et jeux vidéos peuvent être installés sur Linux en utilisant [Wine](https://www.winehq.org/) ou [PlayOnLinux](https://www.playonlinux.com/fr).

## Quitter Microsoft et Windows
***

Même si macOS fait bien mieux que Windows en terme de confidentialité, il est recommandé d'utiliser un système libre et _open source_ **vraiment** respectueux de la vie privée de ses utilisateurs. Une multitude de distributions Linux sont actuellement sur le marché, aussi bien pour les débutants que pour les initiés. Voir le chapitre [GNU/Linux](/docs/outils/os/linux.html).

{{< prev relref="/docs/outils/os" >}} < Systèmes d'exploitation {{< /prev >}}
