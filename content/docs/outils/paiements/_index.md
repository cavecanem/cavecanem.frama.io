---
title: "Paiements en ligne"
bookToc: false
weight: 2
description: Comment payer de manière sécurisée et/ou confidentielle en ligne ?
---

# Paiements en ligne
***



{{<block sain>}}

### Pratique Saine
***

-  Ne jamais utiliser son vrai numéro de carte bancaire.
-  Utiliser une carte bleue numérique.
-  Privilégier les paiements en cash, à réception ou en crypto-monnaie.

{{</block>}}

## Comptant
***

Payer comptant (_cash_) est une des façons les plus sûres et confidentielles qui soit.

Quand on peut payer comptant, le faire.

Avantages du paiement comptant :

-  N'indique pas la date et l'heure d'achat.
-  N'indique pas le magasin.
-  N'indique donc pas votre position géographique à cette date et heure.
-  N'indique pas le montant réglé.

Attention, la date et l'heure, le montant, et l'emplacement du retrait seront enregistrés. Le mieux est de retirer une grosse somme une fois auprès de sa banque et la stocker chez soi sans faire de retraits réguliers.

Dans la vie courante, c'est une bonne attitude pour payer ses frais courants ou avant de partir en voyage et ne pas laisser de traces quand on paie quelque chose par exemple.

Dans l'univers numérique, peu, mais quelques services en ligne permettent d'être réglés en cash (le VPN [Mullvad](https://mullvad.net/fr/) par exemple).


## Cartes cadeaux
***

Les cartes cadeaux permettent aussi des achats sans traces. Un grand nombre peuvent s'acheter comptant en magasins. Les sites acceptants le paiement par carte cadeau sont beaucoup plus nombreux que ceux acceptant le paiement comptant.

## e-Carte Bleue
***

Il est de plus en plus courant de pouvoir utiliser des e-Carte Bleue pour les achats en ligne. Celles-ci permettent plusieurs choses :

-  Mettre un montant fixe sur la carte.
-  Choisir la durée de validité de la carte.
-  Le numéro de carte peut changer à chaque achat.
-  Payer sur Internet sans donner son véritable numéro de carte.

La plupart des banques françaises proposent e-Carte Bleue de Visa. Se renseigner auprès de sa banque.

**Attention :** ce type de paiement sécurise sa carte principale du piratage mais ne rend pas les transactions anonymes ou confidentielles.

## Crypto-monnaie
***

Sécurisée et confidentielle, la crypto-monnaie permet de faire des transactions sur Internet sans que notre banque ou le service ne puisse connaître, nos achats, nos montants, notre identité.

Peu de sites de vente en ligne permettent de payer en crypto-monnaie mais ceux qui le permettent sont à privilégier.

## Services de paiement en ligne
***

Ces entreprises permettent de payer sans passer par sa carte bancaire et de procéder à des transferts d'argent entre personnes. Ils ajoutent de la sécurité pour les utilisateurs au détriment de leurs confidentialité.

Paypal, l'un des plus connu n'est pas recommandé pour la confidentialité de ses données personnelles.

> "Une modification du règlement PayPal sur le respect de la vie privée entre en vigueur le 18 octobre 2013. Un paragraphe de ce changement des conditions générales d'utilisation porte sur la « divulgation d'informations à des tiers autres que des utilisateurs PayPal » et stipule que la société travaille en collaboration avec des partenaires pour lesquels des données peuvent être partagées, comme des organismes de référence de crédit et de lutte contre la fraude, mais aussi des entreprises privées comme Facebook. Des entreprises de reciblage publicitaire comme Criteo et Mediaplex sont également mentionnées."
>
> Source : [wikipedia](https://fr.wikipedia.org/wiki/PayPal#Donn%C3%A9es_personnelles)

## Le marché de l'occasion
***

Lors de l'achat d'un ordinateur ou d'un téléphone, il peut être demandé au client de fournir sa carte d'identité ou de payer en carte bancaire. Le numéro d'identification de l'appareil risque d'être lié à l'identité de l'acheteur.

Si possible, ne pas donner son identité lors de l'émission de la facture d'un achat en magasin et payer en cash.

Privilégier le matériel de seconde main. Il permet de ne pas avoir le numéro d'identifiant de l'appareil lié à sa véritable identité.
