---
title: "Partage de fichiers"
bookToc: false
bookCollapseSection: true
weight: 80
description: Le partage de fichiers numériques est une action très commune pour chacun d'entre nous mais les outils utilisés ne permettent tous pas d'assurer un transfert confidentiel.
---

# Partage de fichiers
***

Partager des fichiers en ligne est le lot commun de la plupart des utilisateurs.

Ici encore, les politiques des services concernant la confidentialité et le respect de la vie privée des utilisateurs, peuvent varier considérablement:

-   Obligation d'ouvrir des comptes utilisateurs.
-   Récolte des adresses IP et durée indéfinie de leur rétention.
-   Récolte des adresses email.
-   Non-chiffrement des fichiers.
-   Récolte et utilisation des fichiers (mots-clés, métadonnées, etc.).
-   Revente des données et métadonnées des utilisateurs à des tiers.
-   Stockage des fichiers sur une longue durée sans possibilité de suppression manuelle.


{{<block sain>}}

### Pratique saine
***

-   Choisir les bons services pour partager ses fichiers.
-   Si les fichiers sont sensibles, les [chiffrer manuellement](/docs/outils/chiffrer.html) avant de les envoyer.
-  Même avec les applications recommandées ci-dessous; si il y a un risque, chiffrer.

{{</block>}}

{{< section >}}
