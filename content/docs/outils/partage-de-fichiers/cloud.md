---
title: "Cloud"
bookToc: false
weight: 4
summary: Stocker ses fichiers en ligne pour y accéder n'importe où, les partager ou travailler à plusieurs via un bureau virtuel.
---

# Stockage Cloud et bureau virtuel

* * *

Stocker ses fichiers en ligne pour y accéder n'importe où, les partager ou travailler à plusieurs via un bureau virtuel.

Les services cloud ne devraient pas nécessiter l'accès à nos données afin de pouvoir les partager avec sa famille, ses amis ou ses collègues.

Si un service classique pour le travail collaboratif n'indique pas clairement que le serveur n'a pas accès aux informations, il est très probable que les données soient utilisées pour faire du profit.

{{<hint danger>}}
**Poubelle:** iCloud, Google Drive, OneDrive, etc.
{{</hint>}}

{{<block info>}}

### Nextcloud | [nextcloud.com](https://nextcloud.com/fr_FR/signup/)
***

-  Inscription gratuite en ligne ou installation sur son propre serveur.
-  Synchronisation des dossiers, contacts, calendriers, notes, emails, fichiers textes, tableurs, présentations, etc.
-  Possibilité de travailler avec d'autres utilisateurs.
-  Windows, macOS, Linux, Android, iOS.

{{</block>}}

{{<block info>}}

### Cryptpad | [cryptpad.fr](https://cryptpad.fr/)
***

-  Gratuit sans inscription (1GB)
-  Payant (5GB à 50GB).
-  Bureau complet et stockage en ligne via le navigateur.
-  Pas d'applications mobile.
-  Pas de synchronisation des contacts, calendriers, notes d'un téléphone ou tablette.

{{</block>}}

{{<block info>}}

### Seafile | [seafile.com](https://www.seafile.com/)
***

-  Inscription en ligne ou installation sur son propre serveur.
-  <span class="orange">Compte et version gratuits limités.</span>
-  Windows, macOS, Linux, Android, iOS.

{{</block>}}

{{<block info>}}

### tresorit | [tresorit.com](https://tresorit.com)
***

-  Cloud ultra sécurisé.
-  Synchronisation et envois de fichier chiffrés de bout en bout.
-  Serveurs suisses.
-  <span class="orange">Prix conséquents</span>.

{{</block>}}

{{<block info>}}

### Keybase
***

<span class="rouge">7 mai 2020: Acheté par Zoom. N'est plus une solution durable.</span>  
Source: [keybase.io/blog/keybase-joins-zoom](https://keybase.io/blog/keybase-joins-zoom)

{{</block>}}

{{< prev relref="/docs/outils/partage-de-fichiers" >}} < Partage de fichiers {{< /prev >}}
