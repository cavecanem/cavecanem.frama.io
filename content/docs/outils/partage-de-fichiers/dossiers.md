---
title: "Dossiers"
bookToc: false
weight: 3
summary: Partager des dossiers de son ordinateur avec d'autres utilisateurs.
---

# Partage de dossiers

* * *

Partager des dossiers de son ordinateur avec d'autres utilisateurs.

Les services ci-dessous fonctionnent comme Dropbox mais <u>sans serveur central entre les contacts</u>. Le partage se fait directement entre les utilisateurs, en [pair à pair](https://fr.wikipedia.org/wiki/Pair_%C3%A0_pair) (P2P) ou, si configuré, depuis un serveur personnel.

{{<hint danger>}}

**Poubelle:** Dropbox, pCloud

**Attention:** Ces services ne sont pas chiffrés de bout en bout, mais uniquement en transit. Il est bon de chiffrer ses fichiers [manuellement](/docs/outils/chiffrer.html).

{{</hint>}}

{{<block info>}}

### Syncthing | [syncthing.net](https://syncthing.net/)

* * *

-  Windows, macOS, Linux
-  Chiffrement en transit TLS

– Tutoriel: [Synchroniser vos fichiers](https://invidious.snopyta.org/watch?v=zXhkFNtg6hw) <span class="gris">24min 05s</span>

{{</block>}}

{{< prev relref="/docs/outils/partage-de-fichiers" >}} < Partage de fichiers {{< /prev >}}
