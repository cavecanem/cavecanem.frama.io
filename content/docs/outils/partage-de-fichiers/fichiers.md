---
title: "Fichiers"
bookToc: false
weight: 1
summary: Envoyer rapidement un fichier quelconque.
---

# Envoi rapide de fichiers

* * *

Envoyer rapidement un fichier quelconque.

{{<hint danger>}}**Poubelle:** Wetransfer, Wesendit{{</hint>}}

{{<block info>}}

### Lufi |  [demo.lufi.io](https://demo.lufi.io/about)

***

-  Programme _open source_.
-  Stocke n'importe quelle taille pendant 24h et max. 45 jours.
-  Possibilité de supprimer le fichier au premier téléchargement.
-  Possibilité de protéger le téléchargement par un mot de passe.
-  Chiffrement de bout en bout.

**Serveurs :**
-  [framadrop.org](https://framadrop.org/lufi/) | Lyon, France
-  [ethibox.fr](https://lufi.ethibox.fr/) | Nancy, France
-  [disroot.org](https://upload.disroot.org/) | Hollande
-  [sans-nuage.fr](https://drop.sans-nuage.fr/) | Alsace, France
-  [infini.fr](https://drop.infini.fr/) | Bretagne, France
{{</block>}}

{{<block info>}}

### Firefox Send | [send.firefox.com](https://send.firefox.com/)

***

-   Limite: 1GB sans inscription, 2,5GB avec inscription.
-   Stocke pendant 5min. et max. 7 jours.
-   Protection possible par mot de passe.
-   Chiffrement de bout en bout.

<span class="rouge">Le serveur principal de Firefox Send a été fermé par Mozilla à la suite d'abus du service</span>

**Serveurs :**
-  [drop.chapril.org](https://drop.chapril.org/) | Paris, France

{{</block>}}

{{<block info>}}

### Jirafeau | [gitlab.com](https://gitlab.com/mojo42/Jirafeau)

***

Jirafeau est un moyen simple d'envoyer des fichiers depuis son serveur.

-  Suppression automatique définit par une date ou après le premier téléchargement.
-  Possibilité de protéger le téléchargement par un mot de passe.
-  Envoi de gros fichiers.
-  N'utilise pas de base de données.
-  Nécessite PHP 5.6 minimum.

Ce projet _open source_ s'installe très facilement sur son propre serveur Apache ou nginx. Il suffit d'y copier les fichiers [de la dernière version](https://gitlab.com/mojo42/Jirafeau/-/tags) dans un dossier et de s'y connecter en ajoutant /install.php afin de lancer la procédure d'installation. Le logiciel de partage est mis en route et utilisable en 5 minutes.

{{</block>}}

Alternatives moins grand public:
-  [OnionShare](https://onionshare.org/), partage via le réseau Tor
-  [Magic-Wormhole](https://magic-wormhole.readthedocs.io/en/latest/), CLI
-  [croc](https://github.com/schollz/croc), CLI, chiffrement de bout en bout

{{< prev relref="/docs/outils/partage-de-fichiers" >}} < Partage de fichiers {{< /prev >}}
{{< next relref="/docs/outils/partage-de-fichiers/textes" >}} Partage de textes > {{< /next >}}
