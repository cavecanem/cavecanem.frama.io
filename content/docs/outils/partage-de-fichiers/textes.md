---
title: "Textes"
bookToc: false
weight: 2
summary: Envoyer un texte, un lien ou un mot de passe en toute sécurité.
---
# Envoi rapide de textes

* * *

Envoyer un texte, un lien ou un mot de passe en toute sécurité.

{{<block info>}}

### PrivateBin | [privatebin.net](https://privatebin.net/)

* * *

-   Effacement après lecture ou, au choix, après 5, 10, 60min. ou 24h.
-   Serveur Zero-Knowledge (aucune connaissance de ce qu'il se passe).
-   Chiffrement de bout en bout.
-   Protection possible par mot de passe.
-   Possibilité de créer un fil de discussion.

{{</block>}}

{{<block info>}}

### Framabin | [framabin.org](https://framabin.org/p/)
***

-  PrivateBin installé sur les serveurs de [Framasoft](https://framasoft.org/fr/)

{{</block>}}

{{<block info>}}

### Signal | [signal.org](https://www.signal.org/fr/)
***

-  Signal peut permettre l'envoi sécurisé de message.
-  Définir dans les paramètres de la conversation en cours (toucher le nom du contact), un temps de suppression pour le message avant de l'envoyer. quand le contact l'aura lu, il s'effacera après ce temps chez les deux utilisateurs.

{{</block>}}

{{< prev relref="/docs/outils/partage-de-fichiers/fichiers" >}} < Partage de fichiers {{< /prev >}}
