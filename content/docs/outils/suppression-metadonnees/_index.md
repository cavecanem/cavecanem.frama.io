---
title: "Métadonnées"
bookToc: false
weight: 8
description: Il est primordial pour sa confidentialité de supprimer les métadonnées contenues dans ses fichiers images, vidéos, PDF, avant de les partager via Internet.
---
# Métadonnées
***

Comme évoqué dans les premiers chapitres du site, [les métadonnées](/docs/start/metadonnees.html) peuvent offrir une quantité importante d'informations qu'il faut supprimer avant de faire un partage en ligne.

Une photo prise par son téléphone inclut, entre autres, les métadonnées suivantes :

-  L'heure et la date de la prise de vue
-  Le type d'image (jpeg, gif, png, ...)
-  Le poids de l'image en Mo ou Mb
-  La dimension de l'image en pixels
-  La marque et le modèle de l'appareil
-  La version du système d'exploitation utilisé (Android, iOS etc.)
-  La marque et le type d'objectif utilisé
-  L'ouverture, la vitesse d'obturation, les ISO de la prise de vue
-  Si le flash (et quel type de flash) a été utilisé
-  L'auteur de l'image ou le numéro de série de l'appareil (dans certains cas)
-  La géolocalisation de la prise de vue (si le GPS est autorisé à l'écrire)
-  Le nom des personnes présentes sur l'image (si cette fonction est utilisée)

![meta-photo](/img/metadonnees/metadonnees-photo.png "Exemple de métadonnées contenues dans une photo d'iPhone 6")

Si l'on donne un dossier rempli d'images sur une clé USB à sa grand-mère ce n'est pas très gênant. Par contre, si l'on envoie ces mêmes images sur un réseau social ou via un site de partage de type WeTransfer, chaque métadonnées de chacune des images pourront être utilisées par le service et dresser un profil monnayable de l'utilisateur.

## Suppression de métadonnées
***

Il est très important de supprimer les métadonnées des images, des vidéos et des documents avant de les envoyer sur Internet car les services en ligne analysent, stockent et revendent toutes les métadonnées.

Il est donc dangereux pour sa confidentialité d'envoyer toutes ces informations à Facebook (Facebook, Messenger, Instagram, WhatsApp), Google (Google Drive, Gmail), Apple (iCloud), Microsoft (Drive, Outlook) Twitter, Snapchat, WeTransfer, DropBox etc.

{{< hint danger >}}

**Très important :**
-  Ne jamais envoyer sans [chiffrement de bout en bout](/docs/start/chiffrement/de-bout-en-bout.html), ni partager sur les réseaux, des photos contenant le numéro de son passeport, de sa carte d'identité, de sa CB.
-  Toujours masquer la plaque d'immatriculation de sa voiture, son adresse, ou son emplacement, et toute autre informations sensibles de ce genre.
-  Ne jamais partager le visage de personne non-consentante sur les réseaux ou sur le Cloud. C'est mettre ces personnes en danger, leurs visages étant analysés, fichés et stockés par les services.
-  Cacher un visage (emojis, traits, etc.) en passant par Instagram ne protège pas la personne. La modification ne se fait pas sur le téléphone mais depuis le serveur. Lors de la retouche, l'image est déjà sur les serveurs d'Instagram et donc probablement analysée et stockée par Facebook avant toute modification.

{{</hint>}}

{{<block info>}}

### ExifCleaner | [exifcleaner.com](https://exifcleaner.com/)
***
Ce micro logiciel _open source_ est très simple léger et très puissant. Supprime toutes les métadonnées d'identification des images, des vidéos et des PDF pour les partager en toute confidentialité.

Glisser-déposer le fichier dans la fenêtre et les métadonnées sont instantanément supprimées.

Fonctionne sur Windows, macOS et Linux.

{{</block>}}

{{<block info>}}

### Signal | [signal.org](https://signal.org)
***
Signal, la [messagerie instantanée sécurisée](/docs/outils/im/centralisees.html), supprime automatiquement une grande partie des métadonnées lors d'envoi d'images dans l'application. Il suffit donc de s'envoyer son image puis d'en enregistrer la copie pour les voir supprimées.

Il est aussi possible depuis juin 2020 de flouer les visages et autres informations sensibles directement sur l'appareil et avant qu'elle n'atteigne le serveur.

1. \> Écrire un nouveau message > Chercher le contact "Note à mon intention" ou "Note to myself" > Envoyer l'image.
2. \> Ouvrir l'image du message et l'enregistrer.
3. Dans la bibliothèque d'image, la nouvelle version n'aura plus de métadonnées.

{{</block>}}

{{<block info>}}

### Image Scrubber | [everestpipkin.github.io/image-scrubber](https://everestpipkin.github.io/image-scrubber/)
***

Cette page Web _open source_ permet d'utiliser un script dans le navigateur qui supprime les métadonnées des images. Cette page n'enregistre aucune information sur serveur, les images ne sont envoyées nulle part.

Particulièrement utile sur les téléphones en créant une PWA (voir [Applications](/docs/outils/applications.html)).

Il permet également de peindre ou flouer les visages, plaques d'immatriculation et autres éléments qui ne doivent pas figurer sur les services en ligne et les réseaux sociaux.

{{</block>}}


## Analyse de ses activités sur Google
***

Si vous êtes client Google, vous pouvez vous faire peur en regardant l'analyse de vos données sur [myactivity.google.com](https://myactivity.google.com/). Ce service Google recense une partie de la collecte des données de votre compte. Il est possible de les supprimer via l'interface, cependant, rien ne vous prouvera que Google les aura effectivement supprimées définitivement de ses serveurs..
