---
title: "Vidéos en ligne"
bookToc: false
bookCollapseSection: true
weight: 90
description: Comment regarder ou mettre en ligne des vidéos en toute confidentialité sur Internet ?
---
# Vidéos en ligne
***

Partager ou regarder des films ou vidéos en ligne restreint aussi le respect de sa vie privée.

Les analyses et traqueurs contenus par les services comme Youtube, Dailymotion, Netflix et même Facebook, Instagram ou SnapChat, sont de véritables pompes à métadonnées qui servent à pister et identifier les faits et gestes des utilisateurs.

-  Sans compte : les contenus qu'ils regardent.
-  Avec un compte : les contenus visionnés, ceux enregistrés en favoris ainsi que les utilisateurs suivis.

Ils peuvent donc connaître précisément des informations sur notre personnalité : nos préférences et habitudes, nos idéaux, notre niveau intellectuel, notre orientation politique, sexuelle, religieuse, etc.

Concrètement, les services savent ce que nous regardons et des algorithmes prédictifs recommandent de nouvelles vidéos du genre pour nous faire rester plus longtemps sur le service, identifier nos intérêts avec une extrême précision et vendre plus de publicité toujours plus ciblée.

Le plus gros danger réside encore dans la fameuse "bulle filtrante". Ces opérations proposent des contenus que d'autres personnes, considéré par l'algorithme comme nos égaux, ont regardés. Ils soumettent ainsi aux utilisateurs des contenus que nous ne choisissons pas, mais vus par la majorité des personnes qui nous ressemblent, ce qui est une assomption qui méprise nos différences. Ce type de pratiques utilisées par tous les réseaux sociaux mène aux nombreuses dérives actuelles telles que la polarisation politique, les théories du complot, les _fake news_, etc.

Certains contenus proposés présentent des vidéos publicitaires déguisées (promotion de produits susceptibles de nous intéresser) ou des vidéos faites par des créateurs en vogue, élus par la plateforme elle-même...

Être soumis à ces algorithmes nous pousse à faire des choix orientés par une AI et donc à être les sujets d'une manipulation par les contenus regardés.

{{< next relref="/docs/outils/videos/visionner-videos" >}} Visionner des vidéos > {{< /next >}}
