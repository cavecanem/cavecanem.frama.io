---
title: "Diffuser des vidéos"
bookToc: false
weight: 3
description: Comment diffuser des vidéos en ligne de manière confidentielle ?
---
# Diffuser des vidéos
***

{{<hint danger>}}

**Éviter :** Youtube, Dailymotion, Facebook, Instagram, Vimeo

{{</hint>}}

## Avant de diffuser ses vidéos
***

Lorsque l'on met en ligne une vidéo, il est recommandé de supprimer ses métadonnées avant de les envoyer en ligne (voir > [Suppression de métadonnées](/docs/outils/suppression-metadonnees.html)).

Pour diffuser du contenu il n'existe pas énormément de solutions sûres à ce jour. Même si Vimeo fait bien mieux que Youtube en matière de respect de ses utilisateurs, il n'en reste pas moins un service payant et propriétaire.

{{<block info>}}

### PeerTube | [joinpeertube.org](https://joinpeertube.org/fr_FR/)
***

Créé en 2015 et soutenu par [Framasoft](https://fr.wikipedia.org/wiki/Framasoft), la première version stable voit le jour en octobre 2018. Peertube est l'alternative décentralisée, fédérée, libre et open-source de Youtube. Chacun peut héberger une instance PeerTube afin que ses vidéos restent sur son propre serveur.

Plusieurs [instances décentralisées](https://joinpeertube.org/instances#instances-list) permettent d'héberger et/ou de regarder des vidéos en ligne.

<iframe width="560" height="315" sandbox="allow-same-origin allow-scripts allow-popups" src="https://framatube.org/videos/embed/9c9de5e8-0a1e-484a-b099-e80766180a6d?subtitle=fr" frameborder="0" allowfullscreen></iframe>

{{</block>}}

{{< prev relref="/docs/outils/videos/visionner-films" >}} < Visionner des films {{< /prev >}}
