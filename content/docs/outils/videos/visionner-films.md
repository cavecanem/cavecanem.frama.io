---
title: "Visionner des films"
bookToc: false
weight: 2
description: Comment visionner des films en ligne de manière confidentielle ?
---
## Visionner des films
***

{{<hint danger>}}

**Éviter :** Netflix, Amazon Prime Video, Google Play, iTunes, MyCanal, Mubi etc.

{{</hint>}}

{{<block info>}}

### Le Cinema Club | [lecinemaclub.com](https://www.lecinemaclub.com/)
***

Le Cinema Club propose un film à visionner gratuitement en ligne chaque semaine sans inscription.

{{</block>}}

{{<block info>}}

### Arte | [arte.tv](https://www.arte.tv/fr/videos/cinema/)
***

Une sélection de films modifiée régulièrement. À visionner gratuitement et sans inscription, sur le site de la chaîne Arte.

{{</block>}}

{{<block info>}}

### Arte VOD |
***

Films à louer
-  Paiement par CB, Paypal, Carte cadeau.

{{</block>}}

{{<block info>}}

### Médiathèque numérique | [vod.mediatheque-numerique.com](https://vod.mediatheque-numerique.com)
***
Des milliers de films, de documentaires, de séries et de programmes jeunesse issus du meilleur d’ARTE et de la collection d’UniversCiné.

-  Nécessite d'être inscrit à la médiathèque de sa ville pour en faire la demande.
-  Visionnage illimité pendant 48h.
-  Le nombre de films par semaine est limité.

Quelques informations sont collectées, mais rien n'est vendus à des tiers (voir les CGU : https://vod.mediatheque-numerique.com/page/cgu#l2d).

{{</block>}}

{{<block info>}}

### La cinetek | [lacinetek.com](https://www.lacinetek.com/)
***

Site payant pour chaque film afin de les visionner.

Les CGU montrent une utilisation des données personnelles auxquelles on peut s'opposer voir article 7.1.e des CGU : [lacinetek.com/a-propos](https://www.lacinetek.com/a-propos)

{{</block>}}

{{<block info>}}

### Vdrome | [vdrome.org](http://www.vdrome.org/)
***

Vdrome propose deux fois par mois environ le film d'un artiste à visionner gratuitement en ligne sans inscription.

{{<hint warning>}}Attention : Vdrome utilise Vimeo{{</hint>}}

{{</block>}}

{{< prev relref="/docs/outils/videos/visionner-videos" >}} < Visionner des vidéos {{< /prev >}}
{{< next relref="/docs/outils/videos/diffusion-videos" >}} Diffuser des vidéos > {{< /next >}}
