---
title: "Visionner des vidéos"
bookToc: false
weight: 1
description: Comment visionner des vidéos en ligne de manière confidentielle ?
---

# Visionner des vidéos
***

{{<hint danger>}}

**Éviter :** Youtube, Dailymotion, Vimeo

{{</hint>}}

Si l'on doit absolument passer par Youtube :
-  Utiliser la navigation privée.
-  Utiliser un [navigateur sain](/docs/outils/navigateurs/choix-navigateurs.html) et [configuré](/docs/outils/navigateurs/configurations.html) avec les [extensions recommandées](/docs/outils/navigateurs/extensions.html).
-  Désactiver la lecture automatique des vidéos suggérées.
-  Ne jamais cliquer sur une vidéo recommandée par l'algorithme.
-  Toujours choisir ce que l'on regarde et ne jamais laisser Youtube choisir à notre place.

## Alternative à Youtube
***

{{<block info>}}

### PeerTube | [joinpeertube.org](https://joinpeertube.org/instances#instances-list)
***

Trouver une instance (chaîne) PeerTube en fonction de ses envies et chercher du contenu.

-  Fonctionne en pair à pair (P2P).
-  Aucun traqueur.
-  Respecte la confidentialité de ses utilisateurs.
-  _Open source_
-  Développé par [Framasoft](https://framasoft.org/).

{{</block>}}

## Pour visionner les vidéos de Youtube
***

{{<block info>}}

### Invidious | [invidio.us](https://invidio.us)
***

-  Regarder et partager les vidéos de Youtube de manière plus sécurisé.
-  Sans divulgation de son adresse IP, sans traqueurs, sans publicités, sans distractions et suggestions algorithmiques.
-  Possibilité de créer un compte sans adresse email pour s'abonner aux chaînes ou enregistrer les vidéos dans des _playlists_ sans passer par un compte Youtube.
-  Informations d'utilisateur non divulguées à des tiers.
-  Possibilité de supprimer l'historique de visionnage de son compte en un clic.
-  Toutes les données sont supprimées à la suppression du compte.
-  Peut être auto-hébergé ou utiliser via diverses instances. Liste : [https://instances.invidio.us](https://instances.invidio.us).

<span class="orange">**Information : le développeur d'Invidious a décidé d'arrêter définitivement l'instance principale à partir du 1er septembre 2020 puis le maintient de l'API a partir du 1er octobre 2020. Source: [omar.yt](https://omar.yt/posts/stepping-away-from-open-source), 01/08/20.**</span>

{{</block>}}

{{<block info>}}

### NewPipe | [F-Droid](https://f-droid.org/en/packages/org.schabi.newpipe/)
***
-  Android uniquement

Application disponible sur [F-Droid](https://f-droid.org/fr/). Permet de regarder des vidéos Youtube de manière plus sécurisée sur son téléphone et sa tablette Android.

{{</block>}}

{{<block info>}}

### Freetube | [freetubeapp.io](https://freetubeapp.io/)
***

-  Application pour Windows, macOS et Linux.
-  Permet de lire les vidéos Youtube sur son ordinateur.
-  Permet de s'abonner aux chaînes sans compte Google.
-  Conçu dans le respect de la vie privée.
-  Repose sur le code d'Invidious et développe son propre code depuis la fermeture d'Invidious, ce qui offre la même confidentialité.  
Il utilise Youtube sans les publicités, sans divulgation d'IP, sans traqueurs.
-  En version Beta, cela comporte parfois certains bugs mais l'application est prometteuse.

{{</block>}}

{{< prev relref="/docs/outils/videos" >}} < Vidéos en ligne {{< /prev >}}
{{< next relref="/docs/outils/videos/visionner-films" >}} Visionner des films > {{< /next >}}
