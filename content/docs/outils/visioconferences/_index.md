---
title: "Visioconférences"
bookToc: false
bookCollapseSection: true
weight: 75
description: Pour communiquer en vidéo avec ses contacts professionnels ou amicaux, sans fuites de données personnelles, et de manière sécurisée.
---

# Visioconférences
---

Pour sécuriser ses communications il est essentiel qu'elles soient chiffrées de bout en bout et que les clés de chiffrements soient stockées sur l'appareil de l'utilisateur, ce qui n'est pas le cas pour Zoom par exemple, qui garde les clés sur leurs serveurs.

Il est aussi utile de ne pas avoir de compte sur les plateformes afin de pouvoir jouir pleinement de l'utilisation de la visioconférence, sans être tracé, avoir ses informations et conversations personnelles utilisées ou même revendues.

Pour rappel, le logiciel [Optic Nerve](https://fr.wikipedia.org/wiki/Optic_Nerve_%28programme_de_surveillance%29), développé par la NSA, a été utilisé pour enregistrer les _webcams_ sur Yahoo Messenger. Sur une période de six mois en 2008, le programme a ainsi collecté et enregistré dans les bases de données de l'agence les images de 1,8 million d'utilisateurs. Source: [The Guardian](https://www.theguardian.com/world/2014/feb/27/gchq-nsa-webcam-images-internet-yahoo).

Inutile de faire confiance à Google, Facebook et consorts sur le fait qu'il n'aient pas de pratiques similaires, que ce soit pour un gouvernement ou pour leur propre compte.

{{<hint danger>}}

**Poubelle:** Skype, Zoom, WhatsApp, Facebook Messenger, Google Meet, Microsoft Teams, etc.

{{</hint>}}

{{<block info>}}

### Jitsi | [jitsi.org](https://jitsi.org)
---

Jitsi est un logiciel _open source_ permettant la visioconférence sans installation de logiciel supplémentaire ni création de compte.

L'utilisateur crée une salle de réunion sur l'instance principale [https://meet.jit.si/](https://meet.jit.si/). Il envoie ensuite le lien à ses contacts pour s'y retrouver au moment voulu, directement depuis le navigateur de son ordinateur.

Sur mobile ou tablette il est par contre nécessaire d'utiliser l'application dédiée.

-  Chiffrement de bout en bout.
-  Gratuit jusqu'à 50 participants.
-  Flux vidéo HD.
-  Affichage optimal de l’orateur actif et des participants.
-  Fonction de chat.
-  Partage d’écran simultané et contrôle à distance possible.
-  Main levée.
-  Micro / Muet.
-  Verrouillage de la salle de réunion avec un mot de passe.
-  Peut être aussi être installer sur son propre serveur VPS ou Linux.

Il est également possible d'utiliser une quantité d'autre instance, si le serveur principal n'est pas adequat. Une liste est disponible sur le site de Framasoft : [framatalk.org](https://framatalk.org/accueil/fr/info/).

{{</block>}}

{{<block info>}}

### Linphone | [linphone.org](https://www.linphone.org)
---

Linphone est une application _open source_ permettant la visioconférence sécurisée et confidentielle.

Linphone nécessite la création d'un compte pour fonctionner ainsi que l'installation du logiciel ou de l'application mobile. Elle est du coup très proche de l'utilisation de Skype ou Zoom et en fait une parfaite alternative.

-  Liste des contacts et historique d'appels.
-  Chiffrement de bout en bout uniquement pour les appels 1 à 1 et pour les messages de groupe.
-  Video HD.
-  Notifications _Push_.
-  Partage d'image et de fichiers.
-  Messagerie instantanée.

{{</block>}}

{{<block info>}}

### Signal | [signal.org](https://www.signal.org)
---
La [messagerie instantanée](/docs/outils/im/centralisees.html) et sécurisée Signal permet (en plus des messages appels et visioconférence en chiffrement de bout en bout actuels) la visioconférence, également avec le chiffrement de bout en bout, pour les groupes nouvellement créés depuis la dernière mise à jour (14/12/20). [Voir article](https://signal.org/blog/group-calls/).

-  Jusqu'à 5 participants pour l'instant, et plus à venir prochainement.

{{</block>}}
