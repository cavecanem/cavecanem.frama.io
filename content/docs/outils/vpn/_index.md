---
title: "VPN"
bookToc: false
bookCollapseSection: true
weight: 6
description: Un VPN permet de contourner les informations qui transitent par son fournisseur d'accès à Internet et sur les sites ou services utilisés. Il permet également de contourner les censures étatiques.
---

# Les VPN
***

En anglais: _Virtual Private Network_ (Réseau Virtuel Privé).

Un réseau VPN agit comme un tunnel.  
En passant par ce tunnel, l'adresse IP, tout le trafic réseau et les requêtes DNS de l'appareil sont cachées au FAI (Fournisseur d'Accès Internet). De ce fait votre connexion ment désormais aux services utilisées vous permettant d'être caché sous une autre forme d'identité.

C'est un système utilisé au départ par les entreprises pour sécuriser les connexions à distance des employés aux serveurs de l'entreprise. Aujourd'hui ce système est largement proposé aux particuliers pour sécuriser leurs utilisations sur Internet.

Il y a 5 raisons d'utiliser un VPN et une quantité de mauvaises quand le fonctionnement n'est pas bien compris.

## Pourquoi utiliser un VPN ?
***

1.  Pour accéder à des contenus étrangers lorsque le pays l'interdit.
2.  Pour cacher son trafic Internet à son FAI.
3.  Pour cacher son adresse IP à un site ou un service.
4.  Pour cacher à son FAI des téléchargements de type bittorrent quand la loi du pays le prohibe (Loi Hadopi par ex.).
5.  Pour éviter d'être espionné ou piraté sur un réseau WI-FI Public (bar, hôtels, aéroports, etc..).

Ce sont les seules raisons d'utiliser un VPN.

## Un VPN ne rend pas anonyme
***

Un VPN ne permet pas de garder l'anonymat sur ses habitudes de navigation (connexions régulières à des services comme Facebook, Google, Gmail, Microsoft, etc., avec le même navigateur et les mêmes appareils. Voir > [Fingerprint](/docs/outils/navigateurs/fingerprint.html))

Il faut aussi comprendre que les informations (IP, trafic internet, DNS etc.) <u>sont juste détournées</u> du FAI vers le fournisseur VPN. Il doit avoir toute notre confiance car, lui, reçoit et connaît désormais absolument tout.

Ce qui se résume à: En qui ai-je le plus confiance, mon FAI ou mon VPN ?

La sécurité et la confidentialité avec un fournisseur VPN résident dans le choix du service le plus fiable possible, aucuns renseignements personnels et un paiement non traçable (cash ou bitcoin).

{{<hint sain>}}Si l'on cherche l'anonymat il est recommandé d'utiliser le navigateur [Tor](/docs/outils/navigateurs/choix-navigateurs.html) plutôt qu'un VPN.{{</hint>}}

{{<hint warning>}}

Il est déconseiller d'utiliser Tor avec un VPN (voir ci-après).

{{</hint>}}

## Un VPN n'ajoute pas de sécurité aux sites non sécurisés
***

Lorsque les fournisseurs annoncent que le trafic est chiffré il s'agit du chiffrement en transit dans le tunnel, entre l'appareil et le serveur du VPN seulement.

En sortie de tunnel, entre le serveur VPN et le serveur du site Web, les informations restent toujours en clair quoi qu'en disent les éléments de langages des fournisseurs. Ce qui veut dire, que tout ce qui est tapé, écrit, ou envoyé sur un site (numéro de carte bancaire, mots de passe, etc.) ne sera pas chiffré par magie grâce au VPN si le site n'utilise pas HTTPS.

Ce chiffrement est valable mais souvent beaucoup trop mis en avant et de manière trompeuse par les fournisseurs alors que l'on peut sécuriser cela sans VPN.

{{<hint sain>}}

**Pour se sécuriser:**

1.  Vérifier que le site sur lequel on donne des informations sensibles (mots de passe, code bancaire, etc.) est en [HTTPS](/docs/outils/navigateurs.html).
2.  [Chiffrer ses requêtes DNS](/docs/outils/dns.html).

{{</hint>}}

## De plus
***

-   Les services VPN clament haut et fort ne pas tenir de registre sur l'utilisateur et son utilisation, <u>c'est invérifiable</u>.
-   Juger de la qualité d'un fournisseur par le nombre d'utilisateurs est trompeur. Un service VPN qui utilise les données de ses clients ou coopère avec un gouvernement ou les forces de l'ordre, reste en place et continue d'avoir des millions d'utilisateurs (HideMyAss par ex.).
-   Une bonne partie des VPN sont détenus par des entreprises, parfois gouvernementales (Chinoises et Américaines) cachées par le biais de filiales. On comprend bien l'intérêt de posséder un services VPN et voir ou enregistrer le trafic de millions d'utilisateurs.
-   Le business en pleine croissance et les gens mal informés poussent les services à utiliser des éléments de langages basés sur la peur et l'insécurité pour conquérir de nouveaux utilisateurs.  Ils alimentent des croyances illusoires et mensongères plutôt que d'expliquer la réelle fonction et les limites de leur service.
-   Un VPN n'est pas à l'abri d'être bloqué par un service (Netflix par ex.) ou par un gouvernement (Chine par ex.).

{{<hint sain>}}

**Pour contourner une censure:** préférer l'utilisation de Tor et de ponts.

{{</hint>}}

## Les VPN et Tor
***
Quand une bonne partie de l'Internet conseille d'utiliser Tor avec un VPN, d'autres expliquent assez clairement le danger.

Ce qui revient fréquemment est que l'utilisation de Tor est visible par son FAI et qu'il vaut mieux cacher tout son trafic dans le tunnel avant d'entrer dans Tor. Mais en utilisant un VPN avec Tor, on crée essentiellement un nœud d'entrée permanent, souvent rattaché à un compte bancaire (paiement du VPN) donc à sa véritable identité. Cela n'apporte aucun avantage supplémentaire et augmente considérablement la surface d'attaque de sa connexion.

Si l'on souhaite cacher son utilisation de Tor à son FAI ou son gouvernement, Tor a une solution intégrée pour cela: [les ponts Tor](https://tb-manual.torproject.org/fr/bridges/) (Bridges).

## Téléchargement de fichiers
***

Un VPN n'est pas forcément le bon outil si l'utilité réside uniquement dans l'optique du téléchargement illégal massif. Il convient la plupart du temps pour un usage normal mais les risques d'être repéré subsistent si le service refile ses informations à des tiers.

Des réseaux vraiment sécurisés, anonymes et ultra-rapides sont spécialement prévus à cet effet et à des prix équivalents. Se renseigner sur [Usenet](https://fr.wikipedia.org/wiki/Usenet) par exemple.

## Les VPN gratuits
***

Un service VPN coûte cher à entretenir. Il s'agit de serveurs dédiés et puissants qui aident à rediriger les utilisateurs. Donc si un VPN est gratuit, c'est qu'il utilise nos données, voire installe des traqueurs sur nos appareils, et revend ces informations à des tiers pour financer ses serveurs.

Il ne faut pas utiliser de services VPN gratuits ! C'est inutile et cela aggrave la protection de confidentialité de l'utilisateur.

ProtonVPN a une version gratuite pour aider les personnes sans moyens financiers à sécuriser leur navigation uniquement. Elle n'équivaut pas une version supérieure de la gamme : les connexions peuvent être établies seulement aux USA, au Japon et en Hollande et ne cachent pas les téléchargements P2P. Elle est financée par les versions payantes, il n'y a pas de risque à utiliser la version gratuite de ProtonVPN pour naviguer sur Internet.

{{< next relref="/docs/outils/vpn/fournisseurs" >}} Fournisseurs VPN > {{< /next >}}
