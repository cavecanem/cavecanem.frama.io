---
title: "Fournisseurs de VPN"
bookToc: false
weight: 1
description: Liste des rares fournisseurs de service VPN auxquels on peut avoir confiance.
---
# Fournisseurs de service VPN
***

Il y a beaucoup de marketing derrière le business des fournisseurs VPN. Il faut donc être vigilant envers les effets d'annonce des fournisseurs peu chers et peu scrupuleux, et à l'utilisation d'éléments de langage basés sur la peur et l'insécurité.

La plupart prétendent ne pas tenir de registre des activités (_no log_), ce qui est invérifiable et probablement faux. En juin 2020, 7 fournisseurs basés Hong-Kong (UFO VPN, FAST VPN, Free VPN, Super VPN, Flash VPN, Secure VPN, Rabbit VPN) ont divulgués 1,2 TB de données d'utilisateurs, soit 1 083 997 361 _log_ hautement sensibles. Sites web visités, journaux de connexion, noms des personnes, adresses électroniques et personnelles des abonnés, mots de passe non chiffrés, informations de paiement Bitcoin et Paypal, messages aux services clients, spécifications des appareils utilisés et informations sur les comptes. Source : [theregister.com](https://www.theregister.com/2020/07/17/ufo_vpn_database/), 17/07/20. 

> _"Sur Internet, vous pouvez souvent trouver des services VPN qui prétendent que leurs services offrent un anonymat complet, une sécurité à toute épreuve, contournent toute censure, un streaming à l'épreuve des balles, etc. Cependant, les limites techniques du VPN sont assez claires et bien définies par la technologie. En termes simples, tout fournisseur qui prétend le contraire ment, ou pire, ne comprend pas pleinement les menaces."_
>
> ProtonVPN - [Understanding the VPN Threat Model](https://protonvpn.com/blog/threat-model/)

## Besoin d'un service VPN ?
***

Après avoir bien compris ce que permet et ne permet pas un service [VPN](vpn.html), ces trois sont les plus recommandables car ils ont <u>construit</u> une confiance solide.

{{<hint danger>}}

**Poubelle:** Tous ceux qui construisent un monopole, utilisent de mauvais éléments de langages et font de la pub partout sur Youtube et Internet.  
 NordVPN, ExpressVPN, HideMyAss, TunnelBear, CyberGhost, etc. ainsi que les VPN gratuits type Hola etc.

{{</hint>}}

{{<block info>}}

### ProtonVPN | [protonvpn.com](https://protonvpn.com/)
***

-   Gratuit (limité à la navigation uniquement (pas de P2P) et aux serveurs de 3 pays : USA, Japon, Hollande)
-   Basic: 5€/mois (2 appareils)
-   Plus: 10€/mois (5 appareils)
-   Sans engagement et prix dégressifs

{{</block>}}

{{<block info>}}

### MullVad | [mullvad.net](https://mullvad.net/fr/)
***

-  5€/mois (5 appareils)
-  Sans engagement
-  Utilisation sans compte client
-  Windows, macOS, Linux, iOS, Android, et configurations possible avec OpenVPN et WireGuard

{{</block>}}

{{<block info>}}

### IVPN | [ivpn.net](https://www.ivpn.net/)
***

-   6$/mois (2 appareils)
-   10$/mois (7 appareils)
-   Sans engagement et prix dégressifs

{{</block>}}

Regarder en détail les options désirées sur leurs sites respectifs.

N.B.: Un utilisateur n'a souvent pas besoin de VPN sur tous ses appareils. Il peut donc partager son compte avec ses amis ou ses proches (7 appareils = 7 personnes avec 1 appareil..).

{{< prev relref="/docs/outils/vpn" >}} < VPN {{< /prev >}}
