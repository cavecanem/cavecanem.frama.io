---
title: "Guides"
bookToc: false
weight: 2
description: Liste de guides pour la protection de la vie privée sur Internet.
---

# Guides
***

Guides pour la protection de la vie privée.

{{<tabs "Lang">}}

{{<tab "En français">}}

{{<list>}}
#### Surveillance Self-Defense | [ssd.eff.org](https://ssd.eff.org/fr)
***
Guide de l'EFF pour se défendre contre la surveillance en utilisant une technologie sécurisée et en développant des pratiques prudentes. (Français traduit)

#### Email Self-Defense | [emailselfdefense.fsf.org](https://emailselfdefense.fsf.org/fr/)
***
Guide de FSF pour lutter contre la surveillance avec le cryptage GnuPG.

#### Security in-a-Box | [securityinabox.org](https://securityinabox.org/fr/)
***
Un guide sur la sécurité numérique pour les militants et les défenseurs des droits de l'homme du monde entier.

#### Hackitude | [hackitude.fr](https://hackitude.fr/)
***
Guide pour une alternative à nos modes de vie.

#### Paf LeGeek | [youtube.com](https://www.youtube.com/channel/UCCSHWqosFfYJY5v2WqbTLhg/videos)
***
Chaîne Youtube sur l'explication et l'apprentissage des outils numériques liés à la sécurité et à la vie privée.
{{</list>}}

{{</tab>}}
{{<tab "En anglais">}}

{{<list>}}
#### The Crypto Paper | [github.com/cryptoseb/CryptoPaper](https://github.com/cryptoseb/CryptoPaper)
***
Confidentialité, sécurité et anonymat pour chaque utilisateur d'Internet.

#### Techlore | [techlore.tech](https://techlore.tech/)
***
Site Internet et surtout chaîne Youtube présentant des services et techniques pour protéger sa vie privée sur le web.

#### Rob Braxman Tech | [lbry.tv](https://lbry.tv/@RobBraxmanTech:6)
***
[Chaîne Youtube](https://www.youtube.com/channel/UCYVU6rModlGxvJbszCclGGw) également disponible sur [lbry.tv](https://lbry.tv). Rob Braxman présente ses guides, techniques et outils pour protéger sa vie privée sur Internet.

#### The Ultimate Privacy Guide | [bestvpn.com/the-ultimate-privacy-guide](https://www.bestvpn.com/the-ultimate-privacy-guide/)
***
Excellent guide sur la protection de la vie privée rédigé par les créateurs du site Web bestVPN.com.

#### IVPN Privacy Guides | [ivpn.net/privacy-guides](https://www.ivpn.net/privacy-guides)
***
Ces guides sur la vie privée expliquent comment obtenir une liberté, une intimité et un anonymat bien plus grands par le cloisonnement et l'isolement.

#### The Ultimate Guide to Online Privacy | [fried.com/privacy](https://fried.com/privacy)
***
Des astuces "ninjas" compréhensives et plus de 150 outils.

#### DuckDuckGo | [spreadprivacy.com](https://spreadprivacy.com/)
***
Compilation de gestes simples à réaliser.

{{</list>}}

{{</tab>}}
{{</tabs>}}

{{< prev relref="/docs/ressources" >}} < Ressources {{< /prev >}}
