---
title: "Informations"
bookToc: false
weight: 3
description: Liste des sites d'informations sur la sécurité et la protection de la vie privée.
---

# Informations
***

{{<tabs "Lang">}}
{{<tab "Français">}}

{{<list>}}
#### La Quadrature du Net | [laquadrature.net](https://www.laquadrature.net/)
***
Défense des libertés fondamentales dans l'environnement numérique.

#### Parti Pirate | [partipirate.org](https://partipirate.org/)
***
Mouvement politique international basé sur le partage et la défense des libertés fondamentales

#### Le code a changé | [franceinter.fr](https://www.franceinter.fr/emissions/le-code-a-change)
***
Podcast de Xavier de la Porte où il est question du numérique et comment/pourquoi il modifie nos vies.
{{</list>}}

{{</tab>}}
{{<tab "Anglais">}}

{{<list>}}
#### Electronic Frontier Foundation | [eff.org](https://www.eff.org/)
***
Principale organisation à but non lucratif qui défend la vie privée, la liberté d'expression et l'innovation dans le domaine numérique.

#### Freedom of the Press Foundation | [freedom.press](https://freedom.press/)
***
Soutient et défend le journalisme. Dédié à la transparence et à la responsabilité depuis 2012.

#### privacy.net | [privacy.net](https://privacy.net/us-government-surveillance-spying-data)
***
What does the US government know about you?

#### r/privacytoolsIO | [r/privacytoolsIO/wiki](https://www.reddit.com/r/privacytoolsIO/wiki/index)
***
Wiki de PrivacyTools.io sur reddit.com.

#### Security Now! | [grc.com/securitynow](https://www.grc.com/securitynow.htm)
***
Podcast hebdomadaire sur la sécurité sur Internet par Steve Gibson et Leo Laporte.

#### TechSNAP | [jupiterbroadcasting.com](https://www.jupiterbroadcasting.com/show/techsnap/)
***
Podcast hebdomadaire sur les systèmes, le réseau et l'administration. Chaque semaine, TechSNAP couvre les histoires qui ont un impact sur ceux d'entre nous qui travaillent dans l'industrie technologique.
{{</list>}}

{{</tab>}}
{{<tab "Allemand">}}

{{<list>}}
#### Erfahrungen | [erfahrungen.com](https://www.erfahrungen.com/mit/anonymisierung/t/)
***
Agrégateur de contenus liés aux services de protection de la vie privée.
{{</list>}}

{{</tab>}}
{{</tabs>}}

{{< prev relref="/docs/ressources" >}} < Ressources {{< /prev >}}
