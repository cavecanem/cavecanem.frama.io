---
title: "Médias"
bookToc: false
weight: 1
description: Liste de vidéos, films documentaires et podcasts autour du numérique et des questions de la vie privée.
---

# Médias
***

Vidéos, films et podcasts autour du numérique et en lien avec les questions de vie privée.

{{<list>}}
#### À écouter
***
[La Silicon Valley, le nouveau centre du pouvoir mondial ?](https://www.franceculture.fr/emissions/la-grande-table-idees/la-silicon-valley-le-nouveau-centre-du-pouvoir-mondial), Olivia Gesbert, La Grande Table Idées, France Culture, 2020 <span class="gris">33min</span>

[Ce que les pouvoirs gagnent à tout savoir de nos vies](https://www.franceinter.fr/emissions/le-code-a-change/ce-que-les-pouvoirs-gagnent-a-tout-savoir-de-nos-vies), Xavier de La Porte, Le code a changé, France Inter, 2020 <span class="gris">38min 20s</span>

[Le code à changé](https://www.franceinter.fr/emissions/le-code-a-change), Xavier de La Porte, série de podcasts sur le numérique et les changements qu'il opère, France Inter, 2020
{{</list>}}

{{<list>}}
#### À voir
***

[The Social Dilemma](https://www.netflix.com/fr/title/81254224), film documentaire, Netflix, 2020 <span class="gris">1h 34min</span> > Regarder sur [PeerTube](https://bittube.video/search?search=derri%C3%A8re%20nos%20%C3%A9crans%20de%20fum%C3%A9e&searchTarget=local)

[Tous surveillés - 7 milliards de suspects](https://boutique.arte.tv/detail/tous_surveilles_7_milliards_de_suspects), documentaire, Arte, 2020 <span class="gris">1h 29min</span> > Regarder sur [PeerTube](https://bittube.video/search?search=tous%20surveill%C3%A9s%20-%207%20milliards%20de%20suspects&searchTarget=local) ou  [Invidious](https://invidious.snopyta.org/watch?v=ibeeOcT3IsQ)

[The Great Hack](https://www.thegreathack.com/), documentaire sur le cas Cambridge Analytica, Netflix, 2019 <span class="gris">1h 54min</span> > Regarder sur [PeerTube](https://bittube.video/search?search=The%20Great%20Hack%20:%20L%27affaire%20Cambridge%20Analytica&searchTarget=local)

[Votre vie privée contre des services ?](https://invidious.snopyta.org/watch?v=KmVdjGeqWD0), Baptiste ROBERT, conférence, TEDxÉcoleCentraleLyon, 2019 <span class="gris">17min 36s</span>

[Internet fragilise-t-il la démocratie ?](https://www.arte.tv/fr/videos/083397-022-A/tous-les-internets-internet-fragilise-t-il-la-democratie/), documentaire, Arte, 2018 <span class="gris">6min 35s</span> > Regarder sur [Invidious](https://invidious.snopyta.org/watch?v=_ys1LOlo5uE)

[Nothing to hide](https://media.zat.im/videos/watch/35badfed-5322-48ac-b5c1-71b1ad88262e), film documentaire, 2017 <span class="gris">1h 26min</span>

[Les nouvelles routes de la soie](https://invidious.snopyta.org/watch?v=FuhUo7Tt7Hc), documentaire, Arte, 2016 <span class="gris">53min 44s</span>

[Nos données ne sont pas des marchandises](https://www.laquadrature.net/donnees_perso/), La Quadrature du Net <span class="gris">11min 20s</span>

[Contre la censure, la décentralisation](https://www.laquadrature.net/censure/), La Quadrature du Net <span class="gris">6min 19s</span>

[Résistons à la surveillance](https://www.laquadrature.net/surveillance/), La Quadrature du Net <span class="gris">18min 47s</span>

[Why privacy matters](https://www.ted.com/talks/glenn_greenwald_why_privacy_matters), Glenn Greenwald, conférence sur l'importance de la confidentialité, TEDGlobal 2014 <span class="gris">20min 38s</span>

[The Internet's Own Boy](https://invidious.snopyta.org/watch?v=7ZBe1VFy0gc), documentaire sur Aaron Swartz, 2014 <span class="gris">1h 45min</span>
{{</list>}}

{{<list>}}
#### À lire
***
[L’Âge du capitalisme de surveillance](https://www.zulma.fr/livre-lage-du-capitalisme-de-surveillance-572196.html), Shoshana Zuboff, Zulma, 2020

[Celui qui pourrait changer le monde](https://editions-b42.com/produit/celui-qui-pourrait-changer-le-monde-2/), Écrits d'Aaron Swartz, Éditions B42, 2017

{{</list>}}

{{< prev relref="/docs/ressources" >}} < Ressources {{< /prev >}}
