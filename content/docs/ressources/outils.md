---
title: "Outils"
bookToc: false
weight: 4
description: Liste de sites référençant des alternatives aux applications et services propriétaires courants.
---

# Outils
***
Pour aider à trouver les alternatives aux applications et services propriétaires utilisés couramment.

## Répertoires
***

Listes les différents outils, applications, logiciels ou services à utiliser pour voir sa confidentialité respectée.

{{<list>}}
#### Privacytools.io | [privacyTools.io](https://www.privacytools.io/)
***
Répertoire d'outils et de services définis d'après une charte de protection de la vie privée stricte.

#### PRISM Break | [prism-break.org](https://prism-break.org/)
***
Outils pour chiffrer nos communications et en mettre fin à notre dépendance à l'égard de services propriétaires.

#### Reset The Net - Privacy Pack | [pack.resetthenet.org](https://pack.resetthenet.org/)
***
Aide à lutter pour mettre fin à la surveillance de masse. Des outils pour vous protéger, vous et vos amis.

#### Osalt | [osalt.com](https://www.osalt.com/)
***
Un répertoire pour aider à trouver des alternatives _open source_ aux outils propriétaires.

#### AlternativeTo | [alternativeto.net](https://alternativeto.net/)
***
Un répertoire pour aider à trouver des alternatives à d'autres logiciels, avec la possibilité de ne montrer que les logiciels _open source_.
{{</list>}}

## Services
***

Outils pratiques et applications respectant la vie privée.

{{<tabs "Lang">}}
{{<tab "En français">}}

{{<list>}}
#### Framasoft | [framasoft.org](https://framasoft.org/fr/)
***
Association d’éducation populaire et de mise à dispositions d'outils numériques gratuits pour contrer les GAFAM.

#### La Brique Internet | [labriqueinter.net](https://labriqueinter.net/)
***
Box multi-fonction pour un Internet libre, neutre et décentralisé chez soi.

#### Exodus Privacy | [exodus-privacy.eu.org](https://exodus-privacy.eu.org/fr/)
***
Analyse les problèmes de vie privée dans les applications Android.

#### justdeleteme | [justdeleteme.xyz](https://justdeleteme.xyz/fr)
***
Un annuaire de liens pour supprimer ses comptes des sites Webs.

{{</list>}}

{{</tab>}}
{{<tab "En anglais">}}

{{<list>}}
#### Terms of Service; Didn't Read | [tosdr.org](https://tosdr.org/)
***
"I have read and agree to the Terms" est le plus gros mensonge du Web. Ils veulent y remédier.

#### ipleak | [ipleak.net](https://ipleak.net/)
***
IP/DNS Detecteur - Quelle est votre adresse IP, quel est votre DNS, quelles informations vous envoyez aux sites Web.

#### dnsleaktest | [dnsleaktest.com](https://dnsleaktest.com/)
***
Quelle est votre adresse IP, vérifie si vous avez des fuites de DNS.

#### Check My DNS | [cmdns.dev.dns-oarc.net](https://cmdns.dev.dns-oarc.net/)
***
Un outil puissant pour étudierla qualité de son résolveur DNS

#### FreedomBox | [freedombox.org](https://freedombox.org/)
***
Box multi-fonctions, pour un Internet libre, neutre et décentralisé chez soi.

#### Energized System | [energized.pro](https://energized.pro/)
***
Liste des domaines qui diffusent des publicités, des scripts de traqueurs et des logiciels malveillants provenant de plusieurs sources réputées afin de crée un fichier host ou une blacklist qui empêche votre système de s'y connecter.

#### filterlists | [filterlists.com](https://filterlists.com/)
***
Liste et information des domaines qui diffusent des publicités, des scripts de traqueurs et des logiciels malveillants afin de maintenir un fichier host ou une blacklist.

#### The ultimate Online Privacy Test Resource List | [ghacks.net](https://www.ghacks.net/2015/12/28/the-ultimate-online-privacy-test-resource-list/)
***
Un ensemble de sites Internet qui vérifient si votre navigateur Web fuit des informations.

#### Security First | [secfirst.org](https://secfirst.org/)
***
Umbrella est une application qui fournit tous les conseils nécessaires pour travailler en toute sécurité dans un environnement hostile.

#### SecureDrop | [securedrop.org](https://securedrop.org/)
***
Un système _open source_ pour les lanceurs d'alerte que les médias peuvent utiliser afin d'accepter en toute sécurité des documents provenant de sources anonymes et communiquer avec elles. Créé à l'origine par feu [Aaron Swartz](http://www.aaronsw.com/) (1986-2013) il est actuellement géré par la Fondation pour la liberté de la presse.

{{</list>}}

{{</tab>}}
{{</tabs>}}

{{< prev relref="/docs/ressources" >}} < Ressources {{< /prev >}}
