---
title: "Matériel"
bookToc: false
weight: 5
description: Liste du matériel conçus "par design" pour maintenir la sécurité et la vie privée de leurs utilisateurs.
---

# Matériel
***

Ces produits sont conçus "par design" pour sécuriser et maintenir la vie privée de leurs utilisateurs.

{{<list>}}

#### Purism | [puri.sm](https://puri.sm/)
***
Téléphone, ordinateurs, serveur... Le matériel, les pièces et le design sont ici choisis afin de servir au mieux la sécurité de ses utilisateurs. Purism a développé et pré-installé le système d'exploitation [PureOS](https://pureos.net/) pensé également pour protéger l'utilisateur et sa confidentialité.

#### Pine64 | [pine64.org](https://www.pine64.org/pinephone/)
***
Téléphone (PinePhone), tablette (PineTab), ordinateurs (PineBook et Pro), montre (PineTime)... _open source_ utilisant des pièces non propriétaires et pouvant utiliser les systèmes d'exploitation Linux.

#### Tails | [tails.boum.org](https://tails.boum.org/index.fr.html)
***
Système d'exploitation Linux fonctionnant sur clé USB. Aucune installation requise. La clé se branche à n'importe quel ordinateur et aucune trace de son passage n'est présente une fois éjectée.

{{</list>}}

{{< prev relref="/docs/ressources" >}} < Ressources {{< /prev >}}
