---
title: "Alliances de surveillance"
bookToc: false
weight: 4
description: Qu'est-ce que les alliances de surveillances ? Quels pays en font partie ?
summary: Accords en vue de collecter, d’analyser et de partager des renseignements en coopération et ainsi contourner les lois nationales qui pourraient interdire l’espionnage de ses citoyens.
---

# Les alliances de surveillance
***

Ces alliances créent des accords en vue de collecter, d’analyser et de partager des renseignements en coopération et ainsi contourner les lois nationales qui pourraient interdire l’espionnage de ses citoyens.

Les membres de ce groupe, connu sous le nom de _Five Eyes_ (cinq yeux), se concentrent sur la collecte et l’analyse de renseignements provenant de différentes parties du monde.

Bien que les pays membres du _Five Eyes_ aient convenu de ne pas s’espionner mutuellement en tant qu’adversaires, les fuites d’Edward Snowden ont remis l’accent sur le _Five Eyes_ en 2013 lorsqu’il a exposé les activités de surveillance du gouvernement américain et de ses alliés entre eux.

Elles ont aussi révélé que certains membres des _Five Eyes_ surveillent leurs citoyens respectifs et partagent ces renseignements entre elles pour contourner les lois nationales qui leur interdisent d’espionner leurs propres citoyens.

L’alliance _Five Eyes_ coopère également avec des groupes de pays tiers pour partager des renseignements, formant les groupes _Nine Eyes_ et _Fourteen Eyes_. Il s’agit d’extensions de l’alliance _Five Eyes_ avec une coopération similaire dans la collecte et le partage des données issues de la surveillance de masse. Cependant, l'alliance _Five Eyes_ et celle des pays tiers ne les empêche pas de s’espionner mutuellement, ce qu'ils font.

{{< columns >}}

### Five Eyes
***

<span class="flag-icon flag-icon-au"></span> Australie

<span class="flag-icon flag-icon-ca"></span> Canada

<span class="flag-icon flag-icon-us"></span> États-Unis d'Amérique

<span class="flag-icon flag-icon-nz"></span> Nouvelle-Zélande

<span class="flag-icon flag-icon-gb"></span> Royaume-Uni

<--->

### Nine Eyes
***

Les _Five Eyes_ +

<span class="flag-icon flag-icon-dk"></span> Danemark

<span class="flag-icon flag-icon-fr"></span> France

<span class="flag-icon flag-icon-no"></span> Norvège

<span class="flag-icon flag-icon-nl"></span> Pays-Bas

<--->

### Fourteen Eyes
***

Les _Nine Eyes_ +

<span class="flag-icon flag-icon-de"></span> Allemagne

<span class="flag-icon flag-icon-be"></span> Belgique

<span class="flag-icon flag-icon-es"></span> Espagne

<span class="flag-icon flag-icon-it"></span> Italie

<span class="flag-icon flag-icon-se"></span> Suède

{{</columns>}}

{{< prev relref="/docs/start/threat-model" >}} < Threat model {{< /prev >}}
{{< next relref="/docs/start/lois-divulgation" >}} Lois de divulgation des clés  > {{< /next >}}
