---
title: "Le chiffrement"
bookCollapseSection: true
bookToc: false
weight: 7
description: Pourquoi devons-nous chiffrer nos contenus numériques ? Quels sont les différentes formes de chiffrement ? Lesquelles et quand doivent-elles être utilisés ?
---

# Le chiffrement
***

Le chiffrement une des solutions utilisé quotidiennement sans que l'on s'en rende toujours compte. Il protège les données en les rendant illisibles en transit, stockées, ou jusqu'au destinataire authentifié.

Cela protège les données numériques des particuliers, de l'armée, des scientifiques, des gouvernements, des entreprises, des organisations politiques, des journalistes, etc.

## Principe
***

Le chiffrement est un procédé cryptographique qui remplace le code source des fichiers par des caractères incompréhensibles à l'aide d'un algorithme. En échange d'un mot de passe ou d'une clé on peut à nouveau inverser le processus correctement et lire le fichier: le déchiffrer.

Exemple avec une phrase :

1.  "Cette phrase doit être chiffrée".
2.  Je chiffre la phrase avec le mot de passe "Sésame".
3.  La phrase devient "ç!ytfgw*5*T%'$^ù(ùé(§'-XAR6Gmg,scg"
4.  Il n'y a plus que le mot de passe qui permet de lire la phrase. Si on ne le connaît pas c'est impossible de la lire.
5.  Lorsque je rentre le mot de passe "Sésame", la phrase redevient "Cette phrase doit être chiffrée".

## Fonction
***

Cela permet de rendre illisibles ses données si elles sont interceptées lors d'une communication, piratées sur un ordinateur ou un serveur, perdues ou volées.

Si je perd mon ordinateur. Le mot de passe de ma session n'empêche absolument pas de retirer le disque dur et de l'ouvrir avec un autre ordinateur pour voir et lire les fichiers. Si le disque dur ou les fichiers sont chiffrés, même en voyant les fichiers, ils seront complètement illisibles sans la clé.

## Catégories
***

Il existe en gros 3 catégories de chiffrement. Elles ont chacune une fonction bien précise et peuvent être cumulées.

-  [**Au repos :**](/docs/start/chiffrement/au-repos.html) pour ce qui est stocké (sur un disque dur, un serveur, une clé usb, etc.).
-  [**En transit :**](/docs/start/chiffrement/en-transit.html) pour ce qui circule entre un appareil et un espace de stockage ou entre deux appareils. (communication avec un site Internet, par message ou mail, par appel, etc.)
-  [**De bout en bout :**](/docs/start/chiffrement/de-bout-en-bout.html) chiffrement réalisé sur l'appareil avant même qu'il ne soit envoyé. Il peut être ouvert seulement par un appareil autorisé par l'expéditeur.


Parfois, il est aussi judicieux ou préférable de chiffrer [manuellement](/docs/outils/chiffrer.html) ses fichiers si le service n’est pas chiffré de bout en bout, ou si notre accord de confiance envers lui est questionné.

{{<block sain>}}

### Pratique saine
***

-  Chiffrer ses espaces de stockage lorsqu'ils contiennent des fichiers sensibles ou personnels.
-  Toujours utiliser des outils qui utilisent le chiffrement en transit pour envoyer des informations aux serveurs.
-  Toujours utiliser des outils qui utilisent le chiffrement au repos sur leurs serveurs.
-  Utiliser impérativement le chiffrement de bout en bout pour ses données sensibles ou personnelles.
-  Toujours chiffrer ses contenus sensibles avant envoi.
-  Combiner les types de chiffrement.
-  Ne jamais confier sa vie privée à une entreprise, chiffrer toutes ses données personnelles.

{{</block>}}

{{< prev relref="/docs/start/code-source" >}} < Le code source {{< /prev >}}

{{< next relref="/docs/start/chiffrement/au-repos" >}} Au repos > {{< /next >}}
