---
title: "Au repos"
weight: 1
bookToc: false
---
# Le chiffrement au repos

* * *

C'est un chiffrement sur un espace de stockage.

Les services en ligne qui stockent les informations des utilisateurs utilisent presque tous le chiffrement de leurs serveurs pour contrer les attaques. Lors de l'envoi du fichier, les données sont chiffrées <u>une fois arrivées</u> sur le serveur.

Ces services ne sont pas égaux dans le chiffrement.
-   La plupart connaissent les clés pour déchiffrer.
-   D'autres utilisent des serveurs Zero-Knowledge: ils n'ont aucun moyen de déchiffrer les fichiers.

Il est donc important de connaître comment un service chiffre les fichiers reçus et où se situe ses serveurs (pour éviter une [loi de divulgation des clés](/docs/start/lois-divulgation.html) par exemple).


{{<block sain>}}

### Pratique saine

* * *

-   Toujours vérifier qu'un service utilise bien le chiffrement au repos sur ses serveurs et quelle est sa capacité de les déchiffrer.
-   Chiffrer son disque dur d'ordinateur portable et son téléphone s'ils contiennent des fichiers sensibles (travaux, photos, codes, contacts, emails, etc.) pour éviter l'utilisation de ses données en cas de perte ou de vol > voir [chiffrement disque dur](manuel/disque-dur.html).
-   Chiffrer manuellement ses fichiers sensibles avant envoi sur Cloud, serveur, transfert, email, messagerie etc. > voir [chiffrement manuel](manuel.html).
-   Chiffrer manuellement ses fichiers sensibles dans un conteneur chiffré si le disque dur ne l'est pas entièrement ou si besoin de les mettre sur un disque dur externe, clé USB, DVD, etc.

{{</block>}}

{{< prev relref="/docs/start/chiffrement" >}} < Chiffrement {{< /prev >}}
{{< next relref="/docs/start/chiffrement/en-transit" >}} En transit > {{< /next >}}
