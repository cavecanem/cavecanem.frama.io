---
title: "De bout en bout"
weight: 3
bookToc: false
---
# Le chiffrement de bout en bout

* * *

En anglais: _End-to-end encryption_ ou _E2EE_.

Le chiffrement de bout en bout garantit qu'un message est chiffré par son expéditeur initial, et déchiffré uniquement par son destinataire final.

Les autres formes de chiffrement peuvent dépendre d'un chiffrement effectué par des tiers, ce qui signifie qu'il faut leur faire totalement confiance.

Le chiffrement de bout en bout est généralement considéré comme plus sûr, car il réduit le nombre de tiers qui pourraient interférer et casser le cryptage.

Le serveur par lequel transite le fichier ou la conversation ne sert que de relai car il ne peut rien déchiffrer.

Ce système est utilisé principalement pour sécuriser les utilisateurs dans les services d'échanges (fichiers, messages, etc.).

## Fonctionnement

* * *

Les systèmes de chiffrement de bout en bout sont conçus pour résister à toute tentative de surveillance ou de falsification.

Il crée, pour chaque utilisateur, une clé privée (déchiffrement) et une clé publique (chiffrement).


1.  La clé privée est générée aléatoirement.
1.  La clé publique est générée à partir de la clé privée.
1.  Pour recevoir un message, j'envoie ma clé publique à Hugo.
1.  Hugo chiffre son message avec ma clé publique.
1.  Je déchiffre le message reçu grâce à ma clé privée.
1.  Moi seul peut prendre connaissance des messages qu'Hugo m'envoie.
1.  Il suffit qu'Hugo applique le même procédé pour que l'échange de nos clés publiques nous permette une communication bi-directionnelle sécurisée.


## Gestion des clés

* * *

iMessage, Facetime, Signal, Telegram, Whatsapp, utilisent l'E2EE par exemple. Pour simplifier l'utilisation, l'échange des clés est automatique.

Certains services ou protocoles ([PGP](manuel/pgp.html) par ex.) délivrent les clés publiques et privées.

Les utilisateurs doivent alors échanger eux-même les clés publiques avant de commencer à échanger des messages chiffrés.

Si c'est un peu plus complexe, c'est aussi plus sécurisé et cela permet d'utiliser le service plus librement n'importe où, avec n'importe quel appareil.

{{<hint danger>}}

Parfois, des services demandent à ce que la clé privée soit stockée par l'utilisateur. Si c'est le cas, le faire de manière sûre > voir [mots de passe](/docs/outils/m
ots-de-passe.html).

Si elle est perdue, tous les fichiers le seront aussi; <u>personne</u> ne pourra jamais la récupérer car personne ne la connaît !

{{</hint>}}

{{<block sain>}}

### Pratique saine

* * *

-   Utiliser le plus possible des services avec le chiffrement de bout en bout.
-   Sauvegarder ses clés privées dans un endroit sûr.
-   Même avec un chiffrement de bout en bout, il peut être utile de chiffrer manuellement ses fichiers avant envoi.

{{</block>}}

{{< prev relref="/docs/start/chiffrement" >}} < Chiffrement {{< /prev >}}
{{< next relref="/docs/start/metadonnees" >}} Les métadonnées > {{< /next >}}
