---
title: "En transit"
weight: 2
bookToc: false
---
# Le chiffrement en transit

* * *

Chiffrement de ce qui circule entre un appareil et un espace de stockage (serveur ou appareil), donc à partir de l'émetteur et jusqu'au destinataire.

Il existe différents protocoles, les plus utilisés sont TLS - SSL.  
– Explication: [HTTPS et TLS - Le chiffrement des messages](https://invidious.snopyta.org/watch?v=_6ukY5p6vTY) <span class="gris">5min 37s</span>

**N.B.:** Ces protocoles sont uniquement fait pour le transit. Ils n'offrent pas un chiffrement [au repos](au-repos.html) ou [de bout en bout](de-bout-en-bout.html).

## Exemples d'utilisation

* * *

Ces protocoles deviennent la norme pour protéger les échanges des utilisateurs. Ils permettent d'éviter l'interception et la lecture d'informations confidentielles par un homme du milieu (pirate, espion, gouvernement, FAI, etc.).

-   Les mails sont donc aujourd'hui quasiment toujours envoyés en TLS avant d'atteindre le serveur.
-   Les sites qui demandent des informations confidentielles (carte de crédit, noms et adresses, etc.) sont aujourd'hui pour la plupart chiffrés avec HTTPS. Ce que les utilisateurs écrivent est envoyé chiffré.

{{<block sain>}}

### Pratique Saine

* * *

-   Toujours vérifier qu'un site est en HTTP<u>**S**</u> avant de donner ses informations (Cadenas vert dans le navigateur à côté de l'adresse Web).
-   Toujours vérifier que son service email utilise bien TLS.
-   Si la messagerie instantanée ne chiffre pas de bout en bout, vérifier qu'elle utilise TLS ou SSL.
-   Assurer la redirection automatique d'un site vers sa version sécurisé (quand elle existe) en utilisant [HTTPS Everywhere](https://www.eff.org/https-everywhere) > voir [extensions de navigateurs](/docs/outils/navigateurs/extensions.html).

{{</block>}}

{{< prev relref="/docs/start/chiffrement" >}} < Chiffrement {{< /prev >}}
{{< next relref="/docs/start/chiffrement/de-bout-en-bout" >}} De bout en bout > {{< /next >}}
