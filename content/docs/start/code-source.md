---
title: "Le code source"
bookToc: false
weight: 6
description: "Qu'est-ce que le code source ? Quelques notions informatiques sur le code, la source, l'open source et l'univers du libre."
---

# Le code source
***

Parce que le code source est ce qui importe dans ce chapitre, il faut déjà comprendre comment nos appareils numériques fonctionnent.

Un appareil numérique, quelqu'il soit, sert à faire des calculs en vue d'une tâche spécifique. Pour se faire, des programmes numériques sont “codés”, de manière à indiquer une marche à suivre.

## Le code
***

Ce que l'on appelle vulgairement le "code" est le langage de programmation qu'un appareil numérique utilise et interprète en binaire, 0 ou 1. C'est ce qui lui permet de faire les calculs nécessaires à la réalisation de la tâche souhaitée.

Il existe énormément de langages de programmation. Ils sont choisis en adéquation avec le type d'utilisation, le type d'appareil ou la tâche à effectuer.

Un langage est utilisé par les ordinateurs, les _smartphone_, les tablettes, les montres connectées, les _smart TV_, les consoles de jeux, les appareils photos numériques, les robots ménagers, les voitures, etc. Il permet d'utiliser ensuite des systèmes d'exploitation, qui utilisent à leur tour le code de logiciels et de programmes informatiques comme les [applications mobiles](/docs/outils/applications.html) ou les [navigateurs](/docs/outils/navigateurs.html), les sites internet, les jeux vidéos, etc.

Le code est donc utilisé partout et à la source de n'importe quel appareil et application numérique. C'est donc cette source qui importe le plus car elle permet de connaître la base du fonctionnement, ou du dysfonctionnement, d'une application.

Les _hackers_ sont très connus pour utiliser le code source afin de détourner la fonction initiale et parfois restrictives des appareils ou des applications. Tous les créateurs de nos outils numériques quotidiens ont été ou sont des _hackers_. Certains _hackers_ se servent de leurs idées pour dérober des utilisateurs ([_black hat_](https://fr.wikipedia.org/wiki/Black_hat)), d'autres pour rendre l'utilisation des appareils ou des logiciels meilleure et plus sécurisée ([_white hat_](https://fr.wikipedia.org/wiki/White_hat)).

## Exemple de code source
***

Prenons un site Web. Pour fonctionner, il a besoin d'être codé avec au moins un langage: le HTML. C'est celui que peuvent lire n'importe quel navigateur depuis la création d'Internet. Le navigateur interprète le code HTML pour donner un résultat défini par le programmeur.

Il est facile d'afficher le code source de cette page par exemple, en faisant Clic droit > Afficher la source.

Cette source permet à ceux qui savent lire HTML de voir ce que contient exactement cette page et si des erreurs ou même des informations cachées comme des traqueurs seraient présentes.

## L'_open source_
***

Donc, comme on vient de le montrer, quand on peut voir la source, on peut travailler en collaboration et vérifier la base d'une page web, d'un programme, d'une application.

L'_open source_, "code source ouvert" en français, s'applique aux logiciels et aux œuvres de l'esprit dont la licence respecte des critères précisément établis par l'[_Open Source Initiative_](https://fr.wikipedia.org/wiki/Open_Source_Initiative). Critères tels que les possibilités de libre redistribution, d'accès au code source et de création de travaux dérivés. Mis à la disposition du grand public, ce code source est généralement le résultat d'une collaboration entre programmeurs.

Le mouvement _open source_, focalisé sur les techniques de développement logiciel, s'est développé en parallèle du [mouvement du logiciel libre](https://fr.wikipedia.org/wiki/Mouvement_du_logiciel_libre) qui prône des valeurs philosophiques et politiques de justice. Dans la pratique, la très grande majorité des logiciels _open source_ sont également libres  (FOSS, voir ci-après).

L’_open source_ a déjà investi tous les grands domaines du système d’information des administrations françaises: environnements serveurs, domaines applicatifs, outils d’ingénierie, solutions de réseaux et sécurité. Les solutions _open source_ sont désormais au même rang que les solutions propriétaires dans le paysage des logiciels du secteur public. Les décideurs effectuent d’ailleurs de plus en plus leur choix à partir d’un jugement éclairé, en comparant systématiquement solutions propriétaires et solutions libres.

Source: [fr.wikipedia.org/wiki/Open_source](https://fr.wikipedia.org/wiki/Open_source)

## Code propriétaire
***

Opposés à l'_open source_, les logiciels propriétaires, dont le code est fermé volontairement. Soit pour des raisons de "sécurité", mais plus souvent pour des raisons mercantiles. Le problème avec ces entreprises privées et qu'en les infiltrant et en collaborant avec elles, la NSA a mené une attaque d'une ampleur terrifiante contre la cryptographie publique en affaiblissant délibérément les normes internationales et communes de cryptage, afin d'espionner massivement la population. Ceci a démontré qu'aucun logiciel propriétaire ne peut être fiable, même les logiciels spécifiquement conçus dans un souci de sécurité.

La NSA a coopéré avec, ou a contraint, des centaines d'entreprises technologiques à intégrer des accès _backdoors_ (portes dérobées) dans leurs programmes et d'en affaiblir la sécurité afin de lui permettre d'y accéder. Les entreprises américaines et britanniques sont particulièrement suspectes, bien que les rapports indiquent clairement que des entreprises du monde entier ont cédées aux demandes de la NSA.

Le problème avec les logiciels propriétaires est que des gouvernements ou des services d'espionnage peuvent assez facilement approcher et convaincre les développeurs et propriétaires de jouer le jeu. Leur code source étant gardé secret, il est donc facile d'ajouter ou de modifier le code de manière douteuse sans que personne ne s'en aperçoive.

## FOSS
***

En anglais: _Free _open source_ Software_ pour Logiciel libre et source ouverte.

La meilleure réponse à ce problème est d'utiliser des logiciels libres et _open source_. Souvent développés conjointement par des personnes disparates et sans lien entre elles, le code source est accessible à tous pour être examiné et évalué par des pairs. Cela minimise les chances que quelqu'un le falsifie, ou qu'une entité essaye de corrompre un groupe.

Idéalement, ce code devrait également être compatible avec d'autres mises en œuvre, afin de minimiser la possibilité d'intégrer une _backdoor_.

Il est bien sûr possible que des agents de la NSA aient infiltré des groupes de développement de logiciels libres et introduit un code malveillant à l'insu de tous. En outre, la quantité de code que de nombreux projets impliquent signifie qu'il est souvent impossible de procéder à un examen complet de l'ensemble du code par les pairs. Certains font donc appel à des entreprises indépendantes pour inspecter l'entièreté du code afin de s'assurer d'autant plus de sa sécurité.

Malgré des écueils potentiels, les logiciels libres restent les plus fiables et les plus inviolables du marché. Lorsqu'on se soucie vraiment de la protection de la vie privée, il faut essayer de les utiliser exclusivement (jusqu'à l'utilisation de systèmes d'exploitation libres tels que Linux).

{{< prev relref="/docs/start/lois-divulgation" >}} < Lois de divulgation des clés {{< /prev >}}
{{< next relref="/docs/start/chiffrement" >}} Le chiffrement > {{< /next >}}
