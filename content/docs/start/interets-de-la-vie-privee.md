---
title: "Intérêts de la vie privée"
bookToc: false
weight: 2
description: Quels sont les intérêts de la vie privée sur Internet ? Pourquoi s'en soucier est devenu un enjeu essentiel aujourd'hui ?
---

# Intérêts de la vie privée
***

On a tous entendu parler d'[Edward Snowden](https://fr.wikipedia.org/wiki/Edward_Snowden), de [Julian Assange](https://fr.wikipedia.org/wiki/Julian_Assange), de la vie privée, de l'espionnage de la NSA, de la [surveillance de masse](https://fr.wikipedia.org/wiki/Surveillance_globale) ou du programme [PRISM](https://fr.wikipedia.org/wiki/PRISM_(programme_de_surveillance)), mais on se penche trop peu sur le sujet pour en connaître les enjeux.

Sachant qu'on utilise quotidiennement des outils numériques, il semble urgent de comprendre ce qui est à l'œuvre depuis des années.

## Crise de la vie privée
***

La confidentialité est aujourd'hui en crise car elle est en train de disparaître petit à petit.

De l'adresse de son domicile, son numéro de téléphone, son numéro de carte d'identité et de passeport, ses comptes bancaires, l'historique de ses achats et crédits, l'adresse des magasins où l'on fait ses courses, l'adresse de son travail, ses habitudes de voyage, tout ce qui a trait à la vie personnelle est enregistré et directement disponible à ceux qui pourraient être intéressés.

Internet a créé un terrain propice pour récolter rapidement et efficacement toutes ces données et cela peut avoir de multiples impacts:

-  **Harcèlement marketing.** Les entreprises bombardent nos emails et nos téléphones qu'ils ont achetés auprès de compagnies, banques, réseaux sociaux, vente en ligne, etc.
-  **Fraudes bancaires.** Les numéros de cartes de crédit sont régulièrement volés, revendus et utilisés à notre insu.
-  **Vol d'identité.** Des infractions ou crimes sont commis au nom d'une autre personne (copies de plaques d'immatriculation, d'empreintes digitales, boîtes email ou comptes Facebook piratés et utilisés par d'autres pour envoyer des messages, etc.).
-  **_Stalking_**. Le suivi en ligne peut prendre la forme de harcèlement et peut aller jusqu'à la violence. La possibilité pour tout le monde de traquer de manière précise et intense n'importe qui en ligne est aujourd'hui facilitée avec les réseaux sociaux ou des boîtes qui revendent des informations personnelles (adresse, numéro de téléphone, etc.).
-  **Surveillances gouvernementales.** La NSA a développé [Echelon](https://fr.wikipedia.org/wiki/Echelon), qui espionne toutes les communications autour du monde. [FinCen](https://fr.wikipedia.org/wiki/Financial_Crimes_Enforcement_Network) qui récupère les informations bancaires en temps réel. [XKeyscore](https://fr.wikipedia.org/wiki/XKeyscore) qui peut mettre n'importe qui sur écoute en utilisant une adresse email. [Fascia](https://en.wikipedia.org/wiki/FASCIA_(database)) qui peut traquer les téléphones dans le monde entier. [Optic Nerve](https://fr.wikipedia.org/wiki/Optic_Nerve_(programme_de_surveillance)) qui enregistre les visioconférences Yahoo et [PRISM](https://fr.wikipedia.org/wiki/PRISM_(programme_de_surveillance)) qui permet d'accéder aux données personnelles des utilisateurs de services américains (Yahoo, Facebook, Google, Apple, Microsoft etc.). L’étendue de cette surveillance n’a été révélée que récemment, grâce à Edward Snowden en 2013. Il existe pour les gouvernements, des centaines de programmes disponibles, pas seulement américains, conçus pour traquer de manière politique les mouvements des citoyens innocents et ne résultant ni d'avertissements, ni de suspicions valables et justifiées.

## La vie privée et la sécurité
***

Les éléments de langage opposent régulièrement ces deux notions.

La vie privée est un droit à la confidentialité.

L’intérêt d’avoir une vie privée est de pouvoir choisir à qui l’on souhaite, ou pas, montrer ses photos de vacances, ses analyses médicales ou ses comptes bancaires par exemple. Et ce n’est pas parce qu’on ne veut pas les montrer à ses voisins qu’on a quelque chose à cacher ou que l’on est un criminel.

_— "Si les gouvernements font cela c'est que c'est bénéfique pour notre sécurité."_

À l'évidence, non.
-  Des études de 2013 ont analysé les cas terroristes depuis 2001 et conclu que la collection d'enregistrements des téléphones par la NSA était "non-essentielle pour prévenir des attaques". Source: [propublica.org](https://www.propublica.org/article/whats-the-evidence-mass-surveillance-works-not-much)
-  En 2013, la NSA a clamé avoir arrêté plus de 50 attaques terroristes mais la liste est classifiée donc ces sources invérifiables. Ils ont sorti publiquement 4 dossiers et seulement 1 montrait que la surveillance numérique avait eu un impact: un chauffeur de taxi de San Diego qui envoyait de l'argent à Al Qaïda. Source: Ibid.
-  En 2014, une autre étude montre que les renseignements traditionnels et l'investigation sur le terrain sont bien plus effectifs contre le terrorisme que la surveillance menée par la NSA. Source: Ibid
-  La NSA donnait au FBI les numéros et adresses emails suspectes pour qu'ils poursuivent les pistes. Elles étaient tellement vastes et fréquentes que le FBI déclara qu'ils leur envoyaient des déchets inutiles car intraitables. Source: Ibid
-  En 2015, ils ont admis n'avoir arrêté aucune attaque qui aurait pu être de grande ampleur et que la collection des numéros de téléphones n'aurait jamais fait la différence. Source: [ nbcnews.com](https://www.nbcnews.com/news/world/nsa-program-stopped-no-terror-attacks-says-white-house-panel-flna2D11783588)

Donc à l'évidence, ces outils de surveillance de masse ne servent pas à nous protéger. Ils construisent en revanche une architecture de l'oppression qui rend doucement possible le contrôle et la censure de la population ou de certains groupes qui ne seraient pas du même avis que le gouvernement en place.

La caméra de surveillance démontre que si je suis seul et qu'une caméra est placée dans mon bureau, je n'agirais pas de la même manière que si je suis vraiment seul. C'est ce problème auquel fait face la démocratie avec la surveillance globale. Elle modifie de plus en plus insidieusement les comportements les plus naturels et force doucement les corps et les esprits à l'auto-contrôle et à l'auto-censure. C'est l'effet du [panoptique](https://fr.wikipedia.org/wiki/Panoptique).

La perte de notre vie privée aujourd’hui, au nom de la sécurité, montre qu'elle ne nous protège pas plus qu’hier mais plutôt que des organismes ont reçu l'autorisation de nous ficher et de nous profiler entièrement, de nos empreintes digitales, visages et corps, jusqu'à nos contacts et réseaux étendus, nos croyances et nos hobbies..

## La surveillance de masse
***
Comme son nom l'indique, elle est appliquée à la masse, c'est à dire à tout le monde, sans frontières et sans éléments singuliers de soupçons. Tout le monde est suspecté, tout le monde est donc fiché et voit son droit à la confidentialité se restreindre progressivement.

> _"Aux États-Unis, la NSA collecte les données téléphoniques de plus de 300 millions d'Américains. L'outil de surveillance internationale XKeyscore permet aux analystes du gouvernement américain de rechercher dans d'immenses bases de données contenant les courriels, les conversations en ligne (chats) et les historiques de navigation de millions de personnes._
>
> _Le programme de surveillance globale britannique Tempora intercepte le trafic des câbles de fibres optiques qui constituent l'épine dorsale d'Internet. Avec le programme de surveillance de la NSA [PRISM](https://fr.wikipedia.org/wiki/PRISM_(programme_de_surveillance)), les données qui auraient déjà atteint leur destination seraient directement récoltées à partir des serveurs des fournisseurs de services américains suivants : Microsoft, Yahoo!, Google, Facebook, Paltalk, AOL, Skype, YouTube et Apple."_

[...]

> _"D'après des documents top secret de la NSA révélés par Edward Snowden; au cours d'une seule journée de l'année 2012, la NSA a collecté les carnets d'adresses de courriel de :_
>
>  - _22 881 comptes Gmail ;_
>  - _82 857 comptes Facebook ;_
>  - _105 068 comptes Hotmail ;_
>  - _444 743 comptes Yahoo!._
>
>_Chaque jour, la NSA collecte les contacts depuis environ 500 000 listes de contacts de services de conversation en ligne (chats), ainsi que depuis ceux affichés dans les boîtes de réception de comptes de messagerie en ligne (Webmail). Une fois réunies, ces données permettent à la NSA de tracer des graphes sociaux, sortes de cartes détaillées de la vie sociale d'une personne, réalisées en se basant sur ses connexions personnelles, professionnelles, religieuses et politiques._
>
> Source: [Surveillance Globale — Wikipedia](https://fr.wikipedia.org/wiki/Surveillance_globale#Surveillance_globale)

## Côté business
***

Le modèle économique d’Internet passe aujourd'hui majoritairement par des financements publicitaires et le rachat des données personnelles des utilisateurs par des entreprises.

Tout ce qui est fait en ligne: rechercher, naviguer, partager, poster, commenter, liker, etc., est collecté, fiché, analysé, vendu et stocké méthodiquement et probablement pour toujours par des moteurs de recherche, des réseaux sociaux, des services et entreprises, des gouvernements et des FAI (Fournisseurs d’Accès à Internet).

Même les personnes n'ayant pas de compte sur leurs plateformes, ont un profil personnel créé à cet effet et vendu de la même manière, bien qu'il soit moins étoffé.

On accepte les conditions générales des services que l'on utilise. Donc nous signons, souvent sans les lire, des autorisations qui contournent nos droits fondamentaux à la confidentialité, dès que l'on utilise Google par exemple, avec ou sans compte.

L'avantage pratique de la création d'un compte pour certains, peut se trouver dans des paramètres ou publicités ciblés (appropriés et au bon moment). Pour d'autres, dans la possibilité de synchroniser identifiants et données personnelles sur de nombreux sites.

## Les dangers
***
Nous ne contrôlons pas la vente de nos données ce qui implique que nous ne savons pas à qui et pourquoi elles seront utilisées, vendues et revendues, ni par qui elles seront définitivement stockées.

Ce que l'on fait et dit aujourd'hui est enregistré et pourrait un jour (aujourd'hui pour certains) être désapprouvé par son gouvernement ou même considéré comme illégal.

-   En 2015, en France, cette surveillance a permis au gouvernement de ficher et d'assigner à résidence 24 personnes innocentes et sans antécédents judiciaires souhaitant manifester pour l'écologie lors de la COP21. Sources: [Humanité](https://www.humanite.fr/moi-joel-domenjoud-militant-assigne-residence-591565), [Nothing to hide](https://media.zat.im/videos/watch/35badfed-5322-48ac-b5c1-71b1ad88262e), [The New York Times](https://www.nytimes.com/2015/11/30/world/europe/france-uses-sweeping-powers-to-curb-climate-protests-but-clashes-erupt.html), [Amnesty International](https://www.amnesty.fr/liberte-d-expression/actualites/joel-la-surveillance-au-quotidien)
-   Aux États-Unis, cette pratique a dirigé et instrumentalisé l'opinion et le vote de millions de personnes lors de l'élection de Donald Trump, c'est [le cas Cambridge Analytica](https://invidious.snopyta.org/watch?v=ODfD3VYCoGU).
-   En Chine, elle permet le contrôle total de la population, l'impossibilité de manifester une opinion différente, et un internement esclavagiste de plus d'1 millions de Ouïghours dans des camps de concentration actuellement. Sources: [Liberation](https://www.liberation.fr/planete/2019/09/05/ouighours-au-xinjiang-un-lent-et-silencieux-genocide-culturel_1749543), [ARTE](https://invidious.snopyta.org/watch?v=8wN3emyA-ew)

Pour ne prendre que trois exemples, la liste est longue et la bascule est mondiale.

De plus, quand des citoyens utilisent les mêmes outils pour surveiller le gouvernement et alerter leurs concitoyens au nom de la démocratie, les lanceurs d'alertes sont poursuivis en justice et emprisonnés: [Reality Winner](https://fr.wikipedia.org/wiki/Reality_Winner), [Terry Albury](https://en.wikipedia.org/wiki/Terry_J._Albury), [Edward Snowden](https://fr.wikipedia.org/wiki/Edward_Snowden), [Julian Assange](https://fr.wikipedia.org/wiki/Julian_Assange), [Chelsea Manning](https://fr.wikipedia.org/wiki/Chelsea_Manning) ...

## Vous n'avez rien à cacher ?
***

La réponse classique est que si je ne fais rien de mal, je n'ai rien à cacher.

> _“Les opposants au « je n’ai rien à cacher » soulignent d’ailleurs que les personnes qui emploient cet argument ne réalisent pas toujours à quel point le croisement des données permet d’obtenir des informations poussées sur leur situation: des réseaux sociaux, des moteurs de recherche, des sociétés bancaires, des grandes surfaces (via l’usage de cartes de fidélité) par exemple, peuvent disposer de suffisamment d’informations pour deviner l’état émotionnel d’un individu, pour détecter le moment où il entame une relation amoureuse, pour obtenir des éléments sur son orientation sexuelle ou politique, sur son hygiène de vie (en fonction des produits achetés) ou sa situation financière (note de solvabilité). En somme, un graphe social plus ou moins complet peut être dressé grâce aux métadonnées.”_
>
> Source: [Rien à cacher (argument) — Wikipedia ](https://fr.wikipedia.org/wiki/Rien_%C3%A0_cacher_(argument))

Lorsqu'on offre sa vie privée à une entreprise ou un gouvernement, on tombe sous son contrôle.

La conférence de Glenn Greenwald, [Why privacy matters](https://www.ted.com/talks/glenn_greenwald_why_privacy_matters/up-next), est sous-titrée en français. En 15 minutes, il résume et démonte ce type de raisonnement en l'appuyant d'exemples concrets.

[Nothing to hide](https://media.zat.im/videos/watch/35badfed-5322-48ac-b5c1-71b1ad88262e) est un documentaire de 2017, montrant les effets pervers de la surveillance de masse et démontrant l'intérêt collectif de la protection de sa vie privée.

## Liberté
***

La liberté est un droit fondamental. Cette courte présentation montre rapidement à quel point les outils numériques permettent aux entreprises et aux gouvernements de l'entraver dangereusement en ayant un accès privilégié à nos données personnelles ou celles de nos contacts.

La liberté se gagne et se conserve ensemble, c'est un combat commun. Quand une personne est prise en photo, enregistrée à son tour sur le cloud de Google, son visage sera scanné et enregistré. Quand nos coordonnées sont enregistrées dans le répertoire d'une connaissance et que celle-ci accepte l'accès à ses contacts sur les applications mobiles et des services dangereux et malintentionnés, nos informations seront disponibles aux entreprises.

Ne pas protéger ses données revient à ne pas protéger ses contacts.
-  C'est compromettre la liberté et la confidentialité de ses proches.
-  C'est participer personnellement au fichage collectif pour les entreprises et les gouvernements.
-  C'est compromettre _in fine_ la liberté de chacun.

Il est bon de s'informer davantage sur les enjeux et dérives actuels de la surveillance de masse. Des documents à voir et à écouter sont [disponibles ici](/docs/ressources/medias.html).


{{< prev relref="/docs/start/introduction" >}} < Introduction {{< /prev >}}
{{< next relref="/docs/start/threat-model" >}} Threat model  > {{< /next >}}
