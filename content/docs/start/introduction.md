---
title: "Introduction"
bookToc: false
weight: 1
description: Cave Canem propose des guides pratiques pour comprendre et agir contre la fuite de ses données personnelles sur Internet. Cette introduction développe le projet qui est mené.
---
# Introduction
***

Cave Canem croise des données de [sources anglaises](/docs/ressources.html) multiples sur les questions de la vie privée numérique dans le but d'informer en français.

Ces guides pratiques permettent de comprendre et d'agir contre la fuite de ses données personnelles sur Internet. Non « exhaustifs », ils sont complétés et actualisés régulièrement. Il est donc important de vérifier sur [cette page](/maj.html), les mises à jour apportées aux chapitres à chaque connexion.

Contributions ou remarques \> [Contact](/contact.html).


## Projet
***

-   Proposer une marche à suivre et des explications simples, à la portée de tous.
-   Proposer des tutoriels existants, simples et en français, si besoin d'aide.
-   Présenter des outils et services alternatifs aux grandes compagnies, majoritairement [_open source_](/docs/start/code-source.html), et au mieux, audités en plus par des entreprises indépendantes, expertes en cybersécurité.
-   Mettre à disposition des outils plus rapides, plus sûrs, respectueux de la vie privée de l'utilisateur et de ses contacts.
-   Faire des chapitres pour évoluer pas à pas car cela demande un peu de temps.

## Objectifs
***

-  Rendre conscient des enjeux et des gestes barrière à adopter plutôt que de rendre paranoïaque.
-  Connaître et utiliser les outils qui œuvrent pour respecter leurs utilisateurs et qui ont d'autres modèles économiques que la vente massive de nos données personnelles.
-  Limiter la surveillance de masse, le fichage, la fuite et la vente de nos données personnelles.
-  Limiter les risques d'être espionné, usurpé.
-  Apprendre à configurer ses outils quotidiens selon le minimum de sécurité recommandée.


## Indications
***

-  Certaines personnes prendront quelques jours, d'autres quelques mois, mais aller jusqu'au bout permettra de se réapproprier ses outils et sa vie privée.
-  Suivre l'ordre des chapitres permettra de bien tout comprendre, apprendre ou réapprendre plus facilement.
-  Ce projet n'est pas un guide pour l'anonymat (l'anonymat réel n'existant pas avec le numérique sans un mode de vie approprié).

## À noter
***

Il est urgent de s'éloigner de tous les outils et services (et matériel si possible) des GAFAM (Google, Amazon, Facebook, Apple, Microsoft) car leur fond de commerce réside sur l'accumulation et la revente des données personnelles de leurs utilisateurs.

Avec le numérique, il faut se tenir au courant des mises à jours des outils utilisés au quotidien. Il arrive régulièrement que des produits sains deviennent dangereux à la suite d'un rachat, d'une loi ou d'une collaboration gouvernementale.

Il est aussi bon de comprendre que les outils doivent être changés quand ils
deviennent néfastes et dangereux pour sa sécurité, sa vie privée et celles de ses contacts.

Le numérique évoluant rapidement, rien n'est acquis, il faut suivre l'évolution et s'adapter.

Lorsque des failles ou des réglementations liberticides sont repérées, il est recommandé d’adapter son comportement et de communiquer ces évolutions à d’autres personnes peut-être moins informées.

## Rappels
***

-   Toute action numérique (et par extension sur Internet) est enregistrée quelque part et probablement pour toujours.
-   La sécurité numérique aujourd'hui ce n'est plus seulement se protéger des virus, c'est aussi et surtout de pouvoir encore préserver le domaine de sa vie privée.
-   Les produits des GAFAM ne sont pas "gratuits". La récupération et la vente systématique des données personnelles des utilisateurs en sont le vrai prix.
-   D'autres modèles économiques existent et sont viables, Wikipédia par ex., fonctionnant sur les apports collectifs et les dons.
-   Ouvert, visible et vérifiable par tous, l'_open source_ permet de s'assurer de l'intégrité du [code source](/docs/start/code-source.html) et donc de la sécurité du programme.
-   Les outils _open source_ sont souvent _free_ donc libres et non pas forcément gratuits.
-   La gratuité des outils _open source_ reposent principalement sur les dons. et la notion de [Pay what you want](https://en.wikipedia.org/wiki/Pay_what_you_want). Il est permis de l'utiliser gratuitement mais suggéré de le payer à la hauteur de ses moyens quand on l'utilise tous les jours. Ces dons soutiennent un Internet cherchant un autre modèle économique que la vente des données personnelles et le fichage collectif. Cela prouve aussi aux autres fournisseurs de services que les gens sont prêts à supporter les technologies améliorant le respect de leur vie privée.
-   Les applications ou logiciels gratuits ne sont pas automatiquement _open source_, sécurisés ou respectueux de la vie privée, loin de là.
-   Les services et outils payants ne sont pas forcément sécurisés ou respectueux pour autant.
-   _Open source_ ne veut pas forcément dire sécurisé. Il faut le vérifier auprès de [spécialistes](/docs/ressources.html).


{{< next relref="/docs/start/interets-de-la-vie-privee" >}} Intérêts de la vie privée  > {{< /next >}}
