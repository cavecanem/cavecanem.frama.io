---
title: "Lois de divulgation des clés"
bookToc: false
weight: 5
description: Qu'est-ce que les lois de divulgation des clés ?
summary: Obligation légale des entreprises de services à remettre les clés de chiffrements aux autorités chargées de l’application de la loi dans le cadre d’enquêtes pénales.
---

# Lois sur la divulgation des clés
***

Les lois sur la divulgation des clés obligent légalement les entreprises de services à remettre les clés de chiffrements aux autorités chargées de l’application de la loi dans le cadre d’enquêtes pénales.

La divulgation des clés permet de déchiffrer les contenus stockés sur les serveurs (Cloud, emails, messageries instantanées, etc.).

Les modalités d’application de ces lois varient d’un pays à l’autre. Un mandat est généralement nécessaire pour pouvoir accéder aux clés.

Mais, un simple changement de lois, ([Patriot Act](https://fr.wikipedia.org/wiki/USA_PATRIOT_Act) aux USA, [État d'urgence en France](https://fr.wikipedia.org/wiki/%C3%89tat_d%27urgence_en_France)) peut assouplir de manière dramatique la permission accordée aux autorités pour mandater et obliger les compagnies à révéler leurs clés.

Le fichage politique et les soupçons étant décomplexés, si les autorités déclarent arbitrairement que c’est un problème pour la sûreté de l’état, n'importe quel groupe s'opposant à la politique du gouvernement peut rapidement faire l’objet d’une surveillance, d'un procès, d'une assignation à résidence et d'un contrôle de ses fichiers et activités en ligne auprès des services utilisés. Exemple en France avec des militants écologistes: [Humanité](https://www.humanite.fr/moi-joel-domenjoud-militant-assigne-residence-591565), [Nothing to hide](https://media.zat.im/videos/watch/35badfed-5322-48ac-b5c1-71b1ad88262e), [The New York Times](https://www.nytimes.com/2015/11/30/world/europe/france-uses-sweeping-powers-to-curb-climate-protests-but-clashes-erupt.html), [Amnesty International](https://www.amnesty.fr/liberte-d-expression/actualites/joel-la-surveillance-au-quotidien).

{{<block sain>}}

### Pratique saine
***

-  Toujours savoir où sont stockés géographiquement ses activités en ligne et ses fichiers.
-  Choisir des services qui ont leurs serveurs dans des pays n'appliquant pas de lois de divulgation des clés de chiffrement.

{{</block>}}

{{<columns>}}

### Lois appliquées
***

<span class="flag-icon flag-icon-ag"></span> [Antigue et Barbude](https://en.wikipedia.org/wiki/Key_disclosure_law#Antigua_and_Barbuda)

<span class="flag-icon flag-icon-za"></span> [Afrique du Sud](https://en.wikipedia.org/wiki/Key_disclosure_law#South_Africa)

<span class="flag-icon flag-icon-au"></span> [Australie](https://en.wikipedia.org/wiki/Key_disclosure_law#Australia)

<span class="flag-icon flag-icon-ca"></span> [Canada](https://en.wikipedia.org/wiki/Key_disclosure_law#Canada)

<span class="flag-icon flag-icon-fr"></span> [France](https://en.wikipedia.org/wiki/Key_disclosure_law#France)

<span class="flag-icon flag-icon-in"></span> [Inde](https://en.wikipedia.org/wiki/Key_disclosure_law#India)

<span class="flag-icon flag-icon-ie"></span> [Irelande](https://en.wikipedia.org/wiki/Key_disclosure_law#Ireland)

<span class="flag-icon flag-icon-no"></span> [Norvège](https://edri.org/norway-introduces-forced-biometric-authentication/)

<span class="flag-icon flag-icon-gb"></span> [Royaume-Uni](https://en.wikipedia.org/wiki/Key_disclosure_law#United_Kingdom)

<span class="flag-icon flag-icon-ru"></span> [Russie](https://www.bloomberg.com/news/articles/2018-03-20/telegram-loses-bid-to-stop-russia-from-getting-encryption-keys)

<--->

### Lois peuvent s'appliquer
***

<span class="flag-icon flag-icon-be"></span> [Belgique](https://tweakers.net/nieuws/163116/belgische-rechter-verdachte-mag-verplicht-worden-code-smartphone-af-te-staan.html)

<span class="flag-icon flag-icon-ee"></span> [Estonie](https://www.riigiteataja.ee/akt/106012016019)

<span class="flag-icon flag-icon-us"></span> [États-Unis](https://en.wikipedia.org/wiki/Key_disclosure_law#United_States)

<span class="flag-icon flag-icon-fi"></span> [Finlande](https://en.wikipedia.org/wiki/Key_disclosure_law#Finland)

<span class="flag-icon flag-icon-nz"></span> [Nouvelle-Zélande](https://en.wikipedia.org/wiki/Key_disclosure_law#New_Zealand)

<span class="flag-icon flag-icon-nl"></span> [Pays-Bas](https://en.wikipedia.org/wiki/Key_disclosure_law#The_Netherlands)

<--->

### Lois ne s'appliquent pas
***

<span class="flag-icon flag-icon-de"></span> [Allemagne](https://en.wikipedia.org/wiki/Key_disclosure_law#Germany)

<span class="flag-icon flag-icon-is"></span> [Islande](https://en.wikipedia.org/wiki/Key_disclosure_law#Iceland)

<span class="flag-icon flag-icon-it"></span> [Italie](https://iclg.com/practice-areas/cybersecurity-laws-and-regulations/italy)

<span class="flag-icon flag-icon-pl"></span> [Pologne](https://en.wikipedia.org/wiki/Key_disclosure_law#Poland)

<span class="flag-icon flag-icon-cz"></span> [République Tchèque](https://en.wikipedia.org/wiki/Key_disclosure_law#Czech_Republic)

<span class="flag-icon flag-icon-se"></span> [Suède](https://en.wikipedia.org/wiki/Key_disclosure_law#Sweden)

<span class="flag-icon flag-icon-ch"></span> [Suisse](https://www.wikipedia.org/wiki/Key_disclosure_law#Switzerland)

{{</columns>}}

Plus d'information sur les lois en vigueur en cliquant sur le pays

{{< prev relref="/docs/start/alliances" >}} < Alliances de surveillance {{< /prev >}}
{{< next relref="/docs/start/code-source" >}} Le code source  > {{< /next >}}
