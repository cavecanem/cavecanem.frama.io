---
title: "Les métadonnées"
bookToc: false
weight: 8
description: Les métadonnées sur Internet et dans l'environnement numérique. Qu'est-ce que c'est ?
---

# Métadonnées
***

Les métadonnées sont des données de données. Ils s'agit d'informations utilisées pour classer et traiter les fichiers numériques.

## Fichiers numériques
***

Un exemple simple de métadonnées pour un document pourrait inclure une collection d'informations comme l'auteur, la taille du fichier, la date de création du document, la date de la dernière modification, les mots clés qui décrivent le document, etc. Les métadonnées d'un fichier musical peuvent inclure le nom de l'artiste, de l'album, l'année de sa sortie, la pochette de l'album, le style de musique, etc.

![meta-musique](/img/metadonnees/metadonnees-musique.png "Exemple de métadonnées d'un fichier audio iTunes")

Les métadonnées représentent donc les coulisses des informations qui sont ou peuvent être utilisées partout.

Les champs couverts par les métadonnées diffèrent grandement en fonction de l'appareil utilisé (appareil photo, TV, téléphone, ordinateur, tablettes, etc.) mais aussi de l'application ou du service utilisé (moteur de recherche, réseaux sociaux, messagerie, vente en ligne, applications mobiles, etc.).

Les métadonnées sont créées par l'appareil utilisé (appareil photo par exemple), ou ajoutées par l'utilisateur et/ou le service (ajout de copyright, géolocalisation, personnes présentes sur une image, etc.).

Il est donc totalement impossible de dresser une liste exhaustive des champs couverts par les métadonnées, d'autant que les services ne sont pas soumis à la transparence de leurs créations ou de leurs collectes.

## Réseaux sociaux
***

Sur les réseaux sociaux, chaque action (ajouter un ami sur Facebook, écouter un morceau sur Spotify, poster un statut, partager le tweet de quelqu'un, etc.) crée des métadonnées qui œuvrent en arrière-plan.

Elles sont utiles dans des situations très spécifiques pour les utilisateurs, comme par exemple lorsqu'on recherche quelqu'un sur Facebook. On peut voir une image de profil et une courte description de l'utilisateur, les contacts et les groupes d'intérêts que l'on a en commun, ceci afin d'en savoir plus avant de décider de l'ajouter à ses contacts ou de lui écrire un message.

Pour le service, Facebook, Gmail, WhatsApp, Instagram etc., les métadonnées lui permettent une myriade de possibilité qu'il a lui-même créé et que l'utilisateur ne peut pas utiliser. Il peut relier, cartographier et archiver les relations des personnes entre elles de manière extrêmement précise en affinant les données des liens d'intérêts, de relations amicales, amoureuses, professionnelles, d'opinions politiques, de genres, de communauté, de couleur de peau, de niveau d'éducation, etc.

## Suivi en ligne
***

Les applications mobiles des GAFAM et autres mauvais services du genre, les détaillants et les sites d'achat en ligne utilisent des métadonnées pour suivre les habitudes et les mouvements des consommateurs/utilisateurs. Les spécialistes du marketing numérique suivent chaque clics et achats, en stockant des informations concernant l'utilisateur comme : le type d'appareil utilisé, la localisation, l'heure, la date et toute autre donnée qu'ils sont légalement autorisés à recueillir.

Avec ces informations, ils créent une image de la routine de l'utilisateur et de ses interactions quotidiennes, de ses préférences, de ses associations et de ses habitudes. Ils peuvent utiliser cette image pour lui vendre d'autres produits ou revendre ses informations à des entreprises tierces qui pourraient être intéressées.

Les FAI, les gouvernements, les grosses entreprises et toute autre personne ayant accès à de larges collections de métadonnées peut potentiellement les utiliser pour surveiller l'activité sur le Web.

Les métadonnées étant une représentation succincte de données plus importantes, elles peuvent être recherchées et filtrées pour trouver des informations sur des millions d'utilisateurs à la fois et suivre des éléments précis comme par exemple: les discours haineux, les menaces ou les photos d'un événement spécifique.

Certains gouvernements, comme les États-Unis, la Chine ou la France, sont connus pour collecter ces données, non seulement le trafic Web mais aussi les appels téléphoniques, les informations de localisation, etc.

## Dangers
***

Les métadonnées étant des registres très précis sur les individus et les liens qui les unissent, il n'est pas difficile de comprendre les dangers de leurs détention par des compagnies privées ou des gouvernements, ni pourquoi elles sont devenues l’or de ces dernières décennies.

Elles sont un moyen efficace pour profiler les individus. Collectées tout à fait légalement elles peuvent être utiliser par un gouvernement, une entreprise, ou des annonceurs. Le profil peut aider à calibrer la surveillance, les publicités, rendre plus efficace les algorithmes des produits.

Ce commerce florissant dont les internautes, qui en sont à la fois à l’origine et la cible, ne voient pas un euro et ignorent totalement qui détient – et exploite – leur vie privée.

Le chiffrement peut aider à contrer la collecte des métadonnées. Le problème est qu'il n'est pas à l'oeuvre partout dans certaines applications et services, et souvent inutilisé sur les appareils des utilisateurs ou dans leur utilisation quotidienne.

Même si l'on chiffre le contenu de ses emails, messages, conversations vocales ou de ses navigations sur le Web, savoir quand, où, de qui, à qui et à quelle fréquence une communication ou un achat quelconque a lieu peut en dire long sur soi.

Par exemple, même en utilisant un service de messagerie crypté tel que WhatsApp, Facebook peut toujours savoir à qui l'on envoie des messages, à quelle fréquence ces messages sont envoyés, pendant combien de temps les discussions dure habituellement, et plus encore. En recoupant l'heure, la durée, la fréquence et les jours de la semaine, il n'est pas difficile de savoir si deux personnes sont collègues, amis ou amantes.

L'avocat général de la NSA, Stewart Baker, a ouvertement reconnu que les métadonnées disent absolument tout de la vie de quelqu'un et qu'avec suffisamment de métadonnées, connaître le contenu des échanges n'est pas vraiment une nécessité.

## L'information c'est le pouvoir
***

Avec toutes ces informations à portée de main, les entreprises peuvent regrouper des données de manière très utile, et pas seulement par utilisateur ou visiteur. Elles peuvent également examiner les tendances et les comportements de villes ou de pays entiers.

Les entreprises peuvent utiliser les informations qu'elles recueillent pour toute une série de choses utiles. Dans tous les domaines où elles sont actives, elles peuvent prendre des décisions de marché, faire des recherches, affiner leurs produits, etc., à l'aide des données collectées.

Si l'on peut découvrir certaines tendances de marché ou de la politique à un stade précoce, on peut y réagir efficacement. On peut découvrir ce que les gens recherchent, ce qu'ils veulent, et prendre des décisions basées sur ces découvertes. Cela est bien sûr extrêmement utile pour les entreprises et notamment pour celles qui contrôlent les marchés comme Google, Amazon, Facebook, Apple ou Microsoft, mais également pour les gouvernements et le contrôle de l'opinion politique et de la population comme en Chine.

Par exemple, Google gagne une grande partie de son argent en diffusant des annonces. Plus Google en sait sur nous, plus il est en mesure de nous diffuser des annonces, ce qui a un effet direct sur ses résultats financiers.

## Réduction d'Internet
***

L’exploitation de ces métadonnées conditionne également la recherche en ligne des usagers, qui n’ont pas toujours conscience des logiques marchandes mises en place, notamment lors de l'affichage des résultats par les moteurs de recherche, les réseaux sociaux, les sites marchands, etc. C’est notamment ce qu’[Eli Pariser](https://fr.wikipedia.org/wiki/Eli_Pariser) nomme _filter bubble_, la bulle cognitive ou bulle filtrante. Les services orientent les recherches et résultats de l’internaute selon le profil qu’ils en ont dressé. Le but est de l’orienter vers ce qu’il est susceptible d’apprécier le plus, ce qui correspond le mieux à son cercle social ou son emplacement géographique.

Les résultats d’une recherche ne sont donc pas les mêmes selon les individus et les appareils, chacun se heurtant à une bulle bâtie par des algorithmes eux-mêmes bâtis sur leurs métadonnées. Un système bien loin des aspirations de liberté et d’universalité qui ont fondé Internet.

Ces bulles, notamment via les réseaux sociaux, mettent en place de véritables créations d’opinions par l’exposition de contenus jouant sur l’affect des usagers. Ceux-ci finissent par exclure la différence, être touchés par des phénomènes viraux, et bien souvent se diviser au sein de la population.

## La suppression des métadonnées
***

Comme écrit en début de chapitre, les métadonnées sont créées par le programme ou l'entreprise. C'est elle qui décide ce qu'elle veut inscrire et/ou récupérer. Il est donc quasiment impossible de supprimer ses métadonnées tant les champs sont vastes.

Avec quelques fichiers numériques - PDF, images, vidéos, audios - il est possible de [supprimer des métadonnées](/docs/outils/suppression-metadonnees.html). Le mieux est d'éviter de fournir des métadonnées en utilisant des services et des outils qui respectent leurs utilisateurs.

{{< prev relref="/docs/start/chiffrement" >}} < Le chiffrement {{< /prev >}}
