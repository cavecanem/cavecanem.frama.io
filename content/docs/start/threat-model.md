---
title: "Threat model"
bookToc: false
weight: 3
description: Le Threat Model (modèle de menace) est l'élément de base personnel déterminant les outils adaptés au maintient de sa confidentialité et à l'accroissement de sa sécurité sur Internet.
---

# Threat Model
***

La base d'une bonne protection commence par la définition de son propre modèle de menace (_threat model_). Sa définition aide à choisir le matériel, les outils et les comportements adaptés à sa sécurité.

## Modèle utilisé ici
***

La protection et les outils proposés ici sont pour un modèle de menace faible, lorsque l'on n'est pas la cible (d'un gouvernement ou de tiers). Ce modèle concerne chaque utilisateur lambda:

-   Mon activité Internet est pistée et mes données revendues à des tiers.
-   Ma navigation n'est pas sécurisée.
-   Ma navigation contient de la publicité et de la publicité ciblée.
-   Ma navigation comporte des risques d'infection.
-   Mes contenus en ligne sont scannés et permettent de me ficher, moi et mes relations, de manière précise et pour une durée inconnue, indéterminée voire illimitée.
-   Mes applications mobiles et services Internet traquent mes activités en ligne et hors ligne.
-   Mes mots de passe et comptes bancaires sont, ou risque, d'être revendus sur le marché noir.
-   Mes données sont, ou risquent d'être, lues et exploitées par des entreprises et des gouvernements.
-   Avec mon gouvernement actuel ou s'il bascule, mon fichage permet d'être identifié, ainsi que mes croyances, mes relations et mes opinions et permet donc une surveillance, un contrôle, une censure.

## Modèles de menace plus élevés
***

-  Consulter le site et appliquer.
-  Aller plus loin dans les [recherches](/docs/ressources.html) et outils qui protègent davantage.
-  Pour les journalistes par exemple, s'équiper de matériel et d'OS adéquats est un bon début, voir [produits](/docs/ressources/produits.html).
-  Si vous êtes une cible (de votre gouvernement par exemple) ces mesures ne suffiront pas. Tournez-vous vers des professionnels en cybersécurité.

{{< prev relref="/docs/start/interets-de-la-vie-privee" >}} < Intérêts de la vie privée {{< /prev >}}
{{< next relref="/docs/start/alliances" >}} Alliances de surveillance  > {{< /next >}}
