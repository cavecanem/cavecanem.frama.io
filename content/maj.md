---
title: "Mises à jour"
bookToc: false
---

# Mises à jour
***

{{<tabs "Cat">}}

{{<tab "Courantes">}}

### 14/12/20
-  Ajout de **Signal** dans [Visioconférences](docs/outils/visioconferences.html). Signal prend désormais en charge la visioconférence jusqu'à 5 participants.

### 09/12/20
-  Ajout de **AnonAddy** et de **SimpleLogin** dans Emails/[Alias](/docs/outils/emails/alias.html).
-  Ajout de [L’Âge du capitalisme de surveillance](https://www.zulma.fr/livre-lage-du-capitalisme-de-surveillance-572196.html) dans Ressources/[Medias](/docs/ressources/medias.html).
-  Ajout de [La Silicon Valley, le nouveau centre du pouvoir mondial ?](https://www.franceculture.fr/emissions/la-grande-table-idees/la-silicon-valley-le-nouveau-centre-du-pouvoir-mondial) dans Ressources/[Medias](/docs/ressources/medias.html).

### 01/12/20
- Ajout du chapitre [Visioconférences](docs/outils/visioconferences.html).

### 30/11/20
-  Ajout de **Jirafeau** et mise à jour pour **Firefox Send** (fermeture) et **Lufi** dans [Partage de fichiers](docs/outils/partage-de-fichiers/fichiers.html).
-  Ajout de [GNU/Linux](docs/outils/os/linux.html) dans [Systèmes d'exploitation](docs/outils/os.html).
-  Ajout du paragraphe "**Permettre l’installation d’applications hors App Store**" dans [macOS](docs/outils/os/macos.html).

### 17/09/20
-  Modification dans [Choix de navigateurs](docs/outils/choix-navigateurs.html), **iOS14 permet désormais de choisir un autre navigateur par défaut**.

### 12/09/20
-  Ajout du documentaire **_The Social Dilemma_** dans Ressources/[Medias](/docs/ressources/medias.html).

### 11/09/20
-  Ajout du chapitre [Inscriptions](/docs/outils/inscriptions.html).
-  Ajout du chapitre [Paiements en ligne](/docs/outils/paiements.html).
-  Ajout du paragraphe "**App Store Alternatifs**" dans [Applications mobiles](/docs/outils/applications.html).
-  Ajout de **Pine64** dans Ressources/[Matériel](/docs/ressources/produits.html).
-  Ajout de **CalyxOS** dans les alternatives à [Android](/docs/outils/os/android.html).
-  Ajout de **Techlore** et **Rob Braxman Tech** dans Ressources/[Guides](/docs/ressources/guides.html) en anglais.
-  Ajout de **Swisscows** dans [Moteurs de recherche](/docs/outils/moteurs-de-recherche.html).
-  Mise à jour du chapitre [Alias](/docs/outils/emails/alias.html).
-  Mise à jour de [Vidéos en ligne](/docs/outils/videos.html).
-  Mise à jour d'**Invidious** dans [Visionner des vidéos](/docs/outils/videos/visionner-videos.html).

### 06/06/20
-  Ajout du chapitre [Code source](/docs/start/code-source.html).
-  Ajout du chapitre [Métadonnées](/docs/start/metadonnees.html).
-  Ajout du chapitre [Suppression de métadonnées](/docs/outils/suppression-metadonnees.html).
-  Ajout du chapitre [Vidéos en ligne](/docs/outils/videos.html).

### 03/06/20
-  Ajout de [Messagerie Instantanées Fédérées](/docs/outils/im/federees.html)

### 29/06/20
-  Ajout de [Fingerprint](/docs/outils/navigateurs/fingerprint.html) au chapitre [Navigateurs](docs/outils/navigateurs.html).

### 15/06/20
-  Ajout de la rubrique [Supprimer son compte](docs/outils/emails/suppression-mail.html) au chapitre Emails.
-  Ajout des paragraphes Protocoles et Environnement, et d'explications dans [Clients emails](docs/outils/emails/clients.html).
-  Ajout du paragraphe **HTTPS** au chapitre [Navigateurs](docs/outils/navigateurs.html).
-  Réécriture de la page [Chiffrement](docs/start/chiffrement.html)

### 14/06/20
-  Ajout du chapitre [Applications mobiles](docs/outils/applications.html).

### 13/06/20
-  Ajout de la rubrique [Protections supplémentaires](docs/outils/mots-de-passe/plus.html) au chapitre Mots de passe.
-  Ajout du paragraphe "**Quitter Windows**" dans [Windows 10](docs/outils/os/windows10.html).
-  Ajout des paragraphes "**Cache**", "**Cookies**", "**Historique**" et "**Marque-pages**" au chapitre [Navigateurs](docs/outils/navigateurs.html).
-  Réécriture et ajout de paragraphes dans [Intérêts de la vie privée](docs/start/interets-de-la-vie-privee.html)

### 09/06/20
-  Mise en ligne du site sur [Framagit](https://framagit.org), le Gitlab de [Framasoft](https://framasoft.org/fr/).

### 04/20
-  Lancement du projet.

{{</tab>}}

{{<tab "Prochaines">}}

### À venir

-  Annexe Fédération
-  Annexe Manifestations
-  Chapitre Brique Internet / Raspberry Pi
-  Chapitre Images en ligne
-  Chapitre Réseaux sociaux
-  Chapitre Cloud
-  Chapitre Notes
-  Chapitre Suppressions -Fichiers, Comptes en ligne
-  Chapitre Machine virtuelle

{{</tab>}}
{{</tabs>}}
